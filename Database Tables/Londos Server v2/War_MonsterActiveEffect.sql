CREATE DATABASE  IF NOT EXISTS `War` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `War`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: War
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `MonsterActiveEffect`
--

DROP TABLE IF EXISTS `MonsterActiveEffect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `MonsterActiveEffect` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MonsterID` bigint(20) NOT NULL,
  `AbilityID` bigint(20) DEFAULT NULL,
  `Unk1` int(11) DEFAULT NULL,
  `Unk2` int(11) DEFAULT NULL,
  `Unk3` int(11) DEFAULT NULL,
  `RegionID` bigint(20) DEFAULT NULL,
  `ZoneID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_MonsterActiveEffect_MonsterID` (`MonsterID`),
  KEY `IX_MonsterActiveEffect_AbilityID` (`AbilityID`),
  KEY `IX_MonsterActiveEffect_RegionID` (`RegionID`),
  KEY `IX_MonsterActiveEffect_ZoneID` (`ZoneID`),
  CONSTRAINT `FK_MonsterActiveEffect_AbilityBin_AbilityID` FOREIGN KEY (`AbilityID`) REFERENCES `abilitybin` (`id`),
  CONSTRAINT `FK_MonsterActiveEffect_Ability_AbilityID` FOREIGN KEY (`AbilityID`) REFERENCES `ability` (`id`),
  CONSTRAINT `FK_MonsterActiveEffect_Monster_MonsterID` FOREIGN KEY (`MonsterID`) REFERENCES `monster` (`id`),
  CONSTRAINT `FK_MonsterActiveEffect_Region_RegionID` FOREIGN KEY (`RegionID`) REFERENCES `region` (`id`),
  CONSTRAINT `FK_MonsterActiveEffect_Zone_ZoneID` FOREIGN KEY (`ZoneID`) REFERENCES `zone` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MonsterActiveEffect`
--

LOCK TABLES `MonsterActiveEffect` WRITE;
/*!40000 ALTER TABLE `MonsterActiveEffect` DISABLE KEYS */;
/*!40000 ALTER TABLE `MonsterActiveEffect` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-26 18:40:16
