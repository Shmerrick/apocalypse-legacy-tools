﻿using System.Collections.Generic;
using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class PatchTable : FigTable<Patch>
    {
        private const int HeaderPosition = 0x90;
        private const int RecordSize = 0xC;

        public PatchTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (int i = 0; i < EntryCount; i++)
            {
                var patch = new Patch(_db, i);
                patch.Count = reader.ReadUInt16();
                patch.Unk = reader.ReadUInt16();
                patch.DataStart = reader.ReadInt32();
                patch.ZoneID = reader.ReadUInt16();
                patch.X = reader.ReadByte();
                patch.Y = reader.ReadByte();

                Records.Add(patch);
            }

            for (int i = 0; i < EntryCount; i++)
            {
                var record = Records[i];

                if (record.Count > 0)
                {
                    reader.BaseStream.Position = Offset + record.DataStart + (i * RecordSize);
                    for (int c = 0; c < record.Count; c++)
                    {
                        var a2 = new PatchAsset();
                        a2.Name = new FigStringRef(_db, reader.ReadUInt32());
                        a2.A02a = reader.ReadUInt32();
                        a2.A03a = reader.ReadUInt32();
                        a2.A04aa = reader.ReadUInt32();
                        record.Assets.Add(a2);
                    }
                }
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;

            writer.BaseStream.Position += RecordSize * Records.Count;
            var DataStart = writer.BaseStream.Position;
            var ExtStart = RecordSize * Records.Count;

            for (int i = 0; i < Records.Count; i++)
            {
                var record = Records[i];

                // record.Part1Start = 0;
                if (record.Assets.Count > 0)
                {
                    record.DataStart = (int)(ExtStart + (writer.BaseStream.Position - DataStart) - (i * RecordSize));

                    foreach (var part in record.Assets)
                    {
                        writer.Write(part.Name);
                        writer.Write(part.A02a);
                        writer.Write(part.A03a);
                        writer.Write(part.A04aa);
                    }
                }
            }
            var endPos = writer.BaseStream.Position;
            writer.BaseStream.Position = pos;

            for (int i = 0; i < Records.Count; i++)
            {
                var record = Records[i];

                writer.Write(record.Count);
                writer.Write(record.Unk);
                writer.Write(record.DataStart);
                writer.Write(record.ZoneID);
                writer.Write(record.X);
                writer.Write(record.Y);
            }

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)Records.Count);
            writer.Write((uint)(Records.Count > 0 ? pos : 0));
            var size = (uint)((endPos - DataStart) + (Records.Count * RecordSize));
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }

    public class Patch : FigRecord
    {
        public int Index { get; }
        public ushort Count { get; set; }
        public ushort Unk { get; set; }
        public int DataStart { get; set; }
        public ushort ZoneID { get; set; }
        public byte X { get; set; }
        public byte Y { get; set; }

        public List<PatchAsset> Assets = new List<PatchAsset>();

        public override string ToString()
        {
            return ZoneID.ToString();
        }
        public Patch(FigleafDB db, int index) : base(db) { Index = index; }
    }

    public class PatchAsset
    {
        public FigStringRef Name{ get; set; }
        public uint A02a{ get; set; }
        public uint A03a{ get; set; }
        public uint A04aa{ get; set; }
    }
}

