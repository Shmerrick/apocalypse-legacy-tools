﻿using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class TextureTable : FigTable<Texture>
    {
        private const int HeaderPosition = 0x60;
        private const int RecordSize = 0x3c;

        public TextureTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (int i = 0; i < EntryCount; i++)
            {

                var texture = new Texture(_db, i);
                texture.SourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                texture.A02 = reader.ReadUInt32();
                texture.A03 = reader.ReadUInt32();
                texture.Size = reader.ReadUInt32();
                texture.Width = reader.ReadUInt16();
                texture.Height = reader.ReadUInt16();
                texture.Type = (Format)reader.ReadUInt32();
                texture.MipMaps = reader.ReadUInt32();
                texture.A09_A = reader.ReadUInt32();
                texture.A09_B = reader.ReadUInt32();
                texture.A10_A = reader.ReadUInt32();
                texture.A10_B = reader.ReadUInt32();
                texture.A11 = reader.ReadUInt32();
                texture.A12 = reader.ReadUInt32();
                texture.A13 = reader.ReadUInt32();
                texture.A14 = reader.ReadUInt32();

                Records.Add(texture);
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for (int i = 0; i < Records.Count; i++)
            {
                var texture = Records[i];
                writer.Write(texture.SourceIndex);
                writer.Write(texture.A02);
                writer.Write(texture.A03);
                writer.Write(texture.Size);
                writer.Write(texture.Width);
                writer.Write(texture.Height);
                writer.Write((uint)texture.Type);
                writer.Write(texture.MipMaps);
                writer.Write(texture.A09_A);
                writer.Write(texture.A09_B);
                writer.Write(texture.A10_A);
                writer.Write(texture.A10_B);
                writer.Write(texture.A11);
                writer.Write(texture.A12);
                writer.Write(texture.A13);
                writer.Write(texture.A14);
            }

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)Records.Count);
            writer.Write((uint)(Records.Count > 0 ? pos : 0));
            var size = (uint)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    } 

    public enum Format : uint
    {
        UNKNOWN = 0,
        R8G8B8 = 20,
        A8R8G8B8 = 21,
        X8R8G8B8 = 22,
        R5G6B5 = 23,
        X1R5G5B5 = 24,
        A1R5G5B5 = 25,
        A4R4G4B4 = 26,
        R3G3B2 = 27,
        A8 = 28,
        A8R3G3B2 = 29,
        X4R4G4B4 = 30,
        A2B10G10R10 = 31,
        A8B8G8R8 = 32,
        X8B8G8R8 = 33,
        G16R16 = 34,
        A2R10G10B10 = 35,
        A16B16G16R16 = 36,
        L8 = 50,
        A8L8 = 51,
        A4L4 = 52,
        D16_LOCKABLE = 70,
        D32 = 71,
        D24X8 = 77,
        D16 = 80,
        D32F_LOCKABLE = 82,
        L16 = 81,
        R16F = 111,
        G16R16F = 112,
        A16B16G16R16F = 113,
        R32F = 114,
        G32R32F = 115,
        A32B32G32R32F = 116,
        DXT1 = 0x31545844,
        DXT2 = 0x32545844,
        DXT3 = 0x33545844,
        DXT4 = 0x34545844,
        DXT5 = 0x35545844,
    }

    public class Texture:FigRecord
    {
        public int Index { get; set; }
        public FigStringRef SourceIndex{ get; set; }
        public uint A02{ get; set; }
        public uint A03{ get; set; }
        public uint Size{ get; set; }
        public ushort Width{ get; set; }
        public ushort Height{ get; set; }
        public Format Type{ get; set; }
        public uint MipMaps{ get; set; }
        public uint A09_A{ get; set; }
        public uint A09_B{ get; set; }
        public uint A10_A{ get; set; }
        public uint A10_B{ get; set; }
        public uint A11{ get; set; }
        public uint A12{ get; set; }
        public uint A13{ get; set; }
        public uint A14{ get; set; }

        public Texture(FigleafDB db, int index) : base(db) { Index = index; }
    }
}

