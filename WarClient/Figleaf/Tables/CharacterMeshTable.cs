﻿using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class CharacterMeshesTable : FigTable<CharacterMesh>
    {
        private const int HeaderPosition = 0x54;
        private const int RecordSize = 0x14;

        public CharacterMeshesTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (int i = 0; i < EntryCount; i++)
            {
                var mesh = new CharacterMesh(_db, i);
                mesh.SourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                mesh.A02a = reader.ReadUInt32();
                mesh.A02c = reader.ReadUInt32();
                mesh.A04 = reader.ReadUInt32();
                mesh.GeomID = reader.ReadInt32();

                Records.Add(mesh);
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for (int i = 0; i < Records.Count; i++)
            {
                var mesh = Records[i];

                writer.Write((uint)mesh.SourceIndex);
                writer.Write(mesh.A02a);
                writer.Write(mesh.A02c);
                writer.Write(mesh.A04);
                writer.Write(mesh.GeomID);
            }

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)Records.Count);
            writer.Write((uint)(Records.Count > 0 ? pos : 0));
            var size = (uint)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }

    public class CharacterMesh : FigRecord
    {
        public int Index { get; }
        public FigStringRef SourceIndex{ get; set; }
        public uint A02a{ get; set; }
        public uint A02c{ get; set; }
        public uint A04{ get; set; }
        public int GeomID{ get; set; }
        public CharacterMesh(FigleafDB db, int index): base(db) { Index = index; }
    }

    
    public class CharacterMeshRef
    {
        private FigleafDB _db;

        public static implicit operator int(CharacterMeshRef r)
        {
            return r.Index;
        }

        public override string ToString()
        {
            if (Index >= 0 && Index <= _db.TableCharacterMeshes.Records.Count)
                return _db.TableCharacterMeshes.Records[Index].SourceIndex.ToString() + " [" + Index + "]";
            return "";
        }

        public int Index { get; set; }
        public CharacterMeshRef(FigleafDB db, int value)
        {
            _db = db;
            Index = value;
        }
    }
}

