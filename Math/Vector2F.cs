﻿using System;

namespace FrameWork
{
    [Serializable]
    public class Vector2F
    {
        public float X;
        public float Y;

        public Vector2F()
        {
        }

        public Vector2F(Vector2F baseVec)
        {
            X = baseVec.X;
            Y = baseVec.Y;
        }

        public Vector2F(float X, float Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public static float DotProduct2D(Vector2F v1, Vector2F v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y;
        }

        public static Vector2F HeadingToVector(ushort heading, ushort unitDistance)
        {
            double angle = heading * Constants.HeadingToRadiansFactor;

            Vector2F vec = new Vector2F((float)(-Math.Sin(angle) * unitDistance * 12), (float)(Math.Cos(angle) * unitDistance * 12));

            return vec;
        }

        public bool IsNullVector()
        {
            return Math.Abs(X) < 0.001f && Math.Abs(Y) < 0.001f;
        }

        public virtual float Magnitude => (float)Math.Sqrt(X * X + Y * Y);

        public float CosineOfAngleWithUp()
        {
            return Y / Magnitude;
        }

        public void Add(Vector2F b)
        {
            X += b.X;
            Y += b.Y;
        }

        public virtual void Multiply(float b)
        {
            X *= b;
            Y *= b;
        }

        public override string ToString()
        {
            return $"X: {X} Y: {Y}";
        }

        public virtual void Normalize()
        {
            float magnitude = Magnitude;

            X /= magnitude;
            Y /= magnitude;
        }
    }

}
