```cs

To make a player kneel when they are curently standing send this packet to all players within a 400 foot radius of the player that will be kneeling including that player...

F_UPDATE_STATE (0xE4) [13 Bytes]
{0x00, 0x0A, 0xE4, 0x0B, 0x22, 0x1B, 0x01, 0x00, 0x00, 0x0B, 0x45, 0x00, 0x00}

{0x0B, 0x22} is the oid of the player who will be kneeling down.
{0x1B} is the kneel/stand update state subtype.
{0x01} is the boolean to choose the action kneel rather than stand.
{0x0B, 0x45} is the oid of the corpse to kneel in front of.



To make a player stand when they are currently kneeling send this packet to all players within a 400 foot radius of the player that will be standing including that player...

F_UPDATE_STATE (0xE4) [13 Bytes]
{0x00, 0x0A, 0xE4, 0x0B, 0x22, 0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

{0x0B, 0x22} is the oid of the player who will be kneeling down.
{0x1B} is the kneel/stand update state subtype.
{0x00} is the boolean to choose the action stand rather than kneel.



To show cast bar with targetted objects name as the bar title send this packet to the player who should see the cast bar...

F_SET_ABILITY_TIMER (0x7E) [15 Bytes]
{0x00, 0x0C, 0x7E, 0x00, 0x01, 0x01, 0x05, 0x00, 0x00, 0x07, 0xD0, 0x04, 0xF1, 0x00, 0x00}

{0x04, 0xF1} is the oid of the object from which the name should be used.
{0x07, 0xD0} is the time the cast bar should be running for.
{0x00, 0x00} is the ability id which tells it to use the target objects name instead of an ability.

```