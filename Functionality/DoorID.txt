https://discordapp.com/api/webhooks/538884659623231508/0NPP-x2uhySeaI6s9ddF4GlTSKwiY_nSIz0cBazq5bXZVHPYsoBbnOOwqsfvkCrtEWS6



Find .nif in Art2.Myp

Ex: dw_tower01.nif

Find the first entry for that nif name

Fixtures ID, NIF  #, Textual Name,            X,        Y,        Z,        A,         NIF Scale, Collide, Radius, OnAnimate, Ground, Flip, Cave, Unique ID, 3D Angle, 3D Axis X, 3D Axis Y, 3D Axis Z, Variation, Primary Tint, Secondary Tint, FarDraw, Interior, Keep Status, Mount, Too Steep
3209,        399983, dw_tower01 (Instance 0), 32332.21, 20254.10, 7343.05,  257,       100,       2048,    0,      0,         0,      0,    0,    2926,      1.800000,  0.000000,  0.000000,  1.000000,          ,            0,               0,      0,        1,            0,     0,        0

Get the uniqueID from that line, the zone index and door index from inside the nif ( 0 for door0, 1 for door1 ) then do the math

doorID  = (uint)((uint)((uniqueID >> 14 & 0xFF) << 30) | (uint)(zoneID << 20) | (uint)((uniqueID & 0x1FFF) << 6) | 0x28 + instance);
Ex:
1235880 = (uint)((uint)((2926     >> 14 & 0xFF) << 30) | (uint)(1      << 20) | (uint)((2926     & 0x1FFF) << 6) | 0x28 + 0);

other numbers

    ID, ZoneFixtureID,  DoorIndex,    DoorID,             Name,         X,        Y,      Z, DefaultState, StaticObjectID, ZoneID, InstanceID, UniqueID,  NifID, FixtureID
'3209',      '399983',        '0', '1235880', 'dw_tower01.nif', '1064882', '880330', '7572',          '0',           NULL,    '1',       NULL,   '2926', '9418',    '1297'
 ^           ^                 ^                                [ From fixtures.csv line ]            ^                                                  ^           ^
 |           |                 |                                                                       |                                                  |           |
 |           |                 Door index from inside the nif ( 0 for door0, 1 for door1 )             |                                                  |           |
 |           |                                                                                         |                                                  |           |
 |           Second field in the fixtures.csv line                                                     0 for closed and 1 for open                        |           |
 |                                                                                                                                                        |           |
 |                                                                                                                                                                    |
 |                                                                                                                                                                    |
 Auto increment db index				                            First field in the fixtures.csv line or line number minus two, the index of the fixture in the zone





