// HashingPerformanceTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#define WIN32_LEAN_AND_MEAN

#include <sdkddkver.h>

#include <WinSock2.h>
#include <MSWSock.h>

#include <Windows.h>
#include <wincred.h>

#include <ppl.h>

#include <initializer_list>
#include <numeric>
#include <optional>
#include <tuple>
#include <chrono>
#include <tuple>
#include <typeinfo>
#include <memory>
#include <unordered_map>
#include <experimental/filesystem>
#include <algorithm> 
#include <filesystem>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <functional> 
#include <iostream>
#include <locale>
#include <map>
#include <set>
#include <stack>
#include <string>
#include <string_view>
#include <vector>
#include <utility>
#include <type_traits>
#include <ostream>
#include <unordered_set>
#include <unordered_map>

#define rot(x, k) (((x)<<(k)) | ((x)>>(32-(k))))

#define mix(a, b, c) { \
		a -= c; a ^= rot(c,4);  c += b; \
		b -= a; b ^= rot(a,6);  a += c; \
		c -= b; c ^= rot(b,8);  b += a; \
		a -= c; a ^= rot(c,16); c += b; \
		b -= a; b ^= rot(a,19); a += c; \
		c -= b; c ^= rot(b,4);  b += a; }

#define final(a, b, c) { \
		c ^= b; c -= rot(b,14); \
		a ^= c; a -= rot(c,11); \
		b ^= a; b -= rot(a,25); \
		c ^= b; c -= rot(b,16); \
		a ^= c; a -= rot(c,4);  \
		b ^= a; b -= rot(a,14); \
		c ^= b; c -= rot(b,24); }

static void HashWARv1(const char* path, uint32_t length, uint32_t* pb, uint32_t* pc) {
    uint32_t edx = 0, eax, esi, ebx = 0, ph = 0, sh = 0;
    uint32_t edi, ecx;
    static uint32_t seed = 0xDEADBEEF;
    int len = (uint32_t)strlen(path);

    eax = ecx = edx = ebx = esi = edi = 0;
    ebx = edi = esi = (uint32_t)strlen(path) + seed;

    int i = 0;

    for (i = 0; i + 12 < len; i += 12) {
        edi = (uint32_t)((path[i + 7] << 24) | (path[i + 6] << 16) | (path[i + 5] << 8) | path[i + 4]) + edi;
        esi = (uint32_t)((path[i + 11] << 24) | (path[i + 10] << 16) | (path[i + 9] << 8) | path[i + 8]) + esi;
        edx = (uint32_t)((path[i + 3] << 24) | (path[i + 2] << 16) | (path[i + 1] << 8) | path[i]) - esi;

        edx = (edx + ebx) ^ (esi >> 28) ^ (esi << 4);
        esi += edi;
        edi = (edi - edx) ^ (edx >> 26) ^ (edx << 6);
        edx += esi;
        esi = (esi - edi) ^ (edi >> 24) ^ (edi << 8);
        edi += edx;
        ebx = (edx - esi) ^ (esi >> 16) ^ (esi << 16);
        esi += edi;
        edi = (edi - ebx) ^ (ebx >> 13) ^ (ebx << 19);
        ebx += esi;
        esi = (esi - edi) ^ (edi >> 28) ^ (edi << 4);
        edi += ebx;
    }

    if (len - i > 0) {
        switch (len - i) {
        case 12: esi += (uint32_t)path[i + 11] << 24;
        case 11: esi += (uint32_t)path[i + 10] << 16;
        case 10: esi += (uint32_t)path[i + 9] << 8;
        case 9: esi += (uint32_t)path[i + 8];
        case 8: edi += (uint32_t)path[i + 7] << 24;
        case 7: edi += (uint32_t)path[i + 6] << 16;
        case 6: edi += (uint32_t)path[i + 5] << 8;
        case 5: edi += (uint32_t)path[i + 4];
        case 4: ebx += (uint32_t)path[i + 3] << 24;
        case 3: ebx += (uint32_t)path[i + 2] << 16;
        case 2: ebx += (uint32_t)path[i + 1] << 8;
        case 1: ebx += (uint32_t)path[i]; break;
        }

        esi = (esi ^ edi) - ((edi >> 18) ^ (edi << 14));
        ecx = (esi ^ ebx) - ((esi >> 21) ^ (esi << 11));
        edi = (edi ^ ecx) - ((ecx >> 7) ^ (ecx << 25));
        esi = (esi ^ edi) - ((edi >> 16) ^ (edi << 16));
        edx = (esi ^ ecx) - ((esi >> 28) ^ (esi << 4));
        edi = (edi ^ edx) - ((edx >> 18) ^ (edx << 14));
        eax = (esi ^ edi) - ((edi >> 8) ^ (edi << 24));

        *pb = edi; *pc = eax; return;
    }
    *pb = edi; *pc = eax; return;
}

static int64_t HashWARv1b(const char* path, uint32_t length) {
    uint32_t edx = 0, eax, esi, ebx = 0, ph = 0, sh = 0;
    uint32_t edi, ecx;
    static uint32_t seed = 0xDEADBEEF;
    int len = (uint32_t)length;

    eax = ecx = edx = ebx = esi = edi = 0;
    ebx = edi = esi = (uint32_t)length + seed;

    int i = 0;

    for (i = 0; i + 12 < len; i += 12) {
        edi = (uint32_t)((path[i + 7] << 24) | (path[i + 6] << 16) | (path[i + 5] << 8) | path[i + 4]) + edi;
        esi = (uint32_t)((path[i + 11] << 24) | (path[i + 10] << 16) | (path[i + 9] << 8) | path[i + 8]) + esi;
        edx = (uint32_t)((path[i + 3] << 24) | (path[i + 2] << 16) | (path[i + 1] << 8) | path[i]) - esi;

        edx = (edx + ebx) ^ (esi >> 28) ^ (esi << 4);
        esi += edi;
        edi = (edi - edx) ^ (edx >> 26) ^ (edx << 6);
        edx += esi;
        esi = (esi - edi) ^ (edi >> 24) ^ (edi << 8);
        edi += edx;
        ebx = (edx - esi) ^ (esi >> 16) ^ (esi << 16);
        esi += edi;
        edi = (edi - ebx) ^ (ebx >> 13) ^ (ebx << 19);
        ebx += esi;
        esi = (esi - edi) ^ (edi >> 28) ^ (edi << 4);
        edi += ebx;
    }

    if (len - i > 0) {
        switch (len - i) {
        case 12: esi += (uint32_t)path[i + 11] << 24;
        case 11: esi += (uint32_t)path[i + 10] << 16;
        case 10: esi += (uint32_t)path[i + 9] << 8;
        case 9: esi += (uint32_t)path[i + 8];
        case 8: edi += (uint32_t)path[i + 7] << 24;
        case 7: edi += (uint32_t)path[i + 6] << 16;
        case 6: edi += (uint32_t)path[i + 5] << 8;
        case 5: edi += (uint32_t)path[i + 4];
        case 4: ebx += (uint32_t)path[i + 3] << 24;
        case 3: ebx += (uint32_t)path[i + 2] << 16;
        case 2: ebx += (uint32_t)path[i + 1] << 8;
        case 1: ebx += (uint32_t)path[i]; break;
        }

        esi = (esi ^ edi) - ((edi >> 18) ^ (edi << 14));
        ecx = (esi ^ ebx) - ((esi >> 21) ^ (esi << 11));
        edi = (edi ^ ecx) - ((ecx >> 7) ^ (ecx << 25));
        esi = (esi ^ edi) - ((edi >> 16) ^ (edi << 16));
        edx = (esi ^ ecx) - ((esi >> 28) ^ (esi << 4));
        edi = (edi ^ edx) - ((edx >> 18) ^ (edx << 14));
        eax = (esi ^ edi) - ((edi >> 8) ^ (edi << 24));

        ph = edi;
        sh = eax;
        return ((int64_t)ph << 32) + sh;
    }
    ph = esi;
    sh = eax;
    return ((int64_t)ph << 32) + sh;
}

static void HashWARv2(const void* key, uint32_t length, uint32_t * pb, uint32_t * pc) {
    uint32_t a;
    uint32_t b;
    uint32_t c = b = a = 0xdeadbeef + length;							/* Set up the internal state */

    uint32_t* k = (uint32_t*)key;										/* all but last block: aligned reads and affect 32 bits of (a,b,c) */
    while (length > 12) {
        a += k[0]; b += k[1]; c += k[2];
        mix(a, b, c);
        length -= 12;
        k += 3;
    }

    const uint8_t* k8 = reinterpret_cast<const uint8_t*>(k);
    switch (length) {													/* handle the last (probably partial) block */
    case 12: c += k[2]; b += k[1]; a += k[0]; break;
    case 11: c += ((uint32_t)k8[10]) << 16;							/* fall through */
    case 10: c += ((uint32_t)k8[9]) << 8;							/* fall through */
    case 9: c += k8[8];												/* fall through */
    case 8: b += k[1]; a += k[0]; break;
    case 7: b += ((uint32_t)k8[6]) << 16;							/* fall through */
    case 6: b += ((uint32_t)k8[5]) << 8;							/* fall through */
    case 5: b += k8[4];												/* fall through */
    case 4: a += k[0]; break;
    case 3: a += ((uint32_t)k8[2]) << 16;							/* fall through */
    case 2: a += ((uint32_t)k8[1]) << 8;							/* fall through */
    case 1: a += k8[0]; break;
    case 0: *pc = c; *pb = b; return;								/* zero length strings require no mixing */
    };

    final(a, b, c);
    *pc = c; *pb = b;
};

static int64_t HashWARv2b(const void* key, uint32_t length) {
    uint32_t a;
    uint32_t b;
    uint32_t c = b = a = 0xdeadbeef + length;							/* Set up the internal state */

    uint32_t* k = (uint32_t*)key;										/* all but last block: aligned reads and affect 32 bits of (a,b,c) */
    while (length > 12) {
        a += k[0]; b += k[1]; c += k[2];
        mix(a, b, c);
        length -= 12;
        k += 3;
    }

    const uint8_t* k8 = reinterpret_cast<const uint8_t*>(k);
    switch (length) {													/* handle the last (probably partial) block */
    case 12: c += k[2]; b += k[1]; a += k[0]; break;
    case 11: c += ((uint32_t)k8[10]) << 16;							    /* fall through */
    case 10: c += ((uint32_t)k8[9]) << 8;							    /* fall through */
    case 9: c += k8[8];												    /* fall through */
    case 8: b += k[1]; a += k[0]; break;
    case 7: b += ((uint32_t)k8[6]) << 16;							    /* fall through */
    case 6: b += ((uint32_t)k8[5]) << 8;							    /* fall through */
    case 5: b += k8[4];												    /* fall through */
    case 4: a += k[0]; break;
    case 3: a += ((uint32_t)k8[2]) << 16;							    /* fall through */
    case 2: a += ((uint32_t)k8[1]) << 8;							    /* fall through */
    case 1: a += k8[0]; break;
    case 0: return (((int64_t)b) << 32) | c;						    /* zero length strings require no mixing */
    };

    final(a, b, c);

    return (((int64_t)b) << 32) | c;
};

static void HashWARv3(const void* key, size_t length, uint32_t * pb, uint32_t * pc) {
    uint32_t a;
    uint32_t b;
    uint32_t c = b = a = 0xdeadbeef + ((uint32_t)length);				/* Set up the internal state */

    uint32_t* k = (uint32_t*)key;										/* all but last block: aligned reads and affect 32 bits of (a,b,c) */
    while (length > 12) {
        a += k[0]; b += k[1]; c += k[2];
        mix(a, b, c);
        length -= 12;
        k += 3;
    }

    switch (length) {													/* handle the last (probably partial) block */
    case 12: { c += k[2]; b += k[1]; a += k[0]; break; }
    case 11: { c += k[2] & 0xffffff; b += k[1]; a += k[0]; break; }
    case 10: { c += k[2] & 0xffff; b += k[1]; a += k[0]; break; }
    case 9: { c += k[2] & 0xff; b += k[1]; a += k[0]; break; }
    case 8: { b += k[1]; a += k[0]; break; }
    case 7: { b += k[1] & 0xffffff; a += k[0]; break; }
    case 6: { b += k[1] & 0xffff; a += k[0]; break; }
    case 5: { b += k[1] & 0xff; a += k[0]; break; }
    case 4: { a += k[0]; break; }
    case 3: { a += k[0] & 0xffffff; break; }
    case 2: { a += k[0] & 0xffff; break; }
    case 1: { a += k[0] & 0xff; break; }
    case 0: { *pc = c; *pb = b;	return;	}							/* zero length strings require no mixing */
    }

    final(a, b, c);
    *pc = c; *pb = b;
}

static int64_t HashWARv3b(const void* key, size_t length) {
    uint32_t a;
    uint32_t b;
    uint32_t c = b = a = 0xdeadbeef + ((uint32_t)length);				/* Set up the internal state */

    uint32_t* k = (uint32_t*)key;										/* all but last block: aligned reads and affect 32 bits of (a,b,c) */
    while (length > 12) {
        a += k[0]; b += k[1]; c += k[2];
        mix(a, b, c);
        length -= 12;
        k += 3;
    }

    switch (length) {													/* handle the last (probably partial) block */
    case 12: { c += k[2]; b += k[1]; a += k[0]; break; }
    case 11: { c += k[2] & 0xffffff; b += k[1]; a += k[0]; break; }
    case 10: { c += k[2] & 0xffff; b += k[1]; a += k[0]; break; }
    case 9: { c += k[2] & 0xff; b += k[1]; a += k[0]; break; }
    case 8: { b += k[1]; a += k[0]; break; }
    case 7: { b += k[1] & 0xffffff; a += k[0]; break; }
    case 6: { b += k[1] & 0xffff; a += k[0]; break; }
    case 5: { b += k[1] & 0xff; a += k[0]; break; }
    case 4: { a += k[0]; break; }
    case 3: { a += k[0] & 0xffffff; break; }
    case 2: { a += k[0] & 0xffff; break; }
    case 1: { a += k[0] & 0xff; break; }
    case 0: { return (((int64_t)b) << 32) | c; }						/* zero length strings require no mixing */
    }

    final(a, b, c);
    return (((int64_t)b) << 32) | c;
}

static int64_t HashWARv4(const std::string & s) {

    uint32_t length = (uint32_t)s.length();
    uint32_t* k = (uint32_t*)s.c_str();

    uint32_t a, b, c = b = a = 0xdeadbeef + length;                         /* Set up the internal state */


    while (length > 12) {                                             /* all but last block: aligned reads and affect 32 bits of (a,b,c) */
        a += k[0]; b += k[1]; c += k[2];
        mix(a, b, c);
        k += 3;
        length -= 12;
    }

    switch (length) {													    /* handle the last (probably partial) block */
    case 12: { c += k[2]; b += k[1]; a += k[0]; break; }
    case 11: { c += k[2] & 0xffffff; b += k[1]; a += k[0]; break; }
    case 10: { c += k[2] & 0xffff; b += k[1]; a += k[0]; break; }
    case 9: { c += k[2] & 0xff; b += k[1]; a += k[0]; break; }
    case 8: { b += k[1]; a += k[0]; break; }
    case 7: { b += k[1] & 0xffffff; a += k[0]; break; }
    case 6: { b += k[1] & 0xffff; a += k[0]; break; }
    case 5: { b += k[1] & 0xff; a += k[0]; break; }
    case 4: { a += k[0]; break; }
    case 3: { a += k[0] & 0xffffff; break; }
    case 2: { a += k[0] & 0xffff; break; }
    case 1: { a += k[0] & 0xff; break; }
    case 0: { return (((int64_t)b) << 32) | c; }						    /* zero length strings require no mixing */
    }

    final(a, b, c);
    return (((int64_t)b) << 32) | c;
}

template< typename T>
static std::string Hexify(T uhash)
{
    std::stringstream ss;
    ss << " ? 0x" << std::setfill('0') << std::setw(2) << std::hex;

    auto buffer = std::vector<char>(reinterpret_cast<char*>(&uhash), reinterpret_cast<char*>(&uhash) + sizeof(T));//reinterpret_cast<char*>(&uhash);

   // OutputDebugStringA((char*)&buffer);

    std::for_each(buffer.begin(), buffer.end(), [&](const char& n) { ss << n; });//   [](char& i) { ss << i; }));

    return ss.str();
}

int main(int argc, char** argv)
{
    int64_t hash = 0;

    double v1t = 0.0;
    double v1tb = 0.0;
    double v2t = 0.0;
    double v2tb = 0.0;
    double v3t = 0.0;
    double v3tb = 0.0;
    double v4t = 0.0;
    double v4tb = 0.0;

    uint32_t rhs = 0;
    uint32_t lhs = 0;

    size_t loops = 64;

    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::time_point();
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::time_point();
    std::chrono::high_resolution_clock::duration duration = std::chrono::high_resolution_clock::duration();

    std::ifstream asset_paths("asset_paths.txt");
    std::unordered_set<std::string> hashSet;
    std::copy(std::istream_iterator<std::string>(asset_paths), std::istream_iterator<std::string>(), std::inserter(hashSet, hashSet.end()));

    //std::string str = "eamythic.lnk";
    //hash = HashWARv3b(str.c_str(), str.length());
    //std::cout << str << "  :  " << hash << std::endl;

    //str = "interface/interface.xsd";
    //hash = HashWARv3b(str.c_str(), str.length());
    //std::cout << str << "  :  " << hash << std::endl;

    //std::string str = "interface.xsd";
    //hash = HashWARv3b(str.c_str(), str.length());
    //std::cout << str << "  :  " << hash << std::endl;

    for (int i = 0; i < loops; ++i) {

        t1 = std::chrono::high_resolution_clock::now();
        for (const auto& path : hashSet) {
            HashWARv1(path.c_str(), path.length(), &rhs, &lhs);
            hash = ((int64_t)lhs << 32) + rhs;
        }
        t2 = std::chrono::high_resolution_clock::now();
        v1t += 0.000001 * std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();

        t1 = std::chrono::high_resolution_clock::now();
        for (const auto& path : hashSet) {
            hash = HashWARv1b(path.c_str(), path.length());
        }
        t2 = std::chrono::high_resolution_clock::now();
        v1tb += 0.000001 * std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();

        t1 = std::chrono::high_resolution_clock::now();
        for (const auto& path : hashSet) {
            HashWARv2(path.c_str(), path.length(), &rhs, &lhs);
            hash = ((int64_t)lhs << 32) + rhs;
        }
        t2 = std::chrono::high_resolution_clock::now();
        v2t += 0.000001 * std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();

        t1 = std::chrono::high_resolution_clock::now();
        for (const auto& path : hashSet) {
            hash = HashWARv2b(path.c_str(), path.length());
        }
        t2 = std::chrono::high_resolution_clock::now();
        v2tb += 0.000001 * std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();

        t1 = std::chrono::high_resolution_clock::now();
        for (const auto& path : hashSet) {
            HashWARv3(path.c_str(), path.length(), &rhs, &lhs);
            hash = ((int64_t)lhs << 32) + rhs;
        }
        t2 = std::chrono::high_resolution_clock::now();
        v3t += 0.000001 * std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();

        t1 = std::chrono::high_resolution_clock::now();
        for (const auto& path : hashSet) {
            hash = HashWARv3b(path.c_str(), path.length());
        }
        t2 = std::chrono::high_resolution_clock::now();
        v3tb += 0.000001 * std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();

        t1 = std::chrono::high_resolution_clock::now();
        for (const auto& path : hashSet) {
            hash = HashWARv4(path);
        }
        t2 = std::chrono::high_resolution_clock::now();
        v4t += 0.000001 * std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
    }

    std::cout << "HashWARv1 ~ " << v1t / loops << "ms ? LastHash:" << hash << std::endl;
    std::cout << "HashWARv1b ~ " << v1tb / loops << "ms ? LastHash:" << hash << std::endl;
    std::cout << "HashWARv2 ~ " << v2t / loops << "ms ? LastHash:" << hash << std::endl;
    std::cout << "HashWARv2b ~ " << v2tb / loops << "ms ? LastHash:" << hash << std::endl;
    std::cout << "HashWARv3 ~ " << v3t / loops << "ms ? LastHash:" << hash << std::endl;
    std::cout << "HashWARv3b ~ " << v3tb / loops << "ms ? LastHash:" << hash << std::endl;
    std::cout << "HashWARv4 ~ " << v4t / loops << "ms ? LastHash:" << hash << std::endl;

    std::string path("patch140.txt");

    std::cout << hash << std::endl;

    HashWARv1(path.c_str(), path.length(), &rhs, &lhs);
    hash = ((int64_t)lhs << 32) + rhs;
    std::cout << hash << std::endl;

    hash = HashWARv1b(path.c_str(), path.length());
    std::cout << hash << std::endl;

    HashWARv2(path.c_str(), path.length(), &rhs, &lhs);
    hash = ((int64_t)lhs << 32) + rhs;
    std::cout << hash << std::endl;

    hash = HashWARv2b(path.c_str(), path.length());
    std::cout << hash << std::endl;

    HashWARv3(path.c_str(), path.length(), &rhs, &lhs);
    hash = ((int64_t)lhs << 32) + rhs;
    std::cout << hash << std::endl;

    hash = HashWARv3b(path.c_str(), path.length());
    std::cout << hash << std::endl;

    hash = HashWARv4(path);
    std::cout << hash << std::endl;

    std::getchar();
}
