﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using WarFigleaf;
using WarFigleaf.Structs;

namespace FigleafReader
{
    internal class MySQLHandler
    {
        public string _hostname;
        public string _username;
        public string _password;
        public string _database;
        public string _connection_string;
        private MySqlConnection _connection;
        private MySqlCommand _command;
        private MySqlTransaction _transaction;

        public enum SelectedTable
        {
            Strings1 = 0,
            Strings2 = 1,
            CharacterArt = 2,
            CharacterArtData = 18,
            FigureParts = 3,
            ArtSwitches = 4,
            CharacterDecals = 5,
            CharacterMeshes = 6,
            Textures = 7,
            Unk8 = 8,
            Fixtures = 9,
            Lightmaps = 10,
            Patches = 11,
            Regions = 12,
            VAR_RECS1 = 13,
            VAR_RECS2 = 14,
            VAR_RECS3 = 15,
            VAR_RECS4 = 16,
            VAR_RECS5 = 17,
        }

        public MySQLHandler(string hostname, string username, string password, string database)
        {
            _hostname = hostname;
            _username = username;
            _password = password;
            _database = database;
            _connection_string = string.Empty;
            _connection = null;
        }
        public void BeginTransaction()
        {
            try
            {
                _command = _connection.CreateCommand();
                _transaction = _connection.BeginTransaction();
            }
            catch (MySqlException ex) { Console.WriteLine(ex.Message); }
        }
        public void CommitTransaction()
        {
            try
            {
                _transaction.Commit();
            }
            catch (MySqlException ex) { Console.WriteLine(ex.Message); }
        }
        public void Connect()
        {
            _connection_string = "server=" + _hostname + ";uid=" + _username + ";pwd=" + _password + ";database=" + _database;
            try
            {
                _connection = new MySqlConnection(_connection_string);
                _connection.Open();
            }
            catch (MySqlException ex) { Console.WriteLine(ex.Message); }
        }
        public void DropTable(string table_name)
        {
            MySqlCommand myCommand = new MySqlCommand("", _connection)
            {
                CommandText = "DROP TABLE IF EXISTS `" + _database + "`.`" + table_name + "`;"
            };

            try
            {
                myCommand.ExecuteNonQuery();
            }
            catch (MySqlException ex) { Console.WriteLine(myCommand.CommandText); Console.WriteLine(ex.Message); }
        }
        public void CreateTable(SelectedTable table_type)
        {
            MySqlCommand myCommand = new MySqlCommand("", _connection);

            switch (table_type)
            {
                case SelectedTable.Strings1:
                {
                    DropTable("Strings1");
                    myCommand.CommandText = "CREATE TABLE `" + _database + "`.`Strings1` ("
                        + "`Index` INT NULL, "
                        + "`Unk1` INT UNSIGNED NULL, "
                        + "`Type` TINYINT UNSIGNED NULL, "
                        + "`Unk3` TINYINT UNSIGNED NULL, "
                        + "`Value` TEXT NULL);";
                    break;
                }
                case SelectedTable.Strings2:
                {
                    DropTable("Strings2");
                    myCommand.CommandText = "CREATE TABLE `" + _database + "`.`Strings2` ("
                        + "`Index` INT NULL, "
                        + "`Value` TEXT NULL);";
                    break;
                }
                case SelectedTable.CharacterArt:
                {
                    DropTable("CharacterArt");
                    myCommand.CommandText = "CREATE TABLE `" + _database + "`.`CharacterArt` ("
                        + "`Index` INT UNSIGNED NULL, "
                        + "`SourceIndex` INT UNSIGNED NULL, "
                        + "`Unk1` INT UNSIGNED NULL, "
                        + "`Unk2` INT UNSIGNED NULL, "
                        + "`Unk3` INT UNSIGNED NULL, "
                        + "`Race` INT UNSIGNED NULL, "
                        + "`Gender` INT UNSIGNED NULL, "
                        + "`Unk6` INT UNSIGNED NULL, "
                        + "`Unk7` INT UNSIGNED NULL, "
                        + "`Animations` INT UNSIGNED NULL, "
                        + "`Unk9` INT UNSIGNED NULL, "
                        + "`PartCount` INT UNSIGNED NULL, "
                        + "`DataStart` INT UNSIGNED NULL, "
                        + "`Unk12` INT UNSIGNED NULL, "
                        + "`DataStart2` INT UNSIGNED NULL, "
                        + "`Unk14` INT UNSIGNED NULL, "
                        + "`DataEnd` INT UNSIGNED NULL, "
                        + "`Unk16` INT UNSIGNED NULL, "
                        + "`DataStart4` INT UNSIGNED NULL);";
                    break;
                }
                case SelectedTable.CharacterArtData:
                {
                    DropTable("CharacterArtData");
                    myCommand.CommandText = "CREATE TABLE `" + _database + "`.`CharacterArtData` ("
                        + "`Index` INT UNSIGNED NULL, "
                        + "`SourceIndex` INT UNSIGNED NULL, "
                        + "`A1` INT UNSIGNED NULL, "
                        + "`A2` INT UNSIGNED NULL, "
                        + "`A3` INT UNSIGNED NULL, "
                        + "`A4` INT UNSIGNED NULL, "
                        + "`A5` INT UNSIGNED NULL, "
                        + "`A6` INT UNSIGNED NULL, "
                        + "`A7` INT UNSIGNED NULL, "
                        + "`A8` INT UNSIGNED NULL, "
                        + "`A9` INT UNSIGNED NULL);";
                    break;
                }
                case SelectedTable.FigureParts:
                {
                    DropTable("FigureParts");
                    myCommand.CommandText = "CREATE TABLE `" + _database + "`.`FigureParts` ("
                        + "`Index` INT UNSIGNED NULL, "
                        + "`SourceIndex` INT UNSIGNED NULL, "
                        + "`Unk1a` SMALLINT UNSIGNED NULL, "
                        + "`Unk1ba` TINYINT UNSIGNED NULL, "
                        + "`Unk1bb` TINYINT UNSIGNED NULL, "
                        + "`Unk2a` SMALLINT UNSIGNED NULL, "
                        + "`Unk2b` SMALLINT UNSIGNED NULL, "
                        + "`Unk3` INT UNSIGNED NULL, "
                        + "`Unk4` INT UNSIGNED NULL, "
                        + "`Unk5` INT UNSIGNED NULL, "
                        + "`Count` INT UNSIGNED NULL, "
                        + "`DataStart` INT NULL, "
                        + "`Geometry` INT UNSIGNED NULL, "
                        + "`DataEnd` INT NULL, "
                        + "`Attachments` INT UNSIGNED NULL, "
                        + "`Unk11` INT UNSIGNED NULL);";
                    break;
                }
                case SelectedTable.ArtSwitches:
                {
                    DropTable("ArtSwitches");
                    myCommand.CommandText = "CREATE TABLE `" + _database + "`.`ArtSwitches` ("
                        + "`SwitchType` INT UNSIGNED NULL, "
                        + "`SourceIndex` INT UNSIGNED NULL, "
                        + "`Unk2` INT UNSIGNED NULL, "
                        + "`Unk3` INT UNSIGNED NULL, "
                        + "`Unk4` INT UNSIGNED NULL, "
                        + "`Other` INT UNSIGNED NULL, "
                        + "`Male` INT UNSIGNED NULL, "
                        + "`Female` INT UNSIGNED NULL, "
                        + "`DwarfOther` INT UNSIGNED NULL, "
                        + "`DwarfMale` INT UNSIGNED NULL, "
                        + "`DwarfFemale` INT UNSIGNED NULL, "
                        + "`HumanOther` INT UNSIGNED NULL, "
                        + "`HumanMale` INT UNSIGNED NULL, "
                        + "`HumanFemale` INT UNSIGNED NULL, "
                        + "`ChaosHumanOther` INT UNSIGNED NULL, "
                        + "`ChaosHumanMale` INT UNSIGNED NULL, "
                        + "`ChaosHumanFemale` INT UNSIGNED NULL, "
                        + "`ElfOther` INT UNSIGNED NULL, "
                        + "`ElfMale` INT UNSIGNED NULL, "
                        + "`ElfFemale` INT UNSIGNED NULL, "
                        + "`DarkElfOther` INT UNSIGNED NULL, "
                        + "`DarkElfMale` INT UNSIGNED NULL, "
                        + "`DarkElfFemale` INT UNSIGNED NULL, "
                        + "`OrcOther` INT UNSIGNED NULL, "
                        + "`OrcMale` INT UNSIGNED NULL, "
                        + "`OrcFemale` INT UNSIGNED NULL, "
                        + "`GoblinOther` INT UNSIGNED NULL, "
                        + "`GoblinMale` INT UNSIGNED NULL, "
                        + "`GoblinFemale` INT UNSIGNED NULL, "
                        + "`BeastmanOther` INT UNSIGNED NULL, "
                        + "`BeastmanMale` INT UNSIGNED NULL, "
                        + "`BeastmanFemale` INT UNSIGNED NULL, "
                        + "`SkavenOther` INT UNSIGNED NULL, "
                        + "`SkavenMale` INT UNSIGNED NULL, "
                        + "`SkavenFemale` INT UNSIGNED NULL, "
                        + "`OgreOther` INT UNSIGNED NULL, "
                        + "`OgreMale` INT UNSIGNED NULL, "
                        + "`OgreFemale` INT UNSIGNED NULL, "
                        + "`ChaosWarriorOther` INT UNSIGNED NULL, "
                        + "`ChaosWarriorMale` INT UNSIGNED NULL, "
                        + "`ChaosWarriorFemale` INT UNSIGNED NULL);";
                    break;
                }
                default:
                    return;
            }

            try
            {
                myCommand.ExecuteNonQuery();
            }
            catch (MySqlException ex) { Console.WriteLine(myCommand.CommandText); Console.WriteLine(ex.Message); }
        }
        public void InsertRow(FigString data)
        {
            _command.CommandText = " INSERT INTO `" + _database + "`.`strings1` (`Index`,`Unk1`,`Type`,`Unk3`,`Value`) VALUES (?Index,?Unk1,?Type,?Unk3,?Value);";

            _command.Parameters.Clear();
            _command.Parameters.Add("?Index", MySqlDbType.Int32).Value = data.Index;
            _command.Parameters.Add("?Unk1", MySqlDbType.UInt32).Value = data.Unk1;
            _command.Parameters.Add("?Type", MySqlDbType.UByte).Value = data.Type;
            _command.Parameters.Add("?Unk3", MySqlDbType.UByte).Value = data.Unk3;
            _command.Parameters.Add("?Value", MySqlDbType.Text).Value = data.Value;

            try
            {
                _command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { Console.WriteLine(_command.CommandText); Console.WriteLine(ex.Message); }
        }
        public void InsertRow(FigString2 data)
        {
            _command.CommandText = " INSERT INTO `" + _database + "`.`strings2` (`Index`,`Value`) VALUES (?Index,?Value);";

            _command.Parameters.Clear();
            _command.Parameters.Add("?Index", MySqlDbType.Int32).Value = data.Index;
            _command.Parameters.Add("?Value", MySqlDbType.Text).Value = data.Value;

            try
            {
                _command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { Console.WriteLine(_command.CommandText); Console.WriteLine(ex.Message); }
        }
        public void InsertRow(CharacterArtView data)
        {
            _command.CommandText = " INSERT INTO `" + _database + "`.`CharacterArt` (`Index`,`SourceIndex`,`Unk1`,`Unk2`,`Unk3`,`Race`,`Gender`,`Unk6`,`Unk7`,`Animations`,`PartCount`,`DataStart`,`Unk12`,`DataStart2`,`Unk14`,`DataEnd`,`Unk16`,`DataStart4`) VALUES (?Index,?SourceIndex,?Unk1,?Unk2,?Unk3,?Race,?Gender,?Unk6,?Unk7,?Animations,?PartCount,?DataStart,?Unk12,?DataStart2,?Unk14,?DataEnd,?Unk16,?DataStart4);";

            _command.Parameters.Clear();
            _command.Parameters.Add("?Index", MySqlDbType.UInt32).Value = data.Index;
            _command.Parameters.Add("?SourceIndex", MySqlDbType.UInt32).Value = data.SourceIndex;
            _command.Parameters.Add("?Unk1", MySqlDbType.UInt32).Value = data.Unk1;
            _command.Parameters.Add("?Unk2", MySqlDbType.UInt32).Value = data.Unk2;
            _command.Parameters.Add("?Unk3", MySqlDbType.UInt32).Value = data.Unk3;
            _command.Parameters.Add("?Race", MySqlDbType.UInt32).Value = data.Race;
            _command.Parameters.Add("?Gender", MySqlDbType.UInt32).Value = data.Gender;
            _command.Parameters.Add("?Unk6", MySqlDbType.UInt32).Value = data.Unk6;
            _command.Parameters.Add("?Unk7", MySqlDbType.UInt32).Value = data.Unk7;
            _command.Parameters.Add("?Animations", MySqlDbType.UInt32).Value = data.Animations;
            _command.Parameters.Add("?PartCount", MySqlDbType.UInt32).Value = data.PartCount;
            _command.Parameters.Add("?DataStart", MySqlDbType.UInt32).Value = data.DataStart;
            _command.Parameters.Add("?Unk12", MySqlDbType.UInt32).Value = data.Unk12;
            _command.Parameters.Add("?DataStart2", MySqlDbType.UInt32).Value = data.DataStart2;
            _command.Parameters.Add("?Unk14", MySqlDbType.UInt32).Value = data.Unk14;
            _command.Parameters.Add("?DataEnd", MySqlDbType.UInt32).Value = data.DataEnd;
            _command.Parameters.Add("?Unk16", MySqlDbType.UInt32).Value = data.Unk16;
            _command.Parameters.Add("?DataStart4", MySqlDbType.UInt32).Value = data.DataStart4;
            try
            {
                _command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { Console.WriteLine(_command.CommandText); Console.WriteLine(ex.Message); }
        }
        public void InsertRow(uint index, int source_index, CharacterArtData data)
        {
            _command.CommandText = " INSERT INTO `" + _database + "`.`CharacterArtData` (`Index`,`SourceIndex`,`A1`,`A2`,`A3`,`A4`,`A5`,`A6`,`A7`,`A8`,`A9`) VALUES (?Index,?SourceIndex,?A1,?A2,?A3,?A4,?A5,?A6,?A7,?A8,?A9);";

            _command.Parameters.Clear();
            _command.Parameters.Add("?Index", MySqlDbType.UInt32).Value = index;
            _command.Parameters.Add("?SourceIndex", MySqlDbType.UInt32).Value = source_index;
            _command.Parameters.Add("?A1", MySqlDbType.UInt32).Value = data.A1;
            _command.Parameters.Add("?A2", MySqlDbType.UInt32).Value = data.A2;
            _command.Parameters.Add("?A3", MySqlDbType.UInt32).Value = data.A3;
            _command.Parameters.Add("?A4", MySqlDbType.UInt32).Value = data.A4;
            _command.Parameters.Add("?A5", MySqlDbType.UInt32).Value = data.A5;
            _command.Parameters.Add("?A6", MySqlDbType.UInt32).Value = data.A6;
            _command.Parameters.Add("?A7", MySqlDbType.UInt32).Value = data.A7;
            _command.Parameters.Add("?A8", MySqlDbType.UInt32).Value = data.A8;
            _command.Parameters.Add("?A9", MySqlDbType.UInt32).Value = data.A9;

            try
            {
                _command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { Console.WriteLine(_command.CommandText); Console.WriteLine(ex.Message); }
        }
        public void InsertRow(FigurePartsView data)
        {
            _command.CommandText = " INSERT INTO `" + _database + "`.`FigureParts` (`Index`,`SourceIndex`,`Unk1a`,`Unk1ba`,`Unk1bb`,`Unk3`,`Unk4`,`Unk5`,`Count`,`DataStart`,`Geometry`,`DataEnd`,`Attachments`,`Unk11`) VALUES (?Index,?SourceIndex,?Unk1a,?Unk1ba,?Unk1bb,?Unk3,?Unk4,?Unk5,?Count,?DataStart,?Geometry,?DataEnd,?Attachments,?Unk11);";

            _command.Parameters.Clear();
            _command.Parameters.Add("?Index", MySqlDbType.UInt32).Value = data.Index;
            _command.Parameters.Add("?SourceIndex", MySqlDbType.UInt32).Value = data.SourceIndex;
            _command.Parameters.Add("?Unk1a", MySqlDbType.UInt16).Value = data.Unk1a;
            _command.Parameters.Add("?Unk1ba", MySqlDbType.UByte).Value = data.Unk1ba;
            _command.Parameters.Add("?Unk1bb", MySqlDbType.UByte).Value = data.Unk1bb;
            _command.Parameters.Add("?Unk3", MySqlDbType.UInt32).Value = data.Unk3;
            _command.Parameters.Add("?Unk4", MySqlDbType.UInt32).Value = data.Unk4;
            _command.Parameters.Add("?Unk5", MySqlDbType.UInt32).Value = data.Unk5;
            _command.Parameters.Add("?Count", MySqlDbType.UInt32).Value = data.Count;
            _command.Parameters.Add("?DataStart", MySqlDbType.Int32).Value = data.DataStart;
            _command.Parameters.Add("?Geometry", MySqlDbType.UInt32).Value = data.Geometry;
            _command.Parameters.Add("?DataEnd", MySqlDbType.Int32).Value = data.DataEnd;
            _command.Parameters.Add("?Attachments", MySqlDbType.UInt32).Value = data.Attachments;
            _command.Parameters.Add("?Unk11", MySqlDbType.UInt32).Value = data.Unk11;
            try
            {
                _command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { Console.WriteLine(_command.CommandText); Console.WriteLine(ex.Message); }
        }
        public void InsertRow(ArtSwitches data)
        {
            _command.CommandText = " INSERT INTO `" + _database + "`.`ArtSwitches` (`SwitchType`, `SourceIndex`, `Unk2`, `Unk3`, `Unk4`, `Other`, `Male`, `Female`, `DwarfOther`, `DwarfMale`, `DwarfFemale`, `HumanOther`, `HumanMale`, `HumanFemale`, `ChaosHumanOther`, `ChaosHumanMale`, `ChaosHumanFemale`, `ElfOther`, `ElfMale`, `ElfFemale`, `DarkElfOther`, `DarkElfMale`, `DarkElfFemale`, `OrcOther`, `OrcMale`, `OrcFemale`, `GoblinOther`, `GoblinMale`, `GoblinFemale`, `BeastmanOther`, `BeastmanMale`, `BeastmanFemale`, `SkavenOther`, `SkavenMale`, `SkavenFemale`, `OgreOther`, `OgreMale`, `OgreFemale`, `ChaosWarriorOther`, `ChaosWarriorMale`, `ChaosWarriorFemale`) VALUES (?SwitchType, ?SourceIndex, ?Unk2, ?Unk3, ?Unk4, ?Other, ?Male, ?Female, ?DwarfOther, ?DwarfMale, ?DwarfFemale, ?HumanOther, ?HumanMale, ?HumanFemale, ?ChaosHumanOther, ?ChaosHumanMale, ?ChaosHumanFemale, ?ElfOther, ?ElfMale, ?ElfFemale, ?DarkElfOther, ?DarkElfMale, ?DarkElfFemale, ?OrcOther, ?OrcMale, ?OrcFemale, ?GoblinOther, ?GoblinMale, ?GoblinFemale, ?BeastmanOther, ?BeastmanMale, ?BeastmanFemale, ?SkavenOther, ?SkavenMale, ?SkavenFemale, ?OgreOther, ?OgreMale, ?OgreFemale, ?ChaosWarriorOther, ?ChaosWarriorMale, ?ChaosWarriorFemale);";

            _command.Parameters.Clear();
    		_command.Parameters.Add("?SwitchType", MySqlDbType.UInt32).Value = data.SwitchType;
            _command.Parameters.Add("?SourceIndex", MySqlDbType.UInt32).Value = data.SourceIndex;
            _command.Parameters.Add("?Unk2", MySqlDbType.UInt32).Value = data.Unk2;
            _command.Parameters.Add("?Unk3", MySqlDbType.UInt32).Value = data.Unk3;
            _command.Parameters.Add("?Unk4", MySqlDbType.UInt32).Value = data.Unk4;
            _command.Parameters.Add("?Other", MySqlDbType.UInt32).Value = data.Other;
            _command.Parameters.Add("?Male", MySqlDbType.UInt32).Value = data.Male;
            _command.Parameters.Add("?Female", MySqlDbType.UInt32).Value = data.Female;
            _command.Parameters.Add("?DwarfOther", MySqlDbType.UInt32).Value = data.DwarfOther;
            _command.Parameters.Add("?DwarfMale", MySqlDbType.UInt32).Value = data.DwarfMale;
            _command.Parameters.Add("?DwarfFemale", MySqlDbType.UInt32).Value = data.DwarfFemale;
            _command.Parameters.Add("?HumanOther", MySqlDbType.UInt32).Value = data.HumanOther;
            _command.Parameters.Add("?HumanMale", MySqlDbType.UInt32).Value = data.HumanMale;
            _command.Parameters.Add("?HumanFemale", MySqlDbType.UInt32).Value = data.HumanFemale;
            _command.Parameters.Add("?ChaosHumanOther", MySqlDbType.UInt32).Value = data.ChaosHumanOther;
            _command.Parameters.Add("?ChaosHumanMale", MySqlDbType.UInt32).Value = data.ChaosHumanMale;
            _command.Parameters.Add("?ChaosHumanFemale", MySqlDbType.UInt32).Value = data.ChaosHumanFemale;
            _command.Parameters.Add("?ElfOther", MySqlDbType.UInt32).Value = data.ElfOther;
            _command.Parameters.Add("?ElfMale", MySqlDbType.UInt32).Value = data.ElfMale;
            _command.Parameters.Add("?ElfFemale", MySqlDbType.UInt32).Value = data.ElfFemale;
            _command.Parameters.Add("?DarkElfOther", MySqlDbType.UInt32).Value = data.DarkElfOther;
            _command.Parameters.Add("?DarkElfMale", MySqlDbType.UInt32).Value = data.DarkElfMale;
            _command.Parameters.Add("?DarkElfFemale", MySqlDbType.UInt32).Value = data.DarkElfFemale;
            _command.Parameters.Add("?OrcOther", MySqlDbType.UInt32).Value = data.OrcOther;
            _command.Parameters.Add("?OrcMale", MySqlDbType.UInt32).Value = data.OrcMale;
            _command.Parameters.Add("?OrcFemale", MySqlDbType.UInt32).Value = data.OrcFemale;
            _command.Parameters.Add("?GoblinOther", MySqlDbType.UInt32).Value = data.GoblinOther;
            _command.Parameters.Add("?GoblinMale", MySqlDbType.UInt32).Value = data.GoblinMale;
            _command.Parameters.Add("?GoblinFemale", MySqlDbType.UInt32).Value = data.GoblinFemale;
            _command.Parameters.Add("?BeastmanOther", MySqlDbType.UInt32).Value = data.BeastmanOther;
            _command.Parameters.Add("?BeastmanMale", MySqlDbType.UInt32).Value = data.BeastmanMale;
            _command.Parameters.Add("?BeastmanFemale", MySqlDbType.UInt32).Value = data.BeastmanFemale;
            _command.Parameters.Add("?SkavenOther", MySqlDbType.UInt32).Value = data.SkavenOther;
            _command.Parameters.Add("?SkavenMale", MySqlDbType.UInt32).Value = data.SkavenMale;
            _command.Parameters.Add("?SkavenFemale", MySqlDbType.UInt32).Value = data.SkavenFemale;
            _command.Parameters.Add("?OgreOther", MySqlDbType.UInt32).Value = data.OgreOther;
            _command.Parameters.Add("?OgreMale", MySqlDbType.UInt32).Value = data.OgreMale;
            _command.Parameters.Add("?OgreFemale", MySqlDbType.UInt32).Value = data.OgreFemale;
            _command.Parameters.Add("?ChaosWarriorOther", MySqlDbType.UInt32).Value = data.ChaosWarriorOther;
            _command.Parameters.Add("?ChaosWarriorMale", MySqlDbType.UInt32).Value = data.ChaosWarriorMale;
            _command.Parameters.Add("?ChaosWarriorFemale", MySqlDbType.UInt32).Value = data.ChaosWarriorFemale;
            try
            {
                _command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { Console.WriteLine(_command.CommandText); Console.WriteLine(ex.Message); }
        }

        //public SwitchType SwitchType;
        //public uint SourceIndex;
        //public uint Unk2;
        //public uint Unk3;
        //public uint Unk4;
        //public uint Other;
        //public uint Male;
        //public uint Female;
        //public uint DwarfOther;
        //public uint DwarfMale;
        //public uint DwarfFemale;
        //public uint HumanOther;
        //public uint HumanMale;
        //public uint HumanFemale;
        //public uint ChaosHumanOther;
        //public uint ChaosHumanMale;
        //public uint ChaosHumanFemale;
        //public uint ElfOther;
        //public uint ElfMale;
        //public uint ElfFemale;
        //public uint DarkElfOther;
        //public uint DarkElfMale;
        //public uint DarkElfFemale;
        //public uint OrcOther;
        //public uint OrcMale;
        //public uint OrcFemale;
        //public uint GoblinOther;
        //public uint GoblinMale;
        //public uint GoblinFemale;
        //public uint BeastmanOther;
        //public uint BeastmanMale;
        //public uint BeastmanFemale;
        //public uint SkavenOther;
        //public uint SkavenMale;
        //public uint SkavenFemale;
        //public uint OgreOther;
        //public uint OgreMale;
        //public uint OgreFemale;
        //public uint ChaosWarriorOther;
        //public uint ChaosWarriorMale;
        //public uint ChaosWarriorFemale;


    }
}
