﻿#region Using directives



#endregion

namespace UnexpectedBytes.Math
{
    public static partial class Functions
    {
        #region Clamp
        /// <summary>
        /// Clamp a <paramref name="value"/> to <paramref name="calmpedValue"/> if it is withon the <paramref name="tolerance"/> range.
        /// </summary>
        /// <param name="value">The value to clamp.</param>
        /// <param name="calmpedValue">The clamped value.</param>
        /// <param name="tolerance">The tolerance value.</param>
        /// <returns>
        /// Returns the clamped value.
        /// result = (tolerance > Abs(value-calmpedValue)) ? calmpedValue : value;
        /// </returns>
        public static double Clamp(double value, double calmpedValue, double tolerance)
        {
            return (tolerance > Abs(value - calmpedValue)) ? calmpedValue : value;
        }
        /// <summary>
        /// Clamp a <paramref name="value"/> to <paramref name="calmpedValue"/> using the default tolerance value.
        /// </summary>
        /// <param name="value">The value to clamp.</param>
        /// <param name="calmpedValue">The clamped value.</param>
        /// <returns>
        /// Returns the clamped value.
        /// result = (EpsilonD > Abs(value-calmpedValue)) ? calmpedValue : value;
        /// </returns>
        /// <remarks><see cref="MathFunctions.EpsilonD"/> is used for tolerance.</remarks>
        public static double Clamp(double value, double calmpedValue)
        {
            return (Constants.EpsilonD > Abs(value - calmpedValue)) ? calmpedValue : value;
        }
        /// <summary>
        /// Clamp a <paramref name="value"/> to <paramref name="calmpedValue"/> if it is withon the <paramref name="tolerance"/> range.
        /// </summary>
        /// <param name="value">The value to clamp.</param>
        /// <param name="calmpedValue">The clamped value.</param>
        /// <param name="tolerance">The tolerance value.</param>
        /// <returns>
        /// Returns the clamped value.
        /// result = (tolerance > Abs(value-calmpedValue)) ? calmpedValue : value;
        /// </returns>
        public static float Clamp(float value, float calmpedValue, float tolerance)
        {
            return (tolerance > Abs(value - calmpedValue)) ? calmpedValue : value;
        }
        /// <summary>
        /// Clamp a <paramref name="value"/> to <paramref name="calmpedValue"/> using the default tolerance value.
        /// </summary>
        /// <param name="value">The value to clamp.</param>
        /// <param name="calmpedValue">The clamped value.</param>
        /// <returns>
        /// Returns the clamped value.
        /// result = (EpsilonF > Abs(value-calmpedValue)) ? calmpedValue : value;
        /// </returns>
        /// <remarks><see cref="MathFunctions.EpsilonF"/> is used for tolerance.</remarks>
        public static float Clamp(float value, float calmpedValue)
        {
            return (Constants.EpsilonF > Abs(value - calmpedValue)) ? calmpedValue : value;
        }
        #endregion
    }
}
