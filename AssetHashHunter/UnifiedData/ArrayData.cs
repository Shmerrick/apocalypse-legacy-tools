﻿using System;
using System.Threading;

namespace AssetHashHunter
{
    internal sealed class ArrayData : UnifiedDataBase
    {
        public override long Length { get => _data.Length; }

        private readonly byte[] _data;

        public ArrayData(byte[] data)
        {
            _data = data ?? throw new ArgumentNullException(nameof(data));
        }

        public override void ForEachRead(Action<byte[], int, int> action, CancellationToken cancellationToken)
        {
            if (action == null)
                throw new ArgumentNullException("action");

            cancellationToken.ThrowIfCancellationRequested();
            action(_data, 0, _data.Length);
        }

        public override void ForEachGroup(int groupSize, Action<byte[], int, int> action, Action<byte[], int, int> remainderAction, CancellationToken cancellationToken)
        {
            if (groupSize <= 0)
                throw new ArgumentOutOfRangeException(nameof(groupSize), $"{nameof(groupSize)} must be greater than 0.");

            if (action == null)
                throw new ArgumentNullException(nameof(action));

            cancellationToken.ThrowIfCancellationRequested();

            var remainderLength = _data.Length % groupSize;

            if (_data.Length - remainderLength > 0)
                action(_data, 0, _data.Length - remainderLength);

            if (remainderAction != null && remainderLength > 0)
            {
                cancellationToken.ThrowIfCancellationRequested();
                remainderAction(_data, _data.Length - remainderLength, remainderLength);
            }
        }

        public override byte[] ToArray(CancellationToken cancellationToken) => _data;
    }
}
