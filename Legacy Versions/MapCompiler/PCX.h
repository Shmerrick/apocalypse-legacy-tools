
#pragma once

using namespace System;
using namespace System::IO;

ref class PCX
{
private:

	Byte _mfr, _version, _encoding, _bpp, _nplanes;
	UInt16 _xmin, _xmax, _ymin, _ymax, _hres, _vres, _bytesPerLine;
	array<Byte> ^_data;

public:

	property int Width
	{
		int get() { return _xmax + 1 - _xmin; }
	}

	property int Height
	{
		int get() { return _ymax + 1 - _ymin; }
	}

	Byte GetValue(int x, int y)
	{
		return GetValue(x, y, 0);
	}

	Byte GetValue(int x, int y, int plane)
	{
		if (plane >= _nplanes)
			throw gcnew ArgumentOutOfRangeException();
		int rowOffset = y * _nplanes * Width;
		return _data[rowOffset + _nplanes * x + plane];
	}

	void Load(Stream^ stream)
	{
		BinaryReader ^reader = gcnew BinaryReader(stream);
		return LoadFromReader(reader);
	}

	void LoadFromReader(System::IO::BinaryReader ^reader)
	{
		_mfr = reader->ReadByte();
		_version = reader->ReadByte();
		_encoding = reader->ReadByte();
		_bpp = reader->ReadByte();

		_xmin = reader->ReadUInt16();
		_ymin = reader->ReadUInt16();
		_xmax = reader->ReadUInt16();
		_ymax = reader->ReadUInt16();
		_hres = reader->ReadUInt16();
		_vres = reader->ReadUInt16();

		array<Byte> ^colormap = reader->ReadBytes(48);

		Byte reserved = reader->ReadByte();
		_nplanes = reader->ReadByte();

		_bytesPerLine = reader->ReadUInt16();
		UInt16 paletteInfo = reader->ReadUInt16();

		array<Byte> ^filler = reader->ReadBytes(58);

		int rowSize = _nplanes * _bytesPerLine;
		array<Byte> ^row = gcnew array<Byte>(rowSize);

		_data = gcnew array<Byte>(Width * Height * _nplanes);

		for (int i = 0; i < Height; i++)
		{
			DecodeRow(row, reader);
			int rowOffset = i * Width * _nplanes;
			row->CopyTo(_data, rowOffset);
		}
	}

private:

	void DecodeRow(array<Byte> ^row, BinaryReader ^reader)
	{
		Byte value = 0;
		int pos = 0, runLength = 0;

		while (pos < row->Length)
		{
			if (runLength > 0)
			{
				row[pos++] = value;
				runLength--;
			}
			else
			{
				value = reader->ReadByte();

				if ((value & 0xC0) == 0xC0)
				{
					runLength = value & 0x3F;
					value = reader->ReadByte();
				}
				else
				{
					runLength = 0;
					row[pos++] = value;
				}
			}
		}
	}
};