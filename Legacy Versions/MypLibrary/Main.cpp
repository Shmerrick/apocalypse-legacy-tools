#include "platform.h"

#include "myp_manager.h"
#include "myp.h"

extern "C" { int _afxForceUSRDLL; }

extern "C"
{

#pragma pack(push, 1)
	struct AssetInfo {
		int64_t Hash;
		int32_t CompressedSize;
		int32_t UnCompressedSize;
	};
#pragma pack(pop, 1)

	__declspec(dllexport) void* LoadMyp(LogCallback log, const char* path)
	{
		Myp* myp = new Myp(log, path);

		return myp;
	}
   
    __declspec(dllexport) void* CreateMyp(LogCallback log, const char* path)
    {
        Myp* myp = new Myp(log, path, true);

        if (nullptr != myp)
            myp->Load(path);

        if (nullptr != myp || !myp->IsLoaded())
            return myp;

        return nullptr;
    }

	__declspec(dllexport) int32_t GetAssetSize(void* MypPtr, const char* assetPath)
	{
		Myp* myp = static_cast<Myp*>(MypPtr);
		FileEntry* const asset = myp->GetAsset(assetPath);
		if (nullptr != asset)
			return asset->UnCompressedSize;
		return 0;
	}

	__declspec(dllexport) int32_t GetAssetSizeByHash(void* MypPtr, int64_t hash)
	{
		Myp* myp = static_cast<Myp*>(MypPtr);
		FileEntry* asset = myp->GetAsset(hash);
		if (asset)
			return asset->UnCompressedSize;
		return 0;
	}

	__declspec(dllexport) int32_t GetAssetSizeCompressedByHash(void* MypPtr, int64_t hash)
	{
		Myp* myp = static_cast<Myp*>(MypPtr);
		FileEntry* asset = myp->GetAsset(hash);
		if (asset)
			return asset->CompressedSize;
		return 0;
	}

	__declspec(dllexport) bool GetAssetData(void* MypPtr, const char* assetPath, char* data)
	{
		Myp* myp = static_cast<Myp*>(MypPtr);
		FileEntry* asset = myp->GetAsset(assetPath);
		if (asset)
			return myp->GetAssetData(asset, (uint8_t*)data);
		return false;
	}

	__declspec(dllexport) bool GetAssetDataByHash(void* MypPtr, int64_t hash, char* data)
	{
		Myp* myp = static_cast<Myp*>(MypPtr);
		FileEntry* asset = myp->GetAsset(hash);
		if (asset)
		{
			memset(data, 0, asset->UnCompressedSize);
			return myp->GetAssetData(asset, (uint8_t*)data);
		}
		return false;
	}

	__declspec(dllexport) bool GetAssetDataByHashRaw(void* MypPtr, int64_t hash, char* data)
	{
		Myp* myp = static_cast<Myp*>(MypPtr);
		FileEntry* asset = myp->GetAsset(hash);
		if (asset)
		{
			memset(data, 0, asset->CompressedSize);
			return myp->GetAssetData(asset, (uint8_t*)data, 0, false);
		}
		return false;
	}

	__declspec(dllexport) void UpdateAssetByHash(void* MypPtr, int64_t hash, char* data, int32_t size, bool compress)
	{
		Myp* myp = static_cast<Myp*>(MypPtr);
		myp->UpdateAsset(hash, (uint8_t*)data, size, (uint8_t)(compress ? 1 : 0));
	}

	__declspec(dllexport) void UpdateAsset(void* MypPtr, const char* assetPath, char* data, int32_t size, bool compress)
	{
		Myp* myp = static_cast<Myp*>(MypPtr);
		myp->UpdateAsset(assetPath, (uint8_t*)data, size, (uint8_t)(compress ? 1 : 0));
	}

	__declspec(dllexport) bool DeleteAssetByHash(void* MypPtr, int64_t hash)
	{
		Myp* myp = static_cast<Myp*>(MypPtr);
		FileEntry* asset = myp->GetAsset(hash);
		if (asset)
		{
			myp->Delete(asset);
			return true;
		}
		return false;
	}

	__declspec(dllexport) bool DeleteAsset(void* MypPtr, const char* assetPath)
	{
		return DeleteAssetByHash(MypPtr, Myp::HashWARv1(assetPath));
	}

	__declspec(dllexport) bool Save(void* MypPtr)
	{
		Myp* myp = static_cast<Myp*>(MypPtr);
		return myp->Save();
	}

	__declspec(dllexport) int64_t HashWARv1(const char* s) {
		uint32_t edx = 0, eax, esi, ebx = 0, ph = 0, sh = 0;
		uint32_t edi, ecx;
		static uint32_t seed = 0xDEADBEEF;
		int len = (uint32_t)std::strlen(s);

		eax = ecx = edx = ebx = esi = edi = 0;
		ebx = edi = esi = (uint32_t)std::strlen(s) + seed;

		int i = 0;

		for (i = 0; i + 12 < len; i += 12) {
			edi = (uint32_t)((s[i + 7] << 24) | (s[i + 6] << 16) | (s[i + 5] << 8) | s[i + 4]) + edi;
			esi = (uint32_t)((s[i + 11] << 24) | (s[i + 10] << 16) | (s[i + 9] << 8) | s[i + 8]) + esi;
			edx = (uint32_t)((s[i + 3] << 24) | (s[i + 2] << 16) | (s[i + 1] << 8) | s[i]) - esi;

			edx = (edx + ebx) ^ (esi >> 28) ^ (esi << 4);
			esi += edi;
			edi = (edi - edx) ^ (edx >> 26) ^ (edx << 6);
			edx += esi;
			esi = (esi - edi) ^ (edi >> 24) ^ (edi << 8);
			edi += edx;
			ebx = (edx - esi) ^ (esi >> 16) ^ (esi << 16);
			esi += edi;
			edi = (edi - ebx) ^ (ebx >> 13) ^ (ebx << 19);
			ebx += esi;
			esi = (esi - edi) ^ (edi >> 28) ^ (edi << 4);
			edi += ebx;
		}

		if (len - i > 0) {
			switch (len - i) {
			    case 12: esi += (uint32_t)s[i + 11] << 24;
			    case 11: esi += (uint32_t)s[i + 10] << 16;
			    case 10: esi += (uint32_t)s[i + 9] << 8;
			    case 9: esi += (uint32_t)s[i + 8];
			    case 8: edi += (uint32_t)s[i + 7] << 24;
			    case 7: edi += (uint32_t)s[i + 6] << 16;
			    case 6: edi += (uint32_t)s[i + 5] << 8;
			    case 5: edi += (uint32_t)s[i + 4];
			    case 4: ebx += (uint32_t)s[i + 3] << 24;
			    case 3: ebx += (uint32_t)s[i + 2] << 16;
			    case 2: ebx += (uint32_t)s[i + 1] << 8;
                case 1: ebx += (uint32_t)s[i]; break;
			}

			esi = (esi ^ edi) - ((edi >> 18) ^ (edi << 14));
			ecx = (esi ^ ebx) - ((esi >> 21) ^ (esi << 11));
			edi = (edi ^ ecx) - ((ecx >> 7) ^ (ecx << 25));
			esi = (esi ^ edi) - ((edi >> 16) ^ (edi << 16));
			edx = (esi ^ ecx) - ((esi >> 28) ^ (esi << 4));
			edi = (edi ^ edx) - ((edx >> 18) ^ (edx << 14));
			eax = (esi ^ edi) - ((edi >> 8) ^ (edi << 24));

			ph = edi;
			sh = eax;
			return ((int64_t)ph << 32) + sh;
		}
		ph = esi;
		sh = eax;
		return ((int64_t)ph << 32) + sh;
	}

	__declspec(dllexport) int64_t HashWARv2(const void* key, uint32_t length) {
		uint32_t a;
		uint32_t b;
		uint32_t c = b = a = 0xdeadbeef + length;							/* Set up the internal state */

		uint32_t* k = (uint32_t*)key;										/* all but last block: aligned reads and affect 32 bits of (a,b,c) */
		while (length > 12) {
			a += k[0]; b += k[1]; c += k[2];
			mix(a, b, c);
			length -= 12;
			k += 3;
		}

		const uint8_t* k8 = reinterpret_cast<const uint8_t*>(k);
		switch (length) {													/* handle the last (probably partial) block */
			case 12: c += k[2]; b += k[1]; a += k[0]; break;
			case 11: c += ((uint32_t)k8[10]) << 16;							/* fall through */
			case 10: c += ((uint32_t)k8[9]) << 8;							/* fall through */
			case 9: c += k8[8];												/* fall through */
			case 8: b += k[1]; a += k[0]; break;
			case 7: b += ((uint32_t)k8[6]) << 16;							/* fall through */
			case 6: b += ((uint32_t)k8[5]) << 8;							/* fall through */
			case 5: b += k8[4];												/* fall through */
			case 4: a += k[0]; break;
			case 3: a += ((uint32_t)k8[2]) << 16;							/* fall through */
			case 2: a += ((uint32_t)k8[1]) << 8;							/* fall through */
			case 1: a += k8[0]; break;
			case 0: return (((int64_t)b) << 32) | c;								/* zero length strings require no mixing */
		};

		final(a, b, c);

        return (((int64_t)b) << 32) | c;
	};

    __declspec(dllexport) int64_t HashWARv3(const void* key, size_t length) {
        uint32_t a;
        uint32_t b;
        uint32_t c = b = a = 0xdeadbeef + ((uint32_t)length);				/* Set up the internal state */

        uint32_t* k = (uint32_t*)key;										/* all but last block: aligned reads and affect 32 bits of (a,b,c) */
        while (length > 12) {
            a += k[0]; b += k[1]; c += k[2];
            mix(a, b, c);
            length -= 12;
            k += 3;
        }

        switch (length) {													/* handle the last (probably partial) block */
        case 12: { c += k[2]; b += k[1]; a += k[0]; break; }
        case 11: { c += k[2] & 0xffffff; b += k[1]; a += k[0]; break; }
        case 10: { c += k[2] & 0xffff; b += k[1]; a += k[0]; break; }
        case 9: { c += k[2] & 0xff; b += k[1]; a += k[0]; break; }
        case 8: { b += k[1]; a += k[0]; break; }
        case 7: { b += k[1] & 0xffffff; a += k[0]; break; }
        case 6: { b += k[1] & 0xffff; a += k[0]; break; }
        case 5: { b += k[1] & 0xff; a += k[0]; break; }
        case 4: { a += k[0]; break; }
        case 3: { a += k[0] & 0xffffff; break; }
        case 2: { a += k[0] & 0xffff; break; }
        case 1: { a += k[0] & 0xff; break; }
        case 0: { return (((int64_t)b) << 32) | c; }						/* zero length strings require no mixing */
        }

        final(a, b, c);
        return (((int64_t)b) << 32) | c;
    }

	__declspec(dllexport) size_t GetAssetCount(void* MypPtr)
	{
		Myp* myp = static_cast<Myp*>(MypPtr);
		return myp->GetAssets().size();
	}

	__declspec(dllexport) void GetAssetsInfo(void* MypPtr, int64_t * hashes, int32_t * compressedSizes, int32_t * uncompressedSizes, int32_t * compressed, int32_t * crc)
	{
		Myp* myp = static_cast<Myp*>(MypPtr);
		int i = 0;
		for (auto& v : myp->GetAssets())
		{
			auto entry = v.second;
			int64_t hash = (int64_t)entry->Hash2 << 32 | entry->Hash1;

			hashes[i] = hash;
			compressedSizes[i] = entry->CompressedSize;
			uncompressedSizes[i] = entry->UnCompressedSize;
			compressed[i] = entry->Compressed;
			crc[i] = entry->CRC32;
			i++;
		}
	}

    //__declspec(dllexport) std::map<std::string, std::string> GetAlternateStreamProperties(std::string filename, std::string streamname)
    //{
    //    HANDLE FindFirstStreamW(
    //        LPCWSTR            lpFileName,
    //        STREAM_INFO_LEVELS InfoLevel,
    //        LPVOID             lpFindStreamData,
    //        DWORD              dwFlags
    //    );
    //}
}

void Log(int, const char* msg)
{
	printf(msg);
	printf("\r\n");
}

BOOL WINAPI DllMain(HANDLE, ULONG ul_reason, LPVOID)
{
	switch (ul_reason) {
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}




int main()
{

	//Myp m(0, "C:\\WarPatcher\\Files\\Development\\c11f3050576943cbb7fb31f022efa459", false, Archive::ARCHIVE_DEV);

	//uint8_t* data = new uint8_t[12345456];
	//FileEntry* entry = m.GetAsset(396182630760429074);
	//m.Delete(entry);
	//m.Save();
	//m.GetAssetData(entry,data, 0);
	//m.UpdateAsset(-6418477353067224073, data, entry->UnCompressedSize, entry->Compressed);
	//m.Save();
	return 0;
}
