#pragma once

#include "platform.h"

#pragma pack(push, 1)

struct MypHeader
{
    uint8_t FileTableHeader[4] = {0};
    uint32_t Unk1 = 0;
    uint32_t Unk2 = 0;
    uint32_t FileOffset1 = 0;
    uint32_t FileOffset2 = 0;
    uint32_t EntriesPerFile = 0;
    uint32_t FileCount = 0;
    uint32_t Unk5 = 0;
    uint32_t Unk6 = 0;
    uint8_t Padding[0x1DC] = {0};
};

struct FileTableHeader
{
    uint32_t FileCount = 0;
    uint32_t NextFileOffset1 = 0;
    uint32_t NextFileOffset2 = 0;
};

struct FileEntry
{
    uint32_t Offset1 = 0;
    uint32_t Offset2 = 0;
    uint32_t HeaderSize = 0;
    uint32_t CompressedSize = 0;
    uint32_t UnCompressedSize = 0;
    uint32_t Hash1 = 0;
    uint32_t Hash2 = 0;
    uint32_t CRC32 = 0;
    uint8_t Compressed = 0;
    uint8_t Unk1 = 0;
};

#pragma pack(pop, 1)

struct FileEntryChange
{
	int64_t _tempFilePos = 0;
	FileEntry* Entry = nullptr;
	const uint8_t* HeaderData = nullptr;
	size_t DataLength = 0;
	size_t HeaderDataLength = 0;
    uint32_t Hash1 = 0;
    uint32_t Hash2 = 0;
    uint32_t crc32 = 0;
	uint8_t Compressed = 0;
	uint8_t Unk1 = 0;
	uint16_t Padding = 0;
};

struct UsedBlock
{
	uint32_t Type = 0;
    void* RecordPtr = nullptr;
    int64_t Offset = 0;
    int64_t Length = 0;
};

class Myp
{
public:
    Myp(LogCallback logger);
    Myp(LogCallback logger, const char* filename);
    Myp(LogCallback logger, const char* filename, bool create);
    ~Myp();

    bool LoadMyp();
    uint32_t EntryCount() const noexcept { return _header.FileCount; }
    std::string GetFileName() const { return _filename; }
    bool IsLoaded() const noexcept { return _loaded; }
    std::map<int64_t, FileEntry*>& GetHashes() noexcept { return _fileEntriesHash; };
    void Delete(FileEntry* entry);
    bool Save();
    bool GetAssetMetaData(FileEntry* entry, uint8_t* dstBuffer, int destOffset = 0);
    bool GetAssetData(FileEntry* entry, uint8_t* dstBuffer, int destOffset = 0, bool decompress = true);
    std::string GetAssetDataString(const char* file);
    std::string GetAssetDataString(int64_t hash);
    const std::map<int64_t, FileEntry*>& GetAssets();
    FileEntry* GetAsset(const char* name);
    FileEntry* GetAsset(int64_t hash);
    bool IsClosed() const noexcept { return !_loaded; }
    bool Load();
    void Close();
    void CloseFile();
    bool UpdateAsset(const char* name, const uint8_t* data, uint32_t dataLength, bool compressed, uint8_t* headerData = nullptr, uint32_t headerDataLength = 0, uint32_t overrideCrc32 = 0);
    bool UpdateAsset(const int64_t hash, const uint8_t* data, uint32_t dataLength, bool compressed, uint8_t* headerData = nullptr, uint32_t headerDataLength = 0, uint32_t overrideCrc32 = 0);
    static int64_t HashWARv1(const char* s);
    static int64_t HashWARv3b(const void* key, size_t length);
    static int64_t HashWARv4(std::string s);
    Archive getArchiveId() const noexcept { return _archive; }
    uint32_t getArchiveHash() const noexcept { return _archiveHash; }
    static std::map<int64_t, std::string> GetDeHash(const char* dehash);
    uint32_t getHashHash();
    bool Create(std::string filename);
    bool Load(std::string filename);
    static void NullLog(int, int, const char*) noexcept { }
    bool HasAsset(int64_t hash);
    bool HasAsset(const char* name);
    int64_t GetArchiveSize() const noexcept { return _filesize; }
    void Open();

private:

    void WriteNewMypHeader();
    bool MarkUsedBlock(int64_t pos, int64_t length, int type, void* recordPtr);
    void CalculateFreeBlocks();
    void LoadFrom(Myp* Myp);

    FileEntry* FindInsertFreeFileEntry();
    bool FreeBlock(int64_t offset, int64_t size);
    UsedBlock GetFreeBlock(int64_t size);

private:

	LogCallback _logger;

	FILE* _file;
	FILE* _tempFile;

    const int BLOCK_MYP_HEADER = 1;
    const int BLOCK_MFT_ENTRY = 2;
    const int BLOCK_ASSET_ENTRY = 3;
    const int BLOCK_FREE = 4;

    int64_t _totalFileSize;
    int64_t _filesize;
    bool _loaded;
	bool _changed;
    uint32_t _entryCount;
    std::vector<FileTableHeader*> _fileTables;
    std::vector<FileEntry*> _insertedFileEntries;
    std::vector<void*> _fileBlockMemory;
    std::map<int64_t, FileEntry*> _fileEntriesHash;
    std::vector<FileEntry*> _fileEntries;
    std::map<int64_t, FileEntryChange> _updated;
    std::map<int64_t, FileEntryChange> _inserted;
    std::map<FileTableHeader*, int64_t> _fileHeaderToc;
    std::map<FileEntry*, int64_t> _fileEntryToc;
    std::vector<UsedBlock> _usedBlocks;
    std::vector<UsedBlock> _freeBlocks;
	Archive _archive;
	uint32_t _archiveHash;
	MypHeader _header;
    std::string _filename;
};
