﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace UnexpectedBytes
{
    public class Parser
    {
        private XAC mXACData;
        private BinaryReader mStream;

        public Parser(BinaryReader iStream, XAC xXACData)
        {
            mXACData = xXACData;
            mStream = iStream;
        }

        public bool Parse()
        {
            bool error = false;
            long stream_position = 0;
            uint magic = mStream.ReadUInt32();
            mStream.BaseStream.Seek(0, SeekOrigin.Begin);

            if (0x58414320 == magic || 0x20434158 == magic)
            {
                XACFileHeader header = new XACFileHeader();
                header.ReadIn(mStream);

                mXACData.mXACFileHeader = header;
                stream_position += header.GetSize();

                Console.WriteLine($"Magic '{System.Text.Encoding.Default.GetString(BitConverter.GetBytes(header.mMagic))}'");
                Console.WriteLine($"Version {header.mMajorVersion}.{header.mMinorVersion}");
                Console.WriteLine($"Big Endian {header.mBigEndianFlag}");
                Console.WriteLine($"Multiply Order {header.mMultiplyOrder}");
                Console.WriteLine("");

                while (mStream.BaseStream.Position < mStream.BaseStream.Length && !error)
                {
                    error = ParseChunk(ref stream_position);
                }

                if (error)
                {
                    mXACData.clear();
                    error = true;
                }

                error = false;
            }

            return error;
        }

        private bool ParseChunk(ref long stream_position)
        {
            bool error = false;

            Chunk actor_chunk = new Chunk();
            actor_chunk.ReadIn(mStream);

            switch (actor_chunk.mType)
            {
                case ChunkType.Node:
                {
                    Node chunk = new Node();
                    chunk.ReadIn(mStream, actor_chunk);

                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");

                    Console.WriteLine($"Rotation           {chunk.mRotation}");
                    Console.WriteLine($"Bind Pose Rotation {chunk.mBindPoseRotation}");
                    Console.WriteLine($"Position           {chunk.mPosition}");
                    Console.WriteLine($"Scale              {chunk.mScale}");
                    Console.WriteLine($"Bind Pose Position {chunk.mBindPosePosition}");
                    Console.WriteLine($"LOD Mask           {Convert.ToString(chunk.mLODMask, 2)}");
                    Console.WriteLine($"Parent             {chunk.mParentID}");
                    Console.WriteLine($"Name               {chunk.mName}");
                    Console.WriteLine($"");

                    mXACData.mXACData.mBonesList.Add(chunk);
                    break;
                }
                case ChunkType.Mesh:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");

                    Mesh vMesh = ParseMesh();

                    Console.WriteLine($"");

                    mXACData.mXACData.mMeshesList.Add(vMesh);

                    break;
                }
                case ChunkType.SkinningInfo:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");
                    SkipBytesInFile(actor_chunk.mSize);

                    //XACUnknownOfChunkID2 vXACUnknownOfChunkID2 = ParseUnknownChunkID2();
                    //mXACData.mXACModelData.mUnknownOfChunkID2List.Add(vXACUnknownOfChunkID2);

                    Console.WriteLine($"");

                    break;
                }
                case ChunkType.StandardMaterial:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");

                    StandardMaterial chunk = new StandardMaterial();
                    chunk.ReadIn(mStream, actor_chunk);

                    Console.WriteLine($"Ambient color       {chunk.mAmbient}");
                    Console.WriteLine($"Diffuse color       {chunk.mDiffuse}");
                    Console.WriteLine($"Specular color      {chunk.mSpecular}");
                    Console.WriteLine($"Emissive color      {chunk.mEmissive}");
                    Console.WriteLine($"Shine               {chunk.mShine}");
                    Console.WriteLine($"Shine strength      {chunk.mShineStrength}");
                    Console.WriteLine($"Opacity             {chunk.mOpacity}");
                    Console.WriteLine($"Index of Refraction {chunk.mIndexOfRefraction}");
                    Console.WriteLine($"Double sided        {chunk.mDoubleSided}");
                    Console.WriteLine($"Wireframe           {chunk.mWireFrame}");
                    Console.WriteLine($"Transparency Type   {(char)chunk.mTransparencyType} (F=Filter, S=Substractive, A=Additive, U=Unknown)");
                    Console.WriteLine($"Index of Refraction {chunk.mLayerCount}");
                    Console.WriteLine($"Name                '{chunk.mName}'");
                    Console.WriteLine($"");

                    mXACData.mXACData.mMaterialListChunkID3.Add(chunk);
                    break;
                }
                case ChunkType.StandardMaterialLayer:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");
                    SkipBytesInFile(actor_chunk.mSize);

                    //XACTexture vTexture = ParseTexture();
                    //mXACData.mXACModelData.mTextureList.Add(vTexture);

                    Console.WriteLine($"");

                    break;
                }
                case ChunkType.FXMaterial:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");
                    SkipBytesInFile(actor_chunk.mSize);

                    //XACTexture vTexture = ParseTexture();
                    //mXACData.mXACModelData.mTextureList.Add(vTexture);

                    Console.WriteLine($"");

                    break;
                }
                case ChunkType.Limit:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");
                    SkipBytesInFile(actor_chunk.mSize);

                    Console.WriteLine($"");

                    break;
                }
                case ChunkType.Info:
                {
                    Console.WriteLine($"'Info' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");

                    Info chunk = new Info();
                    chunk.ReadIn(mStream, actor_chunk);

                    Console.WriteLine($"Motion Extraction Node Index {chunk.mMotionExtractionNodeIndex}");
                    Console.WriteLine($"Trajectory Node Index {chunk.mTrajectoryNodeIndex}");
                    if(actor_chunk.mVersion == 2)
                        Console.WriteLine($"Retarget Root Offset {chunk.mRetargetRootOffset}");
                    Console.WriteLine($"Source application {chunk.mExporterProgramName}");
                    Console.WriteLine($"Source filename {chunk.mSourceFilename}");
                    Console.WriteLine($"Exporter version {chunk.mExporterPluginMajorVersion}.{chunk.mExporterPluginMinorVersion}");
                    Console.WriteLine($"Exporter compilation date {chunk.mExporterCompileDate}");
                    Console.WriteLine($"Actor name {chunk.mActorName}");
                    Console.WriteLine($"Unit type {chunk.mUnitType}");
                    Console.WriteLine($"Unknown {chunk.mUnknown1}");
                    Console.WriteLine("");

                    mXACData.mXACData.mMetaData = chunk;
                    break;
                }
                case ChunkType.StandardProgramMorphTarget:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");
                    SkipBytesInFile(actor_chunk.mSize);

                    Console.WriteLine($"");

                    break;
                }
                case ChunkType.NodeGroups:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");
                    SkipBytesInFile(actor_chunk.mSize);

                    Console.WriteLine($"");

                    break;
                }
                case ChunkType.Nodes:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");

                    Nodes chunk = new Nodes();
                    chunk.ReadIn(mStream, actor_chunk);

                    mXACData.mXACData.mNodeTree = chunk;

                    Console.WriteLine($"");

                    break;
                }
                case ChunkType.StandardMorphTargets:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");
                    SkipBytesInFile(actor_chunk.mSize);

                    Console.WriteLine($"");

                    break;
                }
                case ChunkType.MaterialInfo:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");

                    MaterialInfo chunk = new MaterialInfo();
                    chunk.ReadIn(mStream, actor_chunk);

                    mXACData.mXACData.mMaterialInfo = chunk;

                    Console.WriteLine($"");

                    break;
                }
                case ChunkType.NodeMotionSources:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");
                    SkipBytesInFile(actor_chunk.mSize);

                    Console.WriteLine($"");

                    break;
                }
                case ChunkType.AttachmentNodes:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");
                    SkipBytesInFile(actor_chunk.mSize);

                    Console.WriteLine($"");

                    break;
                }
                case ChunkType.MaterialAttributeSet:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");
                    SkipBytesInFile(actor_chunk.mSize);

                    Console.WriteLine($"");

                    break;
                }
                case ChunkType.GenericMaterial:
                {
                    Console.WriteLine($"'{actor_chunk.mType}' chunk (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");
                    SkipBytesInFile(actor_chunk.mSize);

                    Console.WriteLine($"");

                    break;
                }



                //case 11:
                //{
                //    //NodeTree ID11 (B)
                //    string vChunkInfo = "Chunk Id:" + vChunkId.ToString() + " NodeTree " + " Size:" + vChunkSize.ToString() + " Version:" + vChunkVersion.ToString();
                //    mXACData.mXACModelData.mNodeTree = new XACNodeTree();
                //    mXACData.mXACModelData.mNodeTree.ReadIn(mStream);
                //    break;
                //}
                //case 12:
                //{
                //    //Unknown ID12 (C)
                //    string vChunkInfo = "Chunk Id:" + vChunkId.ToString() + " Unknown " + " Size:" + vChunkSize.ToString() + " Version:" + vChunkVersion.ToString();
                //    if (vChunkSize < 10000000)
                //    {
                //        byte[] vBuffer = new byte[vChunkSize];
                //        mStream.Read(vBuffer, 0, (int)vChunkSize);
                //    }
                //    else
                //    {
                //        SkipBytesInFile(vChunkSize);
                //    }
                //    break;
                //}
                //case 13:
                //{
                //    //MaterialTotals (D)
                //    string vChunkInfo = "Chunk Id:" + vChunkId.ToString() + " MaterialTotals " + " Size:" + vChunkSize.ToString() + " Version:" + vChunkVersion.ToString();
                //    Trace(vChunkInfo);

                //    break;
                //}
                default:
                {
                    Console.WriteLine($"'Unknown' chunk (type {actor_chunk.mType}) (version {actor_chunk.mVersion}) ({actor_chunk.mSize} bytes).");
                    if (actor_chunk.mType > ChunkType.Count)
                    {
                        Console.WriteLine($"ChunkType > ChunkType.Count({ChunkType.Count}).");
                        Console.WriteLine($"Unrecoverable Error.");
                        Console.WriteLine($"Process stopped.");
                        error = true;
                    }
                    SkipBytesInFile(actor_chunk.mSize);
                    break;
                }
            }

            return error;
        }

        private void SkipBytesInFile(uint iNrOfBytes)
        {
            long vBytesToRead = iNrOfBytes;

            int vBufferSize = 256 * 256;
            byte[] vBuffer = new byte[vBufferSize];

            while (0 < vBytesToRead)
            {
                long vLeftBytesToReadAfterABlockRead = vBytesToRead - vBufferSize;
                if (0 > vLeftBytesToReadAfterABlockRead)
                {
                    mStream.Read(vBuffer, 0, (int)vBytesToRead);
                }
                else
                {
                    mStream.Read(vBuffer, 0, vBufferSize);
                }
                vBytesToRead = vLeftBytesToReadAfterABlockRead;
            }
        }


        private XACUnknownOfChunkID2 ParseUnknownChunkID2()
        {
            uint vValue1 = mStream.ReadUInt32();
            uint vValue2 = mStream.ReadUInt32();
            uint vCount = mStream.ReadUInt32();
            mStream.ReadUInt32(); //00 47 00 00

            List<XACUnknownOfChunkID2Entry> vUnknownOfChunkID2List = new List<XACUnknownOfChunkID2Entry>();
            for (int vEntryNr = 1; vEntryNr <= vCount; vEntryNr++)
            {
                float vValue1Entry = mStream.ReadSingle();
                uint vValue2Entry = mStream.ReadUInt32();
                float vValue3Entry = mStream.ReadSingle();
                uint vValue4Entry = mStream.ReadUInt32();

                XACUnknownOfChunkID2Entry vXACUnknownOfChunkID2Entry = new XACUnknownOfChunkID2Entry(vValue1Entry, vValue2Entry, vValue3Entry, vValue4Entry);
                vUnknownOfChunkID2List.Add(vXACUnknownOfChunkID2Entry);
            }

            XACUnknownOfChunkID2 vXACUnknownOfChunkID2 = new XACUnknownOfChunkID2(vValue1, vValue2, vUnknownOfChunkID2List);
            return vXACUnknownOfChunkID2;
        }

        //private StandardMaterial ParseMaterialChunkID3(out long oAdditionalBytesRead)
        //{
        //    oAdditionalBytesRead = 0;

        //    for (int vi = 1; vi <= 16; vi++)
        //    {
        //        mStream.ReadSingle();
        //    }
        //    float vPower = mStream.ReadSingle();
        //    mStream.ReadSingle();
        //    mStream.ReadSingle();
        //    mStream.ReadSingle();
        //    mStream.ReadByte();
        //    mStream.ReadByte();
        //    mStream.ReadByte();
        //    int vTextureCount = mStream.ReadByte();
        //    string vMatName = Common.ReadString(mStream);

        //    List<TextureLinkage> vMatTextureEntryList = new List<TextureLinkage>();
        //    for (int vTextureNr = 1; vTextureNr <= vTextureCount; vTextureNr++)
        //    {
        //        mStream.ReadSingle();
        //        mStream.ReadSingle();
        //        mStream.ReadSingle();
        //        mStream.ReadSingle();
        //        mStream.ReadSingle();
        //        mStream.ReadSingle();
        //        mStream.ReadUInt16();
        //        mStream.ReadUInt16();
        //        string vTextureName = Common.ReadString(mStream);

        //        oAdditionalBytesRead += 6 * 4 + 2 * 2 + 4 + vTextureName.Count(); //6floats + 2*int16 + 4 byte string header + string size

        //        TextureLinkage vMatTextureEntry = new TextureLinkage(vTextureName, null);
        //        vMatTextureEntryList.Add(vMatTextureEntry);
        //    }
        //    StandardMaterial vMaterial = new StandardMaterial(vMatName, vMatTextureEntryList);
        //    return vMaterial;
        //}

        private XACTexture ParseTexture()
        {
            mStream.ReadSingle();
            mStream.ReadSingle();
            mStream.ReadSingle();
            mStream.ReadSingle();
            mStream.ReadSingle();
            mStream.ReadSingle();

            ushort vMatNumber = mStream.ReadUInt16();
            mStream.ReadUInt16();
            string vTextureName = Common.ReadString(mStream);

            //"_d" or "_D" in vTextureName -> diffuse texture
            //"_n" or "_N" in vTextureName -> normal texture
            //none of above -> diffuse texture

            XACTexture vTexture = new XACTexture(vTextureName);
            return vTexture;
        }

        private StandardMaterial ParseMaterialChunkID5()
        {
            //yulgang 2 mat chunk (untested)
            uint vFxCount = mStream.ReadUInt32();
            uint vCount1 = mStream.ReadUInt32();
            uint vCount2 = mStream.ReadUInt32();
            uint vCount3 = mStream.ReadUInt32();
            mStream.ReadUInt32();
            uint vTexMapsCount = mStream.ReadUInt32();
            string vMatName = Common.ReadString(mStream);

            List<StandardMaterialLayer> vMatTextureEntryList = new List<StandardMaterialLayer>();
            for (uint vi = 1; vi <= vFxCount; vi++)
            {
                Common.ReadString(mStream);
                Common.ReadString(mStream);
                mStream.ReadUInt32();
            }
            for (uint vi = 1; vi <= vCount1; vi++)
            {
                Common.ReadString(mStream);
                mStream.ReadSingle();
            }
            for (uint vi = 1; vi <= vCount2; vi++)
            {
                Common.ReadString(mStream);
                mStream.ReadSingle();
                mStream.ReadSingle();
                mStream.ReadSingle();
                mStream.ReadSingle();
            }
            for (uint vi = 1; vi <= vCount3; vi++)
            {
                Common.ReadString(mStream);
                mStream.ReadByte();
            }

            mStream.ReadSingle();

            for (uint vi = 1; vi <= vTexMapsCount; vi++)
            {
                string vTextureType = Common.ReadString(mStream);
                string vTextureName = Common.ReadString(mStream);

                //vTextureType can be:
                // "DIFFUSECOLOR" or  "diffuseTexture"
                // "BUMP" or  "normalTexture"
                // "specTexture"
                // "MASK" or  "maskTexture"
                // "glowTexture"
                StandardMaterialLayer vMatTextureEntry = new StandardMaterialLayer(vTextureName, vTextureType);
                vMatTextureEntryList.Add(vMatTextureEntry);
            }

            //StandardMaterial vMaterial = new StandardMaterial(vMatName, vMatTextureEntryList);
            return null;
        }



        private Mesh ParseMesh()
        {

            Mesh vMesh = new Mesh();

            mStream.ReadUInt32();
            mStream.ReadUInt32();

            uint vVerticesCount = mStream.ReadUInt32();

            mStream.ReadUInt32();

            uint vFaceGroupsCount = mStream.ReadUInt32();
            uint vStructsCount = mStream.ReadUInt32();

            mStream.ReadUInt32();
            mStream.ReadUInt32();
            mStream.ReadUInt32();
            mStream.ReadUInt32();

            //Vertices id's?
            for (int vCount = 1; vCount <= vVerticesCount; vCount++)
            {
                mStream.ReadUInt32(); //read UInt32s
            }

            for (int vStructNr = 1; vStructNr <= (vStructsCount - 1); vStructNr++)
            {
                uint vStructId = mStream.ReadUInt32();
                uint vStructSize = mStream.ReadUInt32(); //Bytes per entry
                uint vUnknown = mStream.ReadUInt32(); //Unknown

                switch (vStructId)
                {
                    case 0:
                    {
                        List<XACVertice> vVerticesList = ParseVertices(vVerticesCount);
                        vMesh.mVerticesList = new List<XACVertice>(vVerticesList);
                        break;
                    }
                    case 1:
                    {
                        List<XACNormal> vNormalsList = ParseNormals(vVerticesCount);
                        vMesh.mNormalsList = new List<XACNormal>(vNormalsList);
                        break;
                    }
                    case 2:
                    {
                        List<XACWeight> vWeightsList = ParseWeights(vVerticesCount);
                        vMesh.mWeightsList = new List<XACWeight>(vWeightsList);
                        break;
                    }
                    case 3:
                    {
                        List<XACUVCoord> vUVCoordList = ParseUVCoords(vVerticesCount);
                        vMesh.mUVCoordsList = new List<XACUVCoord>(vUVCoordList);
                        break;
                    }
                    case 4:
                    {
                        List<XACVerticeColor> vVerticeColorList = ParseVerticeColors(vVerticesCount);
                        vMesh.mVerticeColorList = new List<XACVerticeColor>(vVerticeColorList);
                        break;
                    }
                    default:
                    {
                        Console.WriteLine("Unknown Mesh Struct ID: " + vStructId.ToString());
                        SkipBytesInFile(vStructSize * vVerticesCount);
                        break;
                    }
                }
            }

            //Hm the Last Group is always that FaceGroupList???
            List<XACFaceGroup> vFaceGroupList = ParseFaceGroups(vFaceGroupsCount);
            vMesh.mFaceGroupList = new List<XACFaceGroup>(vFaceGroupList);

            return vMesh;
        }


        private List<XACUVCoord> ParseUVCoords(uint iVerticesCount)
        {
            List<XACUVCoord> vUVCoordsList = new List<XACUVCoord>();

            for (uint vVerticesNr = 1; vVerticesNr <= iVerticesCount; vVerticesNr++)
            {
                float vU = mStream.ReadSingle();
                float vV = mStream.ReadSingle();
                XACUVCoord vUVCoord = new XACUVCoord(vU, vV);
                vUVCoordsList.Add(vUVCoord);
            }
            return vUVCoordsList;
        }

        private List<XACWeight> ParseWeights(uint iVerticesCount)
        {
            List<XACWeight> vWeightList = new List<XACWeight>();

            for (uint vVerticesNr = 1; vVerticesNr <= iVerticesCount; vVerticesNr++)
            {
                float vWeight1 = mStream.ReadSingle();
                float vWeight2 = mStream.ReadSingle();
                float vWeight3 = mStream.ReadSingle();
                float vWeight4 = mStream.ReadSingle();
                XACWeight vWeight = new XACWeight(vWeight1, vWeight2, vWeight3, vWeight4);
                vWeightList.Add(vWeight);
            }
            return vWeightList;
        }

        private List<XACVerticeColor> ParseVerticeColors(uint iVerticesCount)
        {
            List<XACVerticeColor> vVerticeColorsList = new List<XACVerticeColor>();

            for (uint vVerticesNr = 1; vVerticesNr <= iVerticesCount; vVerticesNr++)
            {
                byte vA = mStream.ReadByte();
                byte vB = mStream.ReadByte();
                byte vC = mStream.ReadByte();
                byte vD = mStream.ReadByte();
                XACVerticeColor vVerticeColor = new XACVerticeColor(vA, vB, vC, vD);
                vVerticeColorsList.Add(vVerticeColor);
            }
            return vVerticeColorsList;
        }

        private List<XACNormal> ParseNormals(uint iVerticesCount)
        {
            List<XACNormal> vNormalsList = new List<XACNormal>();

            for (uint vVerticesNr = 1; vVerticesNr <= iVerticesCount; vVerticesNr++)
            {
                float vX = mStream.ReadSingle();
                float vY = mStream.ReadSingle();
                float vZ = mStream.ReadSingle();
                XACNormal vNormal = new XACNormal(vX, vY, vZ);
                vNormalsList.Add(vNormal);
            }
            return vNormalsList;
        }

        private List<XACVertice> ParseVertices(uint iVerticesCount)
        {
            List<XACVertice> vVerticesList = new List<XACVertice>();

            for (uint vVerticesNr = 1; vVerticesNr <= iVerticesCount; vVerticesNr++)
            {
                float vX = mStream.ReadSingle();
                float vY = mStream.ReadSingle();
                float vZ = mStream.ReadSingle();
                XACVertice vVertices = new XACVertice(vX, vY, vZ);
                vVerticesList.Add(vVertices);
            }
            return vVerticesList;
        }


        private List<XACFaceGroup> ParseFaceGroups(uint iFaceGroupsCount)
        {
            List<XACFaceGroup> vFaceGroupList = new List<XACFaceGroup>();
            for (uint vFaceGroupNr = 1; vFaceGroupNr <= iFaceGroupsCount; vFaceGroupNr++)
            {
                uint vVerticesIndexesCount = mStream.ReadUInt32();
                uint vFaceGroupVerticesCount = mStream.ReadUInt32();
                uint vMaterialCount = mStream.ReadUInt32();
                uint vBonesCount = mStream.ReadUInt32();

                if ((vVerticesIndexesCount % 3) != 0)
                {
                    //Error it should be modulo 3 because triangles always have 3 points
                    Console.WriteLine("XAC ERROR: The read VerticesIndexesCount " + vVerticesIndexesCount.ToString() + " is not modulo 3!");
                }

                uint vFacesCount = vVerticesIndexesCount / 3;

                List<XACFace> vFacesList = new List<XACFace>();
                for (uint vFaceNr = 1; vFaceNr <= vFacesCount; vFaceNr++)
                {
                    XACFace vFace = ParseTriangleFace();
                    vFacesList.Add(vFace);
                }

                for (uint i = 1; i <= vBonesCount; i++)
                {
                    mStream.ReadUInt32();
                }

                XACFaceGroup vFaceGroup = new XACFaceGroup(vFacesList, vFaceGroupVerticesCount, vBonesCount);
                vFaceGroupList.Add(vFaceGroup);

            }
            return vFaceGroupList;
        }

        private XACFace ParseTriangleFace()
        {
            List<uint> vVerticesIndexsList = new List<uint>
            {
                mStream.ReadUInt32(),
                mStream.ReadUInt32(),
                mStream.ReadUInt32()
            };
            XACFace vFace = new XACFace(vVerticesIndexsList);
            return vFace;
        }

    }
}
