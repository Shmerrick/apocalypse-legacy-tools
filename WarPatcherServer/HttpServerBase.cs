﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using WarShared.Data;

namespace WarShared
{
    public abstract class HttpServerBase
    {
        #region classes
        public class MethodData
        {
            public bool HasContext;
            public Delegate Method;
            public HttpRoute Route;
            public Dictionary<string, ParamData> Params = new Dictionary<string, ParamData>();
        }

        public class ParamData
        {
            public string Name;
            public bool Required;
            public bool FromBody;
            public Type Type;
            public bool FromJson;
        }

        [AttributeUsage(AttributeTargets.Parameter)]
        public class FromBody : Attribute
        {
        }

        [AttributeUsage(AttributeTargets.Parameter)]
        public class FromJson : Attribute
        {
        }
        [AttributeUsage(AttributeTargets.Parameter)]
        public class FromJsonBody : Attribute
        {
        }
        #endregion

        #region members
        private Dictionary<string, MethodData> _methods = new Dictionary<string, MethodData>();
        private string _address;
        private int _port;
        protected ILog Log;
        private string _name;
        private IWebHost _httpHost;
        private ServerState _state;
        private long _ID;
        public CancellationTokenSource CancellationTokenSource { get; private set; }
        #endregion

        public HttpServerBase(CancellationTokenSource tokenSource, string name, long ID)
        {
            Log = new Logger(name);
            _name = name;
            _ID = ID;
            CancellationTokenSource = tokenSource;
            AddHandlers(GetType(), this); //map nedpoints
        }

        #region methods
        public void SetState(ServerState state)
        {
            _state = state;
        }

        public virtual void AddHandlers(Type type, object obj)
        {
            foreach (var method in type.GetMethods())
            {
                var attrib = method.GetCustomAttribute<HttpRoute>();
                if (attrib != null)
                {
                    var route = new MethodData()
                    {
                        Route = attrib,
                        Method = Util.CreateDelegate(method, obj),
                        Params = new Dictionary<string, ParamData>()
                    };
                    
                    _methods[attrib.Route] = route;

                    for (int i = 0; i < method.GetParameters().Length; i++)
                    {
                        if (method.GetParameters()[i].ParameterType == typeof(HttpContext))
                        {
                            route.Params["context"] = new ParamData()
                            {
                                Name = "context",
                                Required = true,
                                Type = typeof(HttpContext)
                            };
                            route.HasContext = true;
                            continue;
                        }

                        route.Params[method.GetParameters()[i].Name] = new ParamData()
                        {
                            Name = method.GetParameters()[i].Name,
                            Required = true,
                            FromBody = method.GetParameters()[i].GetCustomAttribute<FromBody>() != null,
                            Type = method.GetParameters()[i].ParameterType,
                            FromJson = method.GetParameters()[i].GetCustomAttribute<FromJson>() != null,
                        };

                        if (method.GetParameters()[i].GetCustomAttribute<FromJsonBody>() != null)
                        {
                            route.Params[method.GetParameters()[i].Name].FromBody = true;
                            route.Params[method.GetParameters()[i].Name].FromJson = true;
                        }
                        Validate.IsNotTrue(route.Params[method.GetParameters()[i].Name].FromJson && route.Params[method.GetParameters()[i].Name].Type.IsAssignableFrom(typeof(Stream)), "Parameter cannot both fromJSON and stream type");
                    }
                }
            }
        }

        protected virtual Task Start()
        {
            return Task.CompletedTask;
        }

        protected virtual Task Stop()
        {
            return Task.CompletedTask;
        }


        [HttpRoute("STATUS_CHECK")]
        public Task STATUS_CHECK(HttpContext context)
        {
            return ResponseJson(context, new STATUS_CHECK() { Name = _name, ProcessID = Process.GetCurrentProcess().Id, State = _state, ID = _ID });
        }

        [HttpRoute("SHUTDOWN")]
        public async Task SHUTDOWN(HttpContext context)
        {
            _state = ServerState.ShuttingDown;
            Log.Info($"Shutdown requested! (ServerID={_ID})");
            await context.Response.WriteAsync("");

            CancellationTokenSource.Cancel();
            await _httpHost.StopAsync();
        }

        protected static async Task ResponseJson(HttpContext context, object obj)
        {
            context.Response.Clear();
            context.Response.ContentType = "application/json";
            var data = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj));
            var ds = System.Text.Encoding.UTF8.GetString(data);
            context.Response.ContentLength = data.Length;
            await context.Response.Body.WriteAsync(data, 0, data.Length);
        }

        protected static async Task Response(HttpContext context, object obj)
        {
            context.Response.Clear();
            context.Response.ContentType = "application/text";
            if (obj != null)
            {
                var data = System.Text.Encoding.UTF8.GetBytes(obj.ToString());
                context.Response.ContentLength = data.Length;
                await context.Response.Body.WriteAsync(data, 0, data.Length);
            }
        }

        public async Task RunHttpServer(CancellationToken token, string address, int port, string name = "")
        {
            _address = address;
            _port = port;

            var builder = new WebHostBuilder()
                .UseKestrel(options =>
                {
                    options.Limits.MaxRequestBodySize = null;
                })
                .UseUrls($"http://{_address}:{_port}")
                .ConfigureLogging(l =>
                {
                    //disable .net core logging
                })
                .ConfigureServices( services =>
                {
                    services.Configure<FormOptions>(options =>
                    {
                        options.MultipartBodyLengthLimit = long.MaxValue;
                    });
                })
                .Configure(app =>
                    {
                        Log.Info($"Starting HTTP Server ({(_name + " " + name).Trim()}) http://{_address}:{_port}");
                        app.Run(async (context) =>
                        {
                            context.Features.Get<IHttpMaxRequestBodySizeFeature>().MaxRequestBodySize = long.MaxValue;
                            try
                            {
                                string path = context.Request.Path.ToString().Replace("/", "").Replace("\\", "");
                                if (path.Length > 0 && _methods.ContainsKey(path))
                                {
                                    var method = _methods[path];
                                    var parameters = new List<object>();
                                    if(method.HasContext)
                                        parameters.Add(context);

                                    foreach (var p in method.Params.Values)
                                    {
                                        if (p.Name == "context")
                                            continue;

                                        object data = null;
                                        if (p.Required && !(context.Request.Query.ContainsKey(p.Name) || (p.FromBody)))
                                        {
                                            Log.Warning($"Requested route {path} is missing required parameter {p.Name}");
                                            return;
                                        }
                                        if (p.FromBody)
                                        {
                                            if (p.Required && context.Request.ContentLength == 0)
                                            {
                                                Log.Warning($"Requested route {path} is missing body data for parameter {p.Name}");
                                                return;
                                            }
                                            if (p.FromJson)
                                            {
                                                var ms = new MemoryStream();
                                                context.Request.Body.CopyTo(ms);
                                                data = Newtonsoft.Json.JsonConvert.DeserializeObject(System.Text.Encoding.UTF8.GetString(ms.ToArray()), p.Type);
                                            }
                                            else if (p.Type.IsAssignableFrom(typeof(Stream)))
                                            {
                                                data = context.Request.Body;
                                            }
                                            else if (p.Type == typeof(byte[]))
                                            {
                                                var ms = new MemoryStream();
                                                context.Request.Body.CopyTo(ms);
                                                data = ms.ToArray();
                                            }
                                            else
                                            {
                                                var ms = new MemoryStream();
                                                context.Request.Body.CopyTo(ms);

                                                data = Util.FromString(System.Text.Encoding.ASCII.GetString(ms.ToArray()), p.Type);
                                            }
                                        }
                                        else
                                        {
                                            if (p.FromJson)
                                            {
                                                data = Newtonsoft.Json.JsonConvert.DeserializeObject(context.Request.Query[p.Name], p.Type);
                                            }
                                            else if (p.Type == typeof(byte[]))
                                            {
                                                data = System.Text.Encoding.ASCII.GetBytes(context.Request.Query[p.Name]);
                                            }
                                            else
                                            {
                                                data = Util.FromString(context.Request.Query[p.Name], p.Type);
                                            }
                                        }
                                        parameters.Add(data);
                                    }
                                    _methods[path].Method.DynamicInvoke(parameters.ToArray());
                                }
                                else
                                    Log.Warning($"Requested unknown route {path}");

                            }
                            catch (Exception e)
                            {
                                context.Response.StatusCode = 500;
                                Log.Exception("Error processing http request", e);
                                await context.Response.WriteAsync("Error");
                            }
                        });
                    });

           
             _httpHost = builder.Build();
            var source = new CancellationTokenSource();

            await Start();
            await _httpHost.StartAsync(CancellationTokenSource.Token);
            while (!CancellationTokenSource.IsCancellationRequested)
            {
                await Task.Delay(10);
            }
            Log.Info("Shutting down...");
            await Stop();
           
        }
        #endregion
    }
}
