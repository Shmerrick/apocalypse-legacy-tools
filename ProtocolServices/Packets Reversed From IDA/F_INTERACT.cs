﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_INTERACT : MythicPacket
    {
        public ushort Unk1 { get; set; }
        public ushort ObjectID { get; set; }
        public ushort Menu1 { get; set; }
        public byte Page { get; set; }
        public byte Num { get; set; }
        public byte Count { get; set; }



        public F_INTERACT()
        {
        }
        public F_INTERACT(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_INTERACT, ms, true)
        {
        }

    }
}
