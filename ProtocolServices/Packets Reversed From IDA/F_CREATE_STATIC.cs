﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarCommon;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{
    public class PascalString : Attribute
    {

    }

    public class NetworkOrder : Attribute
    {

    }

    public class ByteArray : Attribute
    {
        public int Size { get; set; }

        public ByteArray(int size)
        {
            Size = size;
        }
    }

    public class F_CREATE_STATIC : MythicPacket 
    {

        public ushort A00_ObjectID { get; set; }
        public ushort A02_VfxID { get; set; }
        public float A04_Direction { get; set; }
        public ushort A06_Z { get; set; }
        [MField(MFieldType.X,11)]
        public UInt32 A08_X { get; set; }
        [MField(MFieldType.Y, 15)]
        public UInt32 A12_Y { get; set; }
        public ushort A16_DisplayID { get; set; }
        public byte A18_Unk { get; set; }
        public byte A19_Faction { get; set; }
        public UInt32 A20_Unk { get; set; }
        public UInt32 A24_Flags { get; set; }

        public byte A24_Disabled_01 { get; set; }
        public byte A24_Interactive_04 { get; set; }
        public byte A24_Wounds_08 { get; set; }
        public byte A24_Unk_10 { get; set; }
        public byte A24_Sparkle_16 { get; set; }
        public byte A24_Flashing_32 { get; set; }

        public ushort A28_Unk { get; set; }
       // public byte A30_Unk { get; set; }
        public byte A31_Health { get; set; }
        public uint A32_Unk { get; set; }
        public uint A36_Unk { get; set; }

        public uint Type { get; set; }
        public uint DoorID { get; set; }
        //public byte A25_Unk { get; set; }
        //public uint A26_Unk { get; set; }
        //public ushort A30_Unk { get; set; }
        //public ushort A32_Unk { get; set; }
        //public uint A34_Unk { get; set; }

        [PascalString]
        public string A38_Name { get; set; }
        public byte A39_Unk { get; set; }

        public uint A40_data { get; set; }

        public float Scale { get; set; }
        [IgnoreField]
        public int ShiftX { get; set; }
        [IgnoreField]
        public int ShiftY { get; set; }

        public int ZoneID { get; set; }
        public int UniqueID { get; set; }
        public int Instance { get; set; }
        public Dictionary<byte, F_OBJECT_EFFECT_STATE> EffectState = new Dictionary<byte, F_OBJECT_EFFECT_STATE>();
        public F_ACTIVE_EFFECTS ActiveEffects;
        public F_CREATE_STATIC()
        {
        }
        public F_CREATE_STATIC(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_CREATE_STATIC, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;

            A00_ObjectID = FrameUtil.GetUint16(ms); //word 0
            A02_VfxID = FrameUtil.GetUint16(ms); //word2
            A04_Direction = (float)(Units.HEADING_TO_RADIAN * FrameUtil.GetUint16(ms));
            A06_Z = FrameUtil.GetUint16(ms); //word6
            A08_X = FrameUtil.GetUint32(ms); //word8
            A12_Y = FrameUtil.GetUint32(ms);//wordC
            A16_DisplayID = (ushort)(FrameUtil.GetUint16(ms)); //word10
            A18_Unk = FrameUtil.GetUint8(ms); //word14
            A19_Faction = FrameUtil.GetUint8(ms); //word14
            A20_Unk = (ushort)FrameUtil.GetUint32(ms); //word18

            A24_Flags = FrameUtil.GetUint32R(ms) >> 16; //esi + 0x1C

            A24_Disabled_01 = (byte)((A24_Flags & 1) == 1?1:0);
            A24_Interactive_04 = (byte)((A24_Flags & 4) == 4?1:0);
            A24_Wounds_08 = (byte)((A24_Flags & 8) == 8?1:0);
            A24_Unk_10 =(byte)((A24_Flags & 0xA) == 0xA?1:0);
            A24_Sparkle_16 = (byte)((A24_Flags & 0x10) == 0x10?1:0);
            A24_Flashing_32 = (byte)((A24_Flags & 0x20) == 0x20?1:0);

            A28_Unk = FrameUtil.GetUint16(ms); //esi + 0x20
            byte A30_Unk = FrameUtil.GetUint8(ms); //esi + 0x20
            A31_Health = FrameUtil.GetUint8(ms); //esi + 0x20

            if (A30_Unk > 0)
                Scale = (A30_Unk * 2) * 0.0099999998f;
            else
                Scale = (float)1;


            A32_Unk = FrameUtil.GetUint32(ms); //esi + 0x24
            A36_Unk = FrameUtil.GetUint32(ms);
            A38_Name = FrameUtil.GetPascalString(ms);
            Type = FrameUtil.GetUint8(ms);
            if (Type == 4)
            {
                DoorID = FrameUtil.GetUint32(ms);
                ZoneID = ((ushort)(((DoorID >> 52 & 0x3FF))));


            }


        }
    }

    public class F_CREATE_STATIC2 : MythicPacket
    {

        public ushort A00_ObjectID { get; set; }
        public ushort A02_Unk { get; set; }

        public ushort A04_Direction { get; set; }
        public ushort A06_Z { get; set; }
        [MField(MFieldType.X, 11)]
        public UInt32 A08_X { get; set; }
        [MField(MFieldType.Y, 15)]
        public UInt32 A12_Y { get; set; }

        public ushort A14_DisplayID { get; set; }
        public uint A16_Unk { get; set; }
        public ushort A20_Unk { get; set; }

        public ushort A22_Unk { get; set; }

        public uint A26_Unk { get; set; }
        public uint A34_Unk { get; set; }

        [PascalString]
        public string A38_Name { get; set; }
        public byte A39_Unk { get; set; }
        public byte[] A40_data { get; set; }

        [IgnoreField]
        public int ShiftX { get; set; }
        [IgnoreField]
        public int ShiftY { get; set; }
        public F_CREATE_STATIC2()
        {
        }
        public F_CREATE_STATIC2(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_CREATE_STATIC, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;

            A00_ObjectID = FrameUtil.GetUint16(ms);
            A02_Unk = FrameUtil.GetUint16(ms);
            A04_Direction = FrameUtil.GetUint16(ms);
            A06_Z = FrameUtil.GetUint16(ms);
            A08_X = FrameUtil.GetUint32(ms);
            A12_Y = FrameUtil.GetUint32(ms);
            A14_DisplayID = (ushort)FrameUtil.GetUint32R(ms);
            A16_Unk = FrameUtil.GetUint32(ms);
            A20_Unk = (ushort)FrameUtil.GetUint32R(ms);
            A22_Unk = (ushort)FrameUtil.GetUint32R(ms);
            A26_Unk = FrameUtil.GetUint32R(ms);
            A34_Unk = FrameUtil.GetUint32R(ms);
            A38_Name = FrameUtil.GetPascalString(ms);

        }
    }
}
