﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarCommon;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{
    public class F_CREATE_MONSTER:MythicPacket 
    {
      

        public ushort A03_ObjectID { get; set; }
        public ushort A05_Unk1 { get; set; }
        public float A07_Heading { get; set; }
        public ushort A09_Z { get; set; }
        public UInt32 A11_X { get; set; }
        public UInt32 A15_Y { get; set; }
        public ushort A19_SpeedZ { get; set; }
        public ushort A21_ModelID { get; set; }
        public byte A23_Scale { get; set; }
        public byte A24_Level { get; set; }
        public byte A25_Faction { get; set; }
        public ushort A26_Unk2 { get; set; }
        public ushort A28_Unk3 { get; set; }
        public byte A30_Emote { get; set; }
        public byte A31_Unk4 { get; set; }
        public byte A32_Unk5 { get; set; }
        public byte A33_Unk5 { get; set; }
        public byte A34_Unk6 { get; set; }
        public ushort A35_Unk7 { get; set; }
        public ushort A37_Unk8 { get; set; }
        public ushort A39_Unk9 { get; set; }
        public ushort MythicID { get; set; }
        public ushort A43_Unk11 { get; set; }
        public ushort A45_TitleID { get; set; }
        public byte A47_SpawnLength { get; set; }
        public int A48_SpawnData32 { get; set; }
        public byte[] A48_SpawnData { get; set; }
        public byte A49_QuestState { get; set; }
        public string A50_Name { get; set; }
        public ushort A51_Unk { get; set; }
        public byte A53_Unk { get; set; }
        public ushort A54_Owner { get; set; }
        public List<byte> UpdateStateBytes { get; set; }
        public ushort A56_StateSize { get; set; }
        public ushort A57_InventorySize { get; set; }
        public F_OBJECT_STATE A58_State { get; set; }
        public F_PLAYER_INVENTORY A59_Inventory { get; set; }
        public Dictionary<byte, F_OBJECT_EFFECT_STATE> EffectState = new Dictionary<byte, F_OBJECT_EFFECT_STATE>();
        public F_ACTIVE_EFFECTS ActiveEffects;
        public byte[] InventoryBytes { get; set; }
        public byte[] ObjectStateBytes { get; set; }
        public byte WeaponStance { get; set; }
        public List<byte> Z0_Flag29Data { get; set; }
        public byte Z0_Flag29DataCount { get; set; }
        public uint Z1_DormantInfo { get; set; }
        public ushort Z2_UpdateState10 { get; set; }
        public ushort Z3_UpdateState21 { get; set; }
        public byte Z4_UpdateState23 { get; set; }
        public ushort Z5_AnimID { get; set; }
        public ushort Z6_Weapon { get; set; }
        public byte Z7_Flag25DataCount { get; set; }
        public List<byte> Z7_Flag25Data { get; set; }
        public uint Z8_FactionData { get; set; }
        public string Inventory { get; set; }
        public string ZoneID { get; set; }
        public class Item
        {
            public InventorySlotType Slot { get; set; }
            public ushort ModelID { get; set; }
            public byte Color { get; set; }
            public override string ToString()
            {
                return Slot.ToString();
            }
        }

        public List<Item> Items { get; set; }

        public F_CREATE_MONSTER(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_CREATE_MONSTER, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            byte[] data = ms.ToArray();
            ms.Position = 3;

            Z7_Flag25Data = new List<byte>();
            Z0_Flag29Data = new List<byte>();
            A03_ObjectID = FrameUtil.GetUint16(ms);
            A05_Unk1 = FrameUtil.GetUint16(ms);

            A07_Heading = (float)(Units.HEADING_TO_RADIAN * FrameUtil.GetUint16(ms));
            A09_Z = FrameUtil.GetUint16(ms);
            A11_X = FrameUtil.GetUint32(ms);
            A15_Y = FrameUtil.GetUint32(ms);
            A19_SpeedZ = FrameUtil.GetUint16(ms);

            A21_ModelID = FrameUtil.GetUint16(ms);
            A23_Scale = FrameUtil.GetUint8(ms);
            A24_Level = FrameUtil.GetUint8(ms);
            A25_Faction = FrameUtil.GetUint8(ms);

            A26_Unk2 = FrameUtil.GetUint16(ms);
            A28_Unk3 = FrameUtil.GetUint16(ms);
            A30_Emote = FrameUtil.GetUint8(ms);
            A31_Unk4 = FrameUtil.GetUint8(ms);

            A32_Unk5 = FrameUtil.GetUint8(ms);
            A33_Unk5 = FrameUtil.GetUint8(ms);
            A34_Unk6 = FrameUtil.GetUint8(ms);
            A35_Unk7 = FrameUtil.GetUint16(ms);
            A37_Unk8 = FrameUtil.GetUint16(ms);
            A39_Unk9 = FrameUtil.GetUint16(ms);
            MythicID = FrameUtil.GetUint16(ms);
            A43_Unk11 = FrameUtil.GetUint16(ms);
            A45_TitleID = FrameUtil.GetUint16(ms);

            A47_SpawnLength = FrameUtil.GetUint8(ms);
            A48_SpawnData = FrameUtil.GetByteArray(ms, A47_SpawnLength);
            var spawnData = A48_SpawnData.ToList();

            A49_QuestState = FrameUtil.GetUint8(ms);
            A50_Name = FrameUtil.GetCString(ms);


            if (spawnData.Contains(29))
            {
                Z0_Flag29Data = new List<byte>();
                for (int i = 0; i < 15; i++)
                {
                    Z0_Flag29DataCount = 15;
                    Z0_Flag29Data.Add(FrameUtil.GetUint8(ms));
                }
            }
            else
            {
                //If(IsBitSet(28)
                //weapon info
            }

            if ((A33_Unk5 & 1) > 0)
            {
                Z1_DormantInfo = FrameUtil.GetUint32(ms);
            }

            if ((A33_Unk5 & 2) > 0)
            {
                Z2_UpdateState10 = FrameUtil.GetUint16(ms);
            }

            if ((A33_Unk5 & 4) > 0)
            {
                Z3_UpdateState21 = FrameUtil.GetUint16(ms);
            }

            if ((A33_Unk5 & 8) > 0)
            {
                Z4_UpdateState23 = FrameUtil.GetUint8(ms);
            }

            if ((A33_Unk5 & 16) > 0)
            {
                Z5_AnimID = FrameUtil.GetUint16(ms);
            }

            if(spawnData.Contains(24))
                Z6_Weapon = FrameUtil.GetUint16(ms);
            if (spawnData.Contains(25))
            {
                Z7_Flag25Data = new List<byte>();
                Z7_Flag25DataCount = FrameUtil.GetUint8(ms);
                if (Z7_Flag25DataCount > 10)
                    Z7_Flag25DataCount = 10;

                for (int i = 0; i < Z7_Flag25DataCount; i++)
                {
                    Z7_Flag25Data.Add(FrameUtil.GetUint8(ms));
                }
            }

            if ((A25_Faction & 16) > 0)
            {
                Z8_FactionData = FrameUtil.GetUint32(ms);
            }
            //byte prev = 0;
            //UpdateStateBytes = new List<byte>();

            //int count = 0;
            //while (ms.Position < ms.Length)
            //{
            //    var r = FrameUtil.GetUint8(ms);
            //    count++;
            //    UpdateStateBytes.Add(prev);
            //    if (prev == 01 && r == 0xA)
            //    {
            //        UpdateStateBytes.Add(r);
            //        var pos = ms.Position;

            //        var ext = new List<byte>();
            //        while (ms.Position < ms.Length) 
            //        {
            //            var eol = FrameUtil.GetUint8(ms);
            //            ext.Add(eol);
            //            if (eol != 0)
            //            {
            //                if (ext.Count > 3)
            //                {
            //                    UpdateStateBytes.AddRange(ext.Take(ext.Count - 4));
            //                    ms.Position = pos + ext.Count - 4;
            //                    break;
            //                }
            //                else
            //                {
            //                    ms.Position = pos;
            //                    break;
            //                }
            //            }
            //        }

            //        break;
            //    }
            //    prev = r;
            //}

        
            //if (count < 6)
            //    FrameUtil.Skip(ms, 6 - count);

            A53_Unk = FrameUtil.GetUint8(ms);

            A54_Owner = FrameUtil.GetUint16(ms);

            A56_StateSize = 0;
            int pad = 0;
            while (A56_StateSize == 0)
            {

                A56_StateSize = FrameUtil.GetUint8(ms);
                pad++;
            }
            if (pad != 1)
            {
                int sdf = 0;
            }

            A57_InventorySize = FrameUtil.GetUint16R(ms);

            ObjectStateBytes = new byte[A56_StateSize + 3];
            ms.Read(ObjectStateBytes, 3, A56_StateSize);
            A58_State = new F_OBJECT_STATE(new MemoryStream(ObjectStateBytes));

           // ms.Position += A56_StateSize;
            InventoryBytes = new byte[A57_InventorySize + 3];
            ms.Read(InventoryBytes, 3, A57_InventorySize);
            A59_Inventory = new F_PLAYER_INVENTORY(new MemoryStream(InventoryBytes));
            foreach (var inv in A59_Inventory.Items)
            {
                Inventory += inv.ModelID + " ";
            }
            ZoneID = A58_State.ZoneID.ToString();
        }

    }
}
