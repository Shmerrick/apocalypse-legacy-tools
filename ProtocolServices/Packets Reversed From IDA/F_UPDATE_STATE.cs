﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_UPDATE_STATE:MythicPacket 
    {
        public ushort ObjectID { get; set; }
        public byte Type { get; set; }
        public byte Unk01 { get; set; }
        public byte Unk02 { get; set; }
        public byte Unk03 { get; set; }
        public byte Unk04 { get; set; }
        public byte Unk05 { get; set; }
        public byte Unk06 { get; set; }
        public byte Unk07 { get; set; }
        public byte Unk08 { get; set; }
        public byte Unk09 { get; set; }
        public byte Unk10 { get; set; }
        public byte Unk11 { get; set; }
        public byte Unk12 { get; set; }
        public byte Unk13 { get; set; }

        public F_UPDATE_STATE()
        {
        }
        public F_UPDATE_STATE(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_UPDATE_STATE, ms, true)
        {
        }
    }
}
