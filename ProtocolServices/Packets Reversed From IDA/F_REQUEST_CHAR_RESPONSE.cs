﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_REQUEST_CHAR_RESPONSE:MythicPacket 
    {
        public string A03_Username { get; set; }
        public byte A24_Unk { get; set; }
        public byte A25_Unk { get; set; }
        public byte A26_Unk { get; set; }
        public byte A27_Unk { get; set; }
        public byte A28_Unk { get; set; }
        public byte A29_Unk { get; set; }
        public byte A30_Unk { get; set; }
        public byte A31_Unk { get; set; }
        public byte A32_MaxSlot { get; set; }
        public uint A33_Unk { get; set; } //0xFF
        public byte A37_Unk { get; set; } //0x14

        public List<Character> Characters { get; set; }


        public class Character
        {
            public string Name { get; set; }
            public byte Level { get; set; }
            public byte CareerID { get; set; }
            public byte Realm { get; set; }
            public byte Sex { get; set; }
            public byte ModelID { get; set; }
            public ushort ZoneID { get; set; }

            public byte Unk1 { get; set; }
            public byte Unk2 { get; set; }
            public byte Unk3 { get; set; }
            public byte Unk4 { get; set; }
            public byte Unk5 { get; set; }
            public List<Item> Items { get; set; }
            public List<KeyValuePair<ushort,ushort>> ModelIDs { get; set; }
            public List<byte[]> UnkBlock { get; set; }
            public byte[] Unk6 { get; set; }
            public ushort Unk7 { get; set; }
            public byte Unk8 { get; set; }
            public byte Race { get; set; }
            public ushort Unk9 { get; set; }
            public uint Unk10 { get; set; }
            public uint Unk11 { get; set; }
            public byte[] Unk12 { get; set; }
            public class Item
            {
                public uint ModelID { get; set; }
                public ushort PrimaryDye { get; set; }
                public ushort Secondary { get; set; }


                public void Load(MemoryStream ms)
                {
                    ModelID = FrameUtil.GetUint32(ms);
                    PrimaryDye = FrameUtil.GetUint16(ms);
                    Secondary = FrameUtil.GetUint16(ms);
                }

            }
            public void Load(MemoryStream ms)
            {
                Name = FrameUtil.GetString(ms, 48);
                Level = FrameUtil.GetUint8(ms);
                CareerID = FrameUtil.GetUint8(ms);
                Realm = FrameUtil.GetUint8(ms);
                Sex = FrameUtil.GetUint8(ms);
                ModelID = FrameUtil.GetUint8(ms);
                ZoneID = FrameUtil.GetUint16(ms);

                Unk1 = FrameUtil.GetUint8(ms);
                Unk2 = FrameUtil.GetUint8(ms);
                Unk3 = FrameUtil.GetUint8(ms);
              //  Unk4 = FrameUtil.GetUint8(ms);
             //   Unk5 = FrameUtil.GetUint8(ms);

                Items = new List<Item>();
                for (UInt16 slotID = 14; slotID < 30; slotID++)
                {
                    Item item = new Item();
                    item.Load(ms);
                    Items.Add(item);

                }

                UnkBlock = new List<byte[]>();

                byte[] data = FrameUtil.GetByteArray(ms, 6);
                UnkBlock.Add(data);
                for (int i = 0; i < 5; i++)
                {
                    data = FrameUtil.GetByteArray(ms, 8);
                    UnkBlock.Add(data);
                }

                ModelIDs = new List<KeyValuePair<ushort, ushort>>();
                for (UInt16 slotID = 10; slotID < 13; ++slotID)
                {
                    KeyValuePair<ushort, ushort> v = new KeyValuePair<ushort, ushort>(FrameUtil.GetUint16(ms), FrameUtil.GetUint16R(ms));
                    ModelIDs.Add(v);
                }

                Unk6 = FrameUtil.GetByteArray(ms,12);
                Unk7 = FrameUtil.GetUint16(ms);
                Unk8 = FrameUtil.GetUint8(ms);
                Race = FrameUtil.GetUint8(ms);
                Unk9 = FrameUtil.GetUint16(ms);
                Unk10 = FrameUtil.GetUint32(ms);
                Unk11 = FrameUtil.GetUint32(ms);
                Unk12 = FrameUtil.GetByteArray(ms, 14);
            }
        }
        public F_REQUEST_CHAR_RESPONSE(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_REQUEST_CHAR_RESPONSE, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            byte[] data = ms.ToArray();
            ms.Position = 3;

            A03_Username = FrameUtil.GetString(ms, 21);
         //   A24_Unk = FrameUtil.GetUint8(ms);
        //    A25_Unk = FrameUtil.GetUint8(ms);
        //    A26_Unk = FrameUtil.GetUint8(ms);

            A27_Unk = FrameUtil.GetUint8(ms);
            A28_Unk = FrameUtil.GetUint8(ms);
            A29_Unk = FrameUtil.GetUint8(ms);
            A30_Unk = FrameUtil.GetUint8(ms);

            A30_Unk = FrameUtil.GetUint8(ms);


            A32_MaxSlot = FrameUtil.GetUint8(ms);
            A33_Unk = FrameUtil.GetUint32(ms);
            A37_Unk = FrameUtil.GetUint8(ms);
            Characters = new List<Character>();
            for (int i = 0; i < 20; i++)
            {
                Character c = new Character();
                c.Load(ms);
                Characters.Add(c);
            }
        }

    }
}
