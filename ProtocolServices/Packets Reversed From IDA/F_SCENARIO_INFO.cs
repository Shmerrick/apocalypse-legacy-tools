﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_SCENARIO_INFO : MythicPacket
    {
        public uint Duration_Elapsed { get; set; }
        public uint Duration_Shutodwn { get; set; }
        public byte State { get; set; }
        public byte Unk1 { get; set; }
        public ushort PointsToWin { get; set; }
        public ushort OrderPoints { get; set; }
        public ushort DestroPoints { get; set; }


        public byte Unk2 { get; set; }
        public byte Unk3 { get; set; }
        public byte Unk4 { get; set; }

        public ushort UnRead { get; set; }


        public F_SCENARIO_INFO()
        {
        }
        public F_SCENARIO_INFO(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_SCENARIO_INFO, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            Duration_Elapsed = FrameUtil.GetUint32(ms);
            Duration_Shutodwn = FrameUtil.GetUint32(ms);
            State = FrameUtil.GetUint8(ms);
            Unk1 = FrameUtil.GetUint8(ms);
            PointsToWin = FrameUtil.GetUint16(ms);
            OrderPoints = FrameUtil.GetUint16(ms);
            DestroPoints = FrameUtil.GetUint16(ms);
            Unk1 = FrameUtil.GetUint8(ms);
            Unk2 = FrameUtil.GetUint8(ms);
            Unk3 = FrameUtil.GetUint8(ms);
            Unk4 = FrameUtil.GetUint8(ms);

            UnRead = (ushort)(ms.Length - ms.Position);
        }
    }
}
