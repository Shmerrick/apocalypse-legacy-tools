﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_CLIENT_DATA : MythicPacket
    {
        public class SlotData
        {
            public ushort AbilityID { get; set; }
            public ushort Value { get; set; }

            public override string ToString()
            {
                return AbilityID + ": " + Value;
            }
        }
        public ushort A03_Unk{ get; set; }
        public ushort A05_Size { get; set; }
        public byte A07_Unk { get; set; }
        public List<SlotData> Data { get; set; }
        public F_CLIENT_DATA(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_CLIENT_DATA, ms, true)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            Data = new List<SlotData>();
            A03_Unk = FrameUtil.GetUint16(ms);
            A05_Size = FrameUtil.GetUint16(ms);
            A07_Unk = FrameUtil.GetUint8(ms);

            for (int i = 0; i < A05_Size/4; i++)
            {
               
                SlotData data = new SlotData()
                {
                    AbilityID = FrameUtil.GetUint16R(ms),
                    Value = FrameUtil.GetUint16R(ms)
                };
                Data.Add(data);
            }
        }
    }
}
