﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_CAREER_PACKAGE_UPDATE : MythicPacket
    {
        public byte A00_CategoryID { get; set; }
        public byte A01_UpdateType { get; set; }
        public byte A02_PackageID { get; set; }
        public byte A03_Param1 { get; set; }
        public ushort A04_Param2 { get; set; }
        public ushort A06_Param3{ get; set; }
        public uint A08_Param4 { get; set; }
        public uint A12_Param5 { get; set; }
        public uint A16_Param6 { get; set; }


        public F_CAREER_PACKAGE_UPDATE(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_CAREER_PACKAGE_UPDATE, ms, true)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            byte[] data = ms.ToArray();

            A00_CategoryID = FrameUtil.GetUint8(ms);
            A01_UpdateType = FrameUtil.GetUint8(ms);
            A02_PackageID = FrameUtil.GetUint8(ms);
            A03_Param1 = FrameUtil.GetUint8(ms);
            A04_Param2 = FrameUtil.GetUint16R(ms);
            A06_Param3 = FrameUtil.GetUint16(ms);
            A08_Param4 = FrameUtil.GetUint32R(ms);
            A12_Param5 = FrameUtil.GetUint32(ms);
            A16_Param6 = FrameUtil.GetUint32(ms);
           

        }
    }
}
