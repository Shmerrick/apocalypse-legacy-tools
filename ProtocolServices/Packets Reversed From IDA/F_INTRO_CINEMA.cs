﻿using System.IO;
using WarAdmin.Net;

namespace WarAdmin.Packets
{
    public class F_INTRO_CINEMA : MythicPacket
    {
       

        public F_INTRO_CINEMA(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_INTRO_CINEMA, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
           
            ushort id = FrameUtil.GetUint16(ms);
            uint unkID2 = FrameUtil.GetUint32(ms);
            uint unk2 = FrameUtil.GetUint32(ms);

            uint count = FrameUtil.GetUint32(ms);
        }


    }
}
