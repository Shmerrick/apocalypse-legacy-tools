﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;


namespace WarAdmin.Packets
{
    public class F_OBJECT_STATE : MythicPacket
    {
        public ushort ObjectID { get; set; }
        public ushort X { get; set; }
        public ushort Y { get; set; }
        public ushort Z { get; set; }
        public byte Health { get; set; }
        public byte Flags { get; set; }
        public ushort ZoneID { get; set; }
        public byte Unk1 { get; set; }
        public uint Unk2 { get; set; }

        public List<Object> Commands = new List<object>();
        public ushort LookAtObjectID { get; set; }
        public ushort Heading { get; set; }

      
        public byte EffectType { get; set; }
        public byte EffectSubType { get; set; }
        public byte EffectState { get; set; }


        public ushort DestSpeed { get; set; }
        public ushort DestUnk { get; set; }
        public ushort DestX { get; set; }
        public ushort DestY { get; set; }
        public ushort DestZ { get; set; }
        public ushort DestZoneID { get; set; }
        public bool NoGravity { get; set; }
        public bool Recall { get; set; }
         

        public F_OBJECT_STATE(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_OBJECT_STATE, ms, false)
        {
        }

        public F_OBJECT_STATE()
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;

            ObjectID = FrameUtil.GetUint16(ms);
            X = FrameUtil.GetUint16(ms);
            Y = FrameUtil.GetUint16(ms);
            Z = FrameUtil.GetUint16(ms);
            Health = FrameUtil.GetUint8(ms);
            Flags = FrameUtil.GetUint8(ms);
            ZoneID = FrameUtil.GetUint8(ms);
            Unk1 = FrameUtil.GetUint8(ms);
            Unk2 = FrameUtil.GetUint32(ms);

            if ((Flags & 0x1) == 0x0)
            {
                Heading = FrameUtil.GetUint16R(ms);
            }
            if ((Flags & 0x1) == 0x1)
            {
                DestSpeed = FrameUtil.GetUint16R(ms);
                DestUnk = FrameUtil.GetUint8(ms);
                DestX = FrameUtil.GetUint16R(ms);
                DestY = FrameUtil.GetUint16R(ms);
                DestZ = FrameUtil.GetUint16R(ms);
                DestZoneID = FrameUtil.GetUint8(ms);
            }
            if ((Flags & 0x2) == 0x2)
            {
                LookAtObjectID = FrameUtil.GetUint16R(ms); 
            }
            if ((Flags & 0x10) == 0x10)
            {
                EffectType = FrameUtil.GetUint8(ms);
                EffectSubType = FrameUtil.GetUint8(ms);
                EffectState = FrameUtil.GetUint8(ms);
            }
            if ((Flags & 0x20) == 0x20)
            {
                NoGravity = true;
            }
            if ((Flags & 0x40) == 0x40)
            {
                Recall = true;
            }
        }

    }
}
