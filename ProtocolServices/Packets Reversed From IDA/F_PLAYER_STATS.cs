﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{
    public class F_PLAYER_STATS : MythicPacket
    {
        public class StatInfo
        {
            public BonusType Statistic{get;set;}
            public byte StatisticIndex { get; set; }
            public ushort Value { get; set; }

            public override string ToString()
            {
                return Statistic.ToString();
            }
        }
        public byte A03_StatCount { get; set; }
        public byte A04_Speed { get; set; }
        public byte A05_Unk { get; set; }
        public byte A06_Unk { get; set; }
        public byte A07_ArmorBase { get; set; }
        public byte A08_ArmorBonus{ get; set; }
        public byte A09_BolsterLevel { get; set; }
        public byte A10_EffectiveLevel { get; set; }
        public List<StatInfo> Statistics { get; set; }

        public F_PLAYER_STATS(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_PLAYER_STATS, ms, true)
        {
        }

        public static int MaxBonus = 0;
        public override void Load(MemoryStream ms)
        {
           
            Statistics = new List<StatInfo>();
            ms.Position = 3;

            A03_StatCount = FrameUtil.GetUint8(ms);
            A04_Speed = FrameUtil.GetUint8(ms);
            A05_Unk = FrameUtil.GetUint8(ms);
            A06_Unk = FrameUtil.GetUint8(ms);
            A07_ArmorBase = FrameUtil.GetUint8(ms);
            A08_ArmorBonus = FrameUtil.GetUint8(ms);
            A09_BolsterLevel = FrameUtil.GetUint8(ms);
            A10_EffectiveLevel = FrameUtil.GetUint8(ms);



            for (int i = 0; i < A03_StatCount; i++)
            {
                StatInfo data = new StatInfo()
                {
                    StatisticIndex = FrameUtil.GetUint8(ms),
                    Value = FrameUtil.GetUint16(ms)
                };

                if (data.StatisticIndex > MaxBonus)
                {
                    MaxBonus = data.StatisticIndex;
                    Console.WriteLine(MaxBonus);
                }
             //  if (Enum.IsDefined(typeof(BonusType), data.StatisticIndex))
                   data.Statistic = (BonusType)data.StatisticIndex;
                Statistics.Add(data);
            }

        }
    }
}
