﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarCommon;
using WarShared;

namespace WarAdmin.Packets
{
    public class F_FRIENDADD : MythicPacket
    {
        public int data0 { get; set; }
        public int data4 { get; set; }
        public int data8 { get; set; }
        public int data12 { get; set; }
        public int data16 { get; set; }
        public int data18 { get; set; }
        public string dataStr { get; set; }
        public F_FRIENDADD(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_FRIEND_ADD, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            byte[] data = ms.ToArray().Skip(3).ToArray();
            byte[] bdPad = new byte[ms.Length + 100];
            Buffer.BlockCopy(data, 0, bdPad, 0, data.Length);
            BitReader reader = new BitReader();
            reader.eax_data = bdPad;

            data0 = reader.ReadInt(0x20);
            data4 = reader.ReadInt(16);
            data8 = reader.ReadInt(8);
            data12 = reader.ReadInt(8);
            data16 = reader.ReadInt(16);
            data18 = reader.ReadInt(8);

            for (int i = 0; i < data18; i++)
            {
                dataStr += (char)reader.ReadInt(8);
            }
        }

    }
}
