﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;

namespace WarAdmin.Packets
{
    public class MythicPacket
    {
        [IgnoreField]
        public int Sequence { get; set; }
        public class NETWORK_ORDER : Attribute
        { }
        public class CSTRING : Attribute
        { }
        public class IgnoreField : Attribute
        { }

        public enum MFieldType
        {
            CASTER,
            TARGET,
            ABILITY,
            X,
            Y,
            Z,
        }
        public class MField : Attribute
        {
            public int Offset { get; set; }
            public MFieldType Type {get;set;}
            public MField(MFieldType type, int offset)
            {
                Type = type;
                Offset = offset;
            }
        }


          [IgnoreField]
        public WarShared.Protocol.GameOp OP;
          [IgnoreField]
        public ushort Length;

        [IgnoreField]
        public ushort PacketSize;
          [IgnoreField]
        public WarShared.Protocol.GameOp PacketCode;
          [IgnoreField]
        public string OrigString = "";
          [IgnoreField]
        public int Line;
          [IgnoreField]
        public int PrevLine;
          [IgnoreField]
        public int LineDiff;
          [IgnoreField]
        public byte[] Data;

          public uint ZUnread { get; set; }
        public MythicPacket()
        {
        }

        public override string ToString()
        {
            return OP.ToString();
        }

        public MythicPacket(WarShared.Protocol.GameOp op, MemoryStream ms, bool autoLoad)
        {
            Data = ms.ToArray();
            PacketCode = op;
            Load(ms);
            if (autoLoad)
                AutoLoad(ms);
        }
        public virtual void Load(MemoryStream ms)
        {
            PacketSize = (ushort)ms.Length;
            Length = FrameUtil.GetUint16(ms);
            OP = (WarShared.Protocol.GameOp)FrameUtil.GetUint8(ms);
            Data = ms.ToArray();
        }

        public virtual void AutoLoad(MemoryStream ms)
        {
            foreach (PropertyInfo field in this.GetType().GetProperties())
            {
                if (field.GetCustomAttribute(typeof(IgnoreField)) != null)
                {
                    continue;
                }
                if (ms.Position < ms.Length)
                {
                    if (field.Name != "Length" && field.Name != "OP" && field.Name != "OrigString" && field.Name != "Data")
                    {
                        object val = null;
                        bool networkOrder = false;
                          if (field.GetCustomAttribute(typeof(NetworkOrder)) != null)
                              networkOrder = true;


                          switch (field.PropertyType.ToString())
                          {
                              case "System.UInt16":
                                  if (networkOrder)
                                      val = FrameUtil.GetUint16R(ms);
                                  else
                                      val = FrameUtil.GetUint16(ms);
                                  break;
                              case "System.UInt8":
                                  val = FrameUtil.GetUint8(ms);
                                  break;
                              case "System.Byte":
                                  val = FrameUtil.GetUint8(ms);
                                  break;
                              case "System.UInt32":
                                  val = FrameUtil.GetUint32(ms);
                                  break;
                              case "System.Single":
                                  val = FrameUtil.GetFloat(ms);
                                  break;
                              case "System.String":

                                  break;
                          }

                        if (field.GetCustomAttribute(typeof(PascalString)) != null)
                        {
                            field.SetValue(this, FrameUtil.GetPascalString(ms));
                        }

                        ByteArray arr = (ByteArray)field.GetCustomAttribute(typeof(ByteArray));
                        if (arr != null)
                        {
                            field.SetValue(this, FrameUtil.GetByteArray(ms, arr.Size));
                        }
                      
                      
                      
                        if (val != null)
                            field.SetValue(this, val);
                    }
                }
            }
            ZUnread = (uint)(ms.Length - ms.Position);
        }

        public virtual void Serialize(MemoryStream stream)
        {
            FrameUtil.WriteUInt16(stream, (ushort)(Length));
            FrameUtil.WriteByte(stream, (byte)OP);
            foreach (PropertyInfo field in this.GetType().GetProperties())
            {
                bool ignore = field.GetCustomAttribute(typeof(IgnoreField)) != null;
                if (ignore)
                    continue;
                bool isNetworkOrder = field.GetCustomAttribute(typeof(NETWORK_ORDER)) != null;


                switch (field.PropertyType.ToString())
                {
                    case "System.UInt16":
                        if (isNetworkOrder)
                            FrameUtil.WriteUInt16R(stream, (ushort)field.GetValue(this));
                        else
                            FrameUtil.WriteUInt16(stream, (ushort)field.GetValue(this));
                        break;
                    case "System.UInt8":
                        FrameUtil.WriteByte(stream, (byte)field.GetValue(this));
                        break;
                    case "System.Byte":
                        FrameUtil.WriteByte(stream, (byte)field.GetValue(this));
                        break;
                    case "System.UInt32":
                        if (isNetworkOrder)
                            FrameUtil.WriteUInt32R(stream, (uint)field.GetValue(this));
                        else
                            FrameUtil.WriteUInt32(stream, (uint)field.GetValue(this));
                        break;
                    case "System.String":
                        FrameUtil.WritePascalString(stream, (string)field.GetValue(this));
                        break;
                }

            }

        }
    }
}
