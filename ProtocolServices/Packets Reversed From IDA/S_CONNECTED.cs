﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class S_CONNECTED : MythicPacket
    {
        public UInt32 Unk1 { get; set; }
        public UInt32 Tag { get; set; }
        public byte ServerID { get; set; }
        public UInt32 Unk2 { get; set; }
        public string Username { get; set; }
        public string ServerName { get; set; }
        public byte Unk3 { get; set; }
        public ushort Unk4 { get; set; }

        public S_CONNECTED()
        {
        }

        public S_CONNECTED(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.S_CONNECTED, ms, true)
        {
        }
    }
}
