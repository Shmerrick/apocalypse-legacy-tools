using System;
using System.Collections.Generic;
using WarShared;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_GET_ITEM, FrameType.Game)]
    public class F_GET_ITEM : Frame
    {
        private List<PlayerItemData> Items;
        private PlayerItemData EnchantingItem;
        private bool EndEnchanting;
        private bool Store;

        public F_GET_ITEM() : base((int)GameOp.F_GET_ITEM)
        {
        }

        public static void BuildStoreItem(Frame packet, ItemData item)
        {
            var ci = new PlayerItemData();
            ci.Item = item;
            ci.ItemID = item.ID;
            BuildBagItem(packet, ci, true);
        }

        private static void BuildBagItem(Frame packet, PlayerItemData ci, bool store = false, bool enhancing = false, bool endEnhance = false)
        {
            if (!store)
            {
                if (enhancing)
                    packet.WriteUInt16(0xFF);
                else
                    packet.WriteUInt16(ci.SlotIndex1);

                if (ci.Item != null)
                {
                    packet.WriteByte((byte)(!String.IsNullOrEmpty(ci.Item.RepairName) ? 1 : 0));

                    if (!String.IsNullOrEmpty(ci.Item.RepairName))
                    {
                        packet.WritePascalString(ci.Item.RepairName);
                        packet.WriteUInt32(ci.Item.RepairIcon);
                        packet.WriteUInt32(ci.Item.RepairPrice);
                        packet.WriteUInt32(ci.Item.RepairSellPrice);
                    }
                }
                else
                    packet.WriteByte((byte)0);
            }
            else
            {
                packet.WriteByte((byte)ci.SlotIndex1);
            }

            packet.WriteUInt32((uint)ci.ItemID);

            if (ci.ItemID == 0)
                return;

            if (endEnhance)
                packet.WriteByte(0);
            else
                packet.WriteUInt16(ci.Item?.ModelID ?? 0);

            if (ci.ApperanceItem != null)
            {
                packet.WriteUInt16(ci.ApperanceItem.ModelID);
                packet.WriteUInt32((uint)ci.ApperanceItem.ID);
                packet.WritePascalString(ci.ApperanceItem.Name);
            }
            else
            {
                packet.WriteUInt16(0);
                packet.WriteUInt32(0);
                packet.WritePascalString(null);
            }

            packet.WriteUInt16((ushort)ci.Item.SlotIndex);

            if (endEnhance)
                packet.WriteByte(0);
            else
                packet.WriteByte((byte)ci.Item.ItemTypeID);

            if (endEnhance)
                packet.WriteByte(0);
            else
                packet.WriteByte(ci.Item.MinLevel);

            packet.WriteByte(ci.Item.ObjectLevel);
            packet.WriteByte(ci.Item.MinRenownLevel);
            packet.WriteByte(ci.Item.MinRenownLevel);
            packet.WriteBool(ci.Item.UniqueEquipped);
            packet.WriteByte(ci.Item.Rarity);
            packet.WriteUInt16(ci.Item.RaceMask);
            packet.WriteUInt32(ci.Item.TrophyMask);

            if (ci.Item.ItemTypeID == ItemType.ENHANCEMENT || ci.Item.ItemTypeID == ItemType.TROPHY)
                packet.WriteUInt32(ci.Item.CareerMask);

            if (ci.Item.ItemTypeID == ItemType.TROPHY)
            {
                packet.WriteByte(ci.TrophySlot);
                packet.WriteByte(ci.TrophyData);
            }

            packet.WriteUInt16(ci.Item.TintA);
            packet.WriteUInt16(ci.Item.TintB);

            packet.WriteUInt32(ci.Item.SellPrice);
            packet.WriteUInt16(ci.Item.MaxStackCount);
            packet.WriteUInt16(ci.Quantity);
            packet.WriteUInt32((uint)ci.Item.ItemSetID.GetValueOrDefault(0));
            packet.WriteUInt32(ci.Item.Skills);
            packet.WriteUInt16(ci.Item.DPS);
            packet.WriteUInt16(ci.Item.Speed);

            if (endEnhance)
                packet.WriteByte(0);
            else
                packet.WritePascalString(ci.Item.Name);

            if (endEnhance)
                packet.WriteByte(0);
            else
            {
                //stats
                packet.WriteByte((byte)ci.Item.Statistics.Count);
                foreach (var stat in ci.Item.Statistics.Values)
                {
                    packet.WriteByte((byte)stat.BonusTypeID);
                    packet.WriteUInt16((ushort)stat.Value);
                    packet.WriteBool(stat.IsPercent);//is percent
                    packet.WriteUInt32((uint)stat.Duration);//time left
                }
            }

            if (endEnhance)
                packet.WriteByte(0);
            else
            {
                //buffs
                packet.WriteByte((byte)ci.Item.Buffs.Count);
                foreach (var effect in ci.Item.Buffs.Values)
                {
                    packet.WriteUInt16((ushort)effect.AbilityID.GetValueOrDefault(0));  //abilityID
                    packet.WriteUInt32(0);//time left
                }
            }

            if (endEnhance)
                packet.WriteByte(0);
            else
            {
                //abilities
                packet.WriteByte((byte)ci.Item.Abilities.Count);
                foreach (var ciAbility in ci.Item.Abilities.Values)
                {
                    var ability = ciAbility.Ability;
                    packet.WriteUInt16(0); //unk
                    packet.WriteUInt16((ushort)ability.ID);
                    packet.WriteUInt16(0); //cooldown
                    packet.WriteUInt16((ushort)ci.UsedTime);
                }
            }

            if (endEnhance)
                packet.WriteByte(0);
            else
            {
                //crafting
                packet.WriteByte((byte)ci.Item.Crafting.Count); //142
                foreach (var craft in ci.Item.Crafting.Values)
                {
                    packet.WriteByte((byte)craft.Type);
                    packet.WriteUInt16((ushort)craft.Value); //value?
                }

                if (ci.Item.Unk5Set)
                {
                    packet.WriteByte(1);
                    packet.WriteUInt16((byte)ci.Item.Unk5);
                    packet.WriteUInt16((byte)ci.Item.Unk6);
                }
                else
                {
                    packet.WriteByte(0);
                }
            }


            packet.WriteByte((byte)ci.Item.TalismanSlotCount);

            for (int i = 0; i < ci.Item.TalismanSlotCount; i++)
            {
                if (ci.Talismans.ContainsKey(i))
                {
                    byte index = (byte)i;
                    var it = ci.Talismans[i];
                    var taliItem = it.Item;
                    if (it.TempItem != null)
                        taliItem = it.TempItem.Item;

                    packet.WriteUInt32((ushort)taliItem.ModelID);

                    packet.WriteUInt16((ushort)(it.TempItem != null ? i + 1 : 0));
                    packet.WritePascalString(taliItem.Name);

                    //stats
                    packet.WriteByte((byte)taliItem.Statistics.Count);
                    foreach (var stat in taliItem.Statistics.Values)
                    {
                        packet.WriteByte((byte)stat.BonusTypeID);
                        packet.WriteUInt16((ushort)stat.Value);
                        packet.WriteBool(stat.IsPercent);//is percent
                        packet.WriteUInt32(0);//time left
                    }

                    //buffs
                    packet.WriteByte((byte)taliItem.Buffs.Count);
                    foreach (var effect in taliItem.Buffs.Values)
                    {
                        packet.WriteUInt16((ushort)effect.AbilityID);  //abilityID
                        packet.WriteUInt32(0);//time left
                    }

                    //abilities
                    packet.WriteByte((byte)taliItem.Abilities.Count);
                    foreach (var ciAbility in taliItem.Abilities.Values)
                    {
                        var ability = ciAbility.Ability;
                        packet.WriteUInt16((ushort)0); //unk
                        packet.WriteUInt16((ushort)ability.ID);
                        packet.WriteUInt16(0); //cooldown

                        packet.WriteUInt16((ushort)ci.UsedTime);
                    }

                    //crafting
                    packet.WriteByte((byte)taliItem.Crafting.Count); //142
                    foreach (var craft in taliItem.Crafting.Values)
                    {
                        packet.WriteByte((byte)craft.Type);
                        packet.WriteUInt16((ushort)craft.Value); //value?
                    }

                    if (taliItem.Unk5Set)
                    {
                        packet.WriteByte(1);
                        packet.WriteByte((byte)taliItem.Unk5);
                        packet.WriteByte((byte)taliItem.Unk6);
                    }
                    else
                    {
                        packet.WriteByte(0);
                    }

                    packet.WriteByte((byte)0);
                    packet.WriteByte((byte)0);
                }
                else
                {
                    packet.WriteUInt32((ushort)0); //talismanID
                }
            }

            packet.WritePascalString(ci.Item.Description);

            packet.WriteByte((byte)ci.Item.Unk7);
            packet.WriteByte((byte)ci.Item.Unk8);
            packet.WriteByte((byte)0);//ci.Item.NoChargeLeftDontDelete
            packet.WriteByte((byte)ci.ChargesRemaning);

            packet.WriteByte(3); //flag count

            if (ci.Bound)
                packet.WriteByte(0);
            else if (ci.Item.BindOnPickup)
                packet.WriteByte(4);
            else if (ci.Item.BindOnEquip)
                packet.WriteByte(10);
            else
                packet.WriteByte(0);

            if (ci.Item.Dyeable)
                packet.WriteByte(3);
            else
                packet.WriteByte(0);

            packet.WriteByte(0);

            packet.WriteBool(ci.Bound);
            packet.WriteUInt16((ushort)ci.Dye1);
            packet.WriteUInt16((ushort)ci.Dye2);
            packet.WriteUInt16((ushort)ci.Item.CultivatingLevel);
            packet.WriteBool(ci.Item.CultivatingEnabled);
            packet.WriteUInt32((uint)ci.Item.Unk17);

            if (ci.Item.Unk18Set)
            {
                packet.WriteByte(1);
                packet.WriteUInt16((ushort)ci.Item.Unk18);
                packet.WriteUInt16((ushort)ci.Item.Unk19);
                packet.WriteByte((byte)ci.Item.Unk20);
                packet.WriteByte((byte)ci.Item.Unk21);
                packet.WriteByte((byte)ci.Item.Unk22);
                packet.WriteByte((byte)ci.Item.Unk23);
                packet.WriteByte((byte)ci.Item.Unk24);
            }
            else
                packet.WriteByte(0);

            packet.WriteUInt32(ci.TimeLeftBeforeDecay);
            packet.WriteBool(ci.DecayPaused);
            packet.WriteBool(ci.Item.IsTwhoHanded);
        }

        public static F_GET_ITEM Create(List<PlayerItemData> items, bool store = false, PlayerItemData enchantingItem = null, bool endChanting = false)
        {
            return new F_GET_ITEM()
            {
                Items = items,
                EnchantingItem = enchantingItem,
                EndEnchanting = endChanting,
                Store = store
            };
        }

        protected override void SerializeInternal()
        {
            Validate.NotNull(Items, nameof(Items));

            WriteByte((byte)Items.Count);
            Fill(0, 3);

            foreach (var ci in Items)
            {
                Validate.NotNull(ci, nameof(ci));

                BuildBagItem(this, ci, Store, EnchantingItem == ci, EndEnchanting);
            }

        }
    }
}
