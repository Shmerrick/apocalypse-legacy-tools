using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_SURVEY_RESULT, FrameType.Game)]
    public class F_SURVEY_RESULT : Frame
    {
        public F_SURVEY_RESULT() : base((int)GameOp.F_SURVEY_RESULT)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
