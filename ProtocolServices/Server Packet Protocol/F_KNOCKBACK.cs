﻿using WarServer.Game.Entities;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_KNOCKBACK, FrameType.Game)]
    public class F_KNOCKBACK : Frame
    {
        public Entity Caster;
        public Entity Target;
        public ushort Force;
        public byte Angle;
        public byte Gravity;
        public bool SelfPunt;

        public F_KNOCKBACK() : base((int)GameOp.F_KNOCKBACK)
        {
        }

        public static F_KNOCKBACK Create(Entity caster, Entity target, ushort force, byte angle, byte gravity, bool selfPunt = false)
        {
           return new F_KNOCKBACK()
            {
                Caster = caster,
                Target = target,
                Force = force,
                Angle = angle,
                Gravity = gravity,
                SelfPunt = selfPunt
            };
        }

        protected override void SerializeInternal()
        {
            WriteUInt32((uint)(Target.X));   // DefenderWorldPosX (defender will be moved to this pos when kb starts)
            WriteUInt32((uint)(Target.Y));   // DefenderWorldPosY
            WriteUInt32((uint)(Caster.X)); // AttackerWorldPosX (acts as source of knockback)
            WriteUInt32((uint)(Caster.Y));  // AttackerWorldPosY
            Fill(0, 10);
            WriteUInt16(Force);  // KickForce
            Fill(0, 3);
            WriteByte(Angle);
            Fill(0, 3);
            WriteByte((byte)(SelfPunt ? 180 : 0));
            Fill(0, 6);
            WriteUInt16(0);  // KickForce2
            Fill(0, 3);
            WriteByte(Gravity);
            WriteUInt16((ushort)Caster.Z);
            WriteUInt16((ushort)Target.Z);
            Fill(0, 2);
            WriteUInt16(Target.ObjectID);
            WriteByte((byte)(SelfPunt ? 1 : 0));
            Fill(0, 3);
        }
    }
}
