using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.S_WORLD_SENT, FrameType.Game)]
    public class S_WORLD_SENT : Frame
    {
        public S_WORLD_SENT() : base((int)GameOp.S_WORLD_SENT)
        {
        }

        public static S_WORLD_SENT Create()
        {
            return new S_WORLD_SENT();
        }
    }
}
