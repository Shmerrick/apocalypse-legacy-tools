using WarServer.Game.Entities;
using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_JUMP, FrameType.Game)]
    public class F_PLAYER_JUMP : Frame
    {
        private ushort ObjectID;
        private uint X;
        private uint Y;
        private uint Z;
        private float Heading;

        public F_PLAYER_JUMP() : base((int)GameOp.F_PLAYER_JUMP)
        {
        }

        public static F_PLAYER_JUMP Create(Player player, uint x, uint  y, ushort z, float heading)
        {
            return new F_PLAYER_JUMP()
            {
                ObjectID = player.ObjectID,
                X = x,
                Y = y,
                Z = z,
                Heading = heading
            };
        }
		protected override void SerializeInternal()
        {
            WriteUInt32((uint)(X));
            WriteUInt32((uint)(Y));
            WriteUInt16(ObjectID);
            WriteUInt16((ushort)Z);
            WriteUInt16(Util.ToHeading16(Heading));
            Fill(0, 5);
            WriteByte(1);

        }
    }
}
