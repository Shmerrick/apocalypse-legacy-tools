using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_ZONEJUMP, FrameType.Game)]
    public class F_ZONEJUMP : Frame
    {
        public F_ZONEJUMP() : base((int)GameOp.F_ZONEJUMP)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
