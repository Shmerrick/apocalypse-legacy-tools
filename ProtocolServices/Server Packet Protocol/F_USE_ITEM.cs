using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_USE_ITEM, FrameType.Game)]
    public class F_USE_ITEM : Frame
    {
        public ushort SlotIndex;
        public byte Option;
        public byte TargetSlot;

        public F_USE_ITEM() : base((int)GameOp.F_USE_ITEM)
        {
        }
		
        protected override void DeserializeInternal()
        {
            SlotIndex = ReadUInt16();
            Option = ReadByte();
            var a = ReadByte();
            var a1 = ReadByte();
            var a2 = ReadByte();
            var a3 = ReadByte();
            TargetSlot = ReadByte();
        }
    }
}
