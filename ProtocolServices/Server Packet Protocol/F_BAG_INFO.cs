using System.Collections.Generic;
using WarServer.Game.Entities;
using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_BAG_INFO, FrameType.Game)]
    public class F_BAG_INFO : Frame
    {
       // private PlayerData Player = null;
        private BagType Type;
        private Dictionary<BonusType, float> Stats = new Dictionary<BonusType, float>();
        private enum BagType
        {
            BAG,
            STATS
        }

        public F_BAG_INFO() : base((int)GameOp.F_BAG_INFO)
        {
        }

        public static F_BAG_INFO CreateBag(Player player)
        {
            return new F_BAG_INFO()
            {
                Type = BagType.BAG,
               // Player = player.Data
            };
        }

        public static F_BAG_INFO CreateStats(Dictionary<BonusType, float> stats)
        {
            return new F_BAG_INFO()
            {
                Type = BagType.STATS,
                Stats = stats
            };
        }

        protected override void SerializeInternal()
        {
            if (Type == BagType.BAG)
            {
                WriteByte(0x0F);
                WriteByte((byte)180); //32
                WriteUInt16((ushort)180); //16
                WriteByte(0);
                WriteUInt32R((uint)180);//100
                WriteUInt16(2);
                WriteByte(0x50);
                WriteUInt16(0x08);
                WriteUInt16(0x60);
                WriteByte(0xEA);
                WriteUInt16(0);
            }
            else if (Type == BagType.STATS)
            {
                WriteByte(0x19);
                WriteByte((byte)0x62);

                for (int i = 0; i < 98; i++)
                {
                    if (Stats.ContainsKey((BonusType)i) && i > 32)
                    {
                        WriteUInt32R((uint)Stats[(BonusType)i]);
                    }
                    else
                        WriteUInt32R(0);

                    WriteUInt16(0);
                    WriteUInt16(0x803F);
                }
            }
        }
    }
}
