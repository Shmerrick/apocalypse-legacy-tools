using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_MOUNT_UPDATE, FrameType.Game)]
    public class F_MOUNT_UPDATE : Frame
    {
        public ushort ObjectID;
        public ushort MonsterID;
        public ushort SaddleID;

        public F_MOUNT_UPDATE() : base((int)GameOp.F_MOUNT_UPDATE)
        {
        }

        public static F_MOUNT_UPDATE Create(ushort objectID, ushort monsterID, ushort saddleID)
        {
            return new F_MOUNT_UPDATE()
            {
                ObjectID = objectID,
                MonsterID = monsterID,
                SaddleID = saddleID
            };
        }

        protected override void SerializeInternal()
        {
            WriteUInt16(ObjectID);
            WriteUInt16(MonsterID);
            WriteUInt16(SaddleID);
            WriteUInt16(0);
            WriteUInt16(0);
            WriteUInt16(0);
            WriteUInt16(0);
            WriteUInt16(0);
            WriteUInt16(0);
            WriteUInt16(0);
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
