using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_XENON_VOICE, FrameType.Game)]
    public class F_XENON_VOICE : Frame
    {
        public F_XENON_VOICE() : base((int)GameOp.F_XENON_VOICE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
