using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_DEATHSPAM, FrameType.Game)]
    public class F_DEATHSPAM : Frame
    {
        public F_DEATHSPAM() : base((int)GameOp.F_DEATHSPAM)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
