using System;
using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYER_INFO, FrameType.Game)]
    public class F_PLAYER_INFO : Frame
    {
        public ushort Unk1;
        public ushort ObjectID;
        public ushort Unk2;
        public TargetType Type;

        public F_PLAYER_INFO() : base((int)GameOp.F_PLAYER_INFO)
        {
        }

        protected override void DeserializeInternal()
        {
            Unk1 = ReadUInt16();
            ObjectID = ReadUInt16();
            Unk2 = ReadByte();
            int type = ReadByte();
            if (type == 2)
                Type = TargetType.ENEMY;
            else if (type == 3 || type == 1)
                Type = TargetType.FRIENDLY_TARGET;
            else if (type == 0)
                Type = TargetType.NONE;
            else
                throw new Exception("Unknown target type");
        }
    }
}
