using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_INTERACT_QUEUE, FrameType.Game)]
    public class F_INTERACT_QUEUE : Frame
    {
        public byte unk1;
        public byte unk2;
        public byte unk3;
        public byte unk4;
        public byte unk5;
        public byte cmd;
        public ushort id;

        public F_INTERACT_QUEUE() : base((int)GameOp.F_INTERACT_QUEUE)
        {
        }

        protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
            unk1 = ReadByte();
            unk2 = ReadByte();
            unk3 = ReadByte();
            unk4 = ReadByte();
            unk5 = ReadByte();
            cmd = ReadByte();
            id = ReadUInt16();
        }
    }
}
