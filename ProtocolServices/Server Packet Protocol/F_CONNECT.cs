using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CONNECT, FrameType.Game)]
    public class F_CONNECT : Frame
    {
        public uint Tag;
        public string Token;
        public string Username;

        public F_CONNECT() : base((int)GameOp.F_CONNECT)
        {
        }
		
        protected override void DeserializeInternal()
        {
            Skip(8);
            Tag = ReadUInt32();
            Token = ReadString(80);
            Skip(21);
            Username = ReadString(21);
        }
    }
}
