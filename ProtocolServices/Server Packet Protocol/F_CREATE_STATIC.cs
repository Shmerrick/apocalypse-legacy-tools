using WarServer.Game.Entities;
using WarShared;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CREATE_STATIC, FrameType.Game)]
    public class F_CREATE_STATIC : Frame
    {
        private StaticObjectData StaticObject;
        private ushort ObjectID;
        private ushort VfxState;

        public F_CREATE_STATIC() : base((int)GameOp.F_CREATE_STATIC)
        {
        }

        public static F_CREATE_STATIC Create(StaticObject so)
        {
            return new F_CREATE_STATIC()
            {
                StaticObject = so.Data,
                ObjectID = so.ObjectID,
                VfxState = so.VfxState
            };
        }

        protected override void SerializeInternal()
        {
            WriteUInt16((ushort)(ObjectID));
            WriteUInt16(VfxState);
            WriteUInt16(Util.ToHeading16(StaticObject.Heading));
            WriteUInt16((ushort)(StaticObject.Z));
            WriteUInt32((uint)(StaticObject.X));
            WriteUInt32((uint)(StaticObject.Y));
            WriteUInt16((ushort)StaticObject.ModelID);

            WriteByte((byte)StaticObject.Unk1);
            WriteByte((byte)0); //faction
            WriteUInt32((uint)StaticObject.Unk2);
            WriteUInt32R((uint)StaticObject.Flags << 16);
            WriteUInt16((ushort)StaticObject.Unk3);

            WriteByte((byte)(StaticObject.Size / 0.0099999998f / 2f));
            WriteByte(100); //wounds
            WriteUInt32((uint)StaticObject.Unk4);
            WriteUInt32((uint)StaticObject.Unk5);
            WritePascalString(StaticObject.Name);

            if (StaticObject.DoorID != 0)
            {
                WriteByte(0x04);
                WriteUInt32((uint)StaticObject.DoorID);
            }
            else
                WriteByte(0x0);
        }
    }
}
