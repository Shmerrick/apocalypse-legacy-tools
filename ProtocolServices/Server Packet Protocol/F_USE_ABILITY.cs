using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_USE_ABILITY, FrameType.Game)]
    public class F_USE_ABILITY : Frame
    {
        private ushort AbilityID;
        private ushort CasterID;
        private ushort TargetID;
        private ushort EffectID;
        private AbilityActionInfo Info;
        private AbilityResult Result;
        private ushort Time;
        private byte Sequence;
        private byte Unk1;

        public F_USE_ABILITY() : base((int)GameOp.F_USE_ABILITY)
        {
        }

        public static F_USE_ABILITY Create(ushort casterID, ushort targetID, ushort abilityID, ushort effectID, AbilityActionInfo info, AbilityResult result, ushort time, byte sequence, byte unk = 1)
        {
            return new F_USE_ABILITY()
            {
                AbilityID = abilityID,
                CasterID = casterID,
                TargetID = targetID,
                EffectID = effectID,
                Info = info,
                Result = result,
                Time = time,
                Sequence = sequence,
                Unk1 = unk
            };
        }

        protected override void SerializeInternal()
        {
            WriteUInt16(0);
            WriteUInt16(AbilityID);
            WriteUInt16(CasterID);
            WriteUInt16(EffectID);
            WriteUInt16(TargetID);
            WriteByte((byte)Info);
            WriteByte(Unk1); //0, 1, 2
            WriteUInt16((ushort)Result);
            WriteUInt16(Time);
            WriteByte(Sequence);
            WriteUInt16(0);
            WriteByte(0);
        }
    }
}
