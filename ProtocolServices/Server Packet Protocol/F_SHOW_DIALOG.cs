using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_SHOW_DIALOG, FrameType.Game)]
    public class F_SHOW_DIALOG : Frame
    {
        public DialogType Type { get; set; }
        public string Message { get; set; }
        public uint Time { get; set; }

        public F_SHOW_DIALOG() : base((int)GameOp.F_SHOW_DIALOG)
        {
        }

        public static F_SHOW_DIALOG CreateGroupInvite(string message)
        {
            return new F_SHOW_DIALOG()
            {
                Type = DialogType.GroupInvite,
                Message = message
            };

        }

        public static F_SHOW_DIALOG CreateRespawn(uint expireTime)
        {
            return new F_SHOW_DIALOG()
            {
                Type = DialogType.Respawn,
                Time = expireTime
            };
        }

        protected override void SerializeInternal()
        {
            WriteUInt16((ushort)Type);

            if (Type == DialogType.GroupInvite)
            {
                WriteByte(0);
                WritePascalString(Message);
            }
            else if (Type == DialogType.Respawn)
            {
                WriteUInt32(Time);
            }
        }

    }
}
