using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.MAX_GAME_OPCODE, FrameType.Game)]
    public class MAX_GAME_OPCODE : Frame
    {
        public MAX_GAME_OPCODE() : base((int)GameOp.MAX_GAME_OPCODE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
