using WarServer.Game.Entities;
using WarShared;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.S_PLAYER_INITTED, FrameType.Game)]
    public class S_PLAYER_INITTED : Frame
    {
        private ushort ObjectID;
        private PlayerData Player;
        private string ServerInfoName;
        private RegionData Region;
        private float X;
        private float Y;
        private float Z;
        private float Heading;

        public S_PLAYER_INITTED() : base((int)GameOp.S_PLAYER_INITTED)
        {
        }

        public static S_PLAYER_INITTED Create(Player player)
        {
            return new S_PLAYER_INITTED()
            {
                Player = player.Data,
                ObjectID = player.ObjectID,
                ServerInfoName = player.Client.ServerInfo.Name,
                X = player.Client.JumpToRegion == null ? player.Client.PlayerData.X : player.Client.JumpToX,
                Y = player.Client.JumpToRegion == null ? player.Client.PlayerData.Y : player.Client.JumpToY,
                Z = player.Client.JumpToRegion == null ? player.Client.PlayerData.Z : player.Client.JumpToZ,
                Heading = player.Client.JumpToRegion == null ? player.Client.PlayerData.Heading : player.Client.JumpToHeading,
                Region = player.Client.JumpToRegion == null ? player.Client.PlayerData.Zone.Region : player.Client.JumpToRegion,
            };
        }
		protected override void SerializeInternal()
        {
            WriteUInt16(ObjectID);
            WriteUInt16(0);
            WriteUInt32((uint)Player.ID);

            WriteUInt16((ushort)Z);
            WriteUInt16(0);

            WriteUInt32((uint)X);
            WriteUInt32((uint)Y);

            WriteUInt16((ushort)Util.ToHeading16(Heading));
            WriteByte(0);
            WriteByte((byte)Player.Career.Realm);

            WriteUInt16(0);
            WriteUInt16(0);

            WriteUInt16((ushort)Region.ID);
            WriteUInt16((ushort)0);
            WriteUInt16((ushort)Player.Career.ID);
            Fill(0, 6);
            WritePascalString(ServerInfoName);
            Fill(0, 3);
        }
    }
}
