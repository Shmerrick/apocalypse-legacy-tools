﻿using System;
using System.IO;
using System.Text;
using WarShared.Utility;

namespace WarShared.Net
{
    public class Packet
    {
        #region fields
        protected int _offset;
        protected byte[] _data = new byte[8000];
        private int bitIndexByte;
        private int bitIndexBuffer; 
        protected int _currentBitIndex;
        #endregion

        #region properties
        public int Size { get; private set; } = 0;

        public int CurrentByteIndex
        {
            get
            {
                return (int)Math.Floor((float)_currentBitIndex / 8);
            }
        }

        public int CurrentBitOffset
        {
            get
            {
                return _currentBitIndex % 8;
            }
        }

        public byte[] Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
                Size = _data.Length;
            }
        }

        public int Offset
        {
            get
            {
                return _offset;
            }
            set
            {
                _offset = value;
                _currentBitIndex = _offset * 8;
            }
        }
        #endregion

        public Packet()
        {
        }

        public Packet(int size)
        {
            if (size > 0)
                _data = new byte[size];
            Size = size;
        }

        #region bit packed methods
        protected void CommitBitIndex()
        {
            _offset = (int)Math.Ceiling((double)_currentBitIndex / 8.0);
        }

        public byte ReadBit()
        {
            int ecx = 0, edx = 0, ebx = 0;
            bool val = false;

            ecx = bitIndexByte;
            edx = _offset;
            ebx++;
            ebx = ebx << ecx;

            if (edx >= _data.Length)
                return 0;
            if ((ebx & _data[edx]) != 0)
                val = true;

            ecx = bitIndexByte;
            bitIndexBuffer++;
            ecx++;
            edx = ecx;
            edx = edx >> 3;
            _offset += edx;
            ecx = ecx & 7;
            bitIndexByte = ecx;

            return (byte)(val ? 1 : 0);
        }

        public int ReadBitIntClamped(int min, int max)
        {
            int eax = 0, ecx = 0, edx = 0;

            eax = max;
            eax = eax - min;
            if (eax < 0)
                eax = min - max;

            eax++;
            edx = 0x20;
            max = eax;
            ecx = 0;

            //find out how many bits min->max fits in
            while (eax >= 1 << ecx && ecx < 32)
                ecx++;

            edx = ecx;
            var val = ReadBitInt(edx);
            return min + val;
        }

        public int ReadBitSigned(int size)
        {
            byte sign = ReadBit();
            int result = ReadBitInt(size - 1);

            if (sign > 0)
                result = -result;

            return result;
        }

        public float ReadBitAngleRad(int size)
        {
            int eax = 0, ecx = 0;
            int result = ReadBitSigned(size);

            float xmm0 = (float)result;
            eax = 1;
            ecx = size - 1;
            eax = eax << ecx;
            eax--;
            float xmm1 = (float)eax;
            xmm0 = (xmm0 / xmm1) * 6.2831855f;

            return (float)xmm0;
        }

        public int ReadBitInt(int size)
        {
            if (_offset >= _data.Length)
                return 0;
            int var4 = 0;
            int ecx = 0, edx = 0, edi = 0, ebx = 0;
            int value = 0;
            while (size > 0)
            {
                ecx = 8;
                edx = ecx;
                edx = edx - bitIndexByte;
                edi = 0xFF;

                if (size < edx)
                    edx = size;

                ecx = ecx - edx;
                size = size - edx;
                edi = edi >> (ushort)ecx;

                ecx = _offset;
                if (ecx >= _data.Length)
                    return 0;
                ebx = _data[ecx];
                ecx = bitIndexByte;
                ebx = ebx >> (byte)ecx;

                ecx = var4;
                var4 = var4 + edx;

                edi = edi & ebx;
                edi = edi << ecx;

                value += edi;

                ecx = bitIndexByte;
                bitIndexBuffer += edx;
                ecx += edx;
                edx = ecx;
                edx = edx >> 3;
                _offset += edx;

                ecx = ecx & 7;
                bitIndexByte = ecx;
            }

            return value;
        }

        public string ReadBitString(int length)
        {
            string result = "";
            for (int i = 0; i < length; i++)
            {
                char c = (char)ReadBitInt(8);
                if (c != '\n')
                    result += c;
            }
            return result;
        }

        public byte[] ToArray()
        {
            byte[] buffer = new byte[CurrentByteIndex + 2];
            Buffer.BlockCopy(_data, 0, buffer, 0, CurrentByteIndex + 1);
            return buffer;
        }

        public void WriteBit(int value)
        {
            WriteBit(value > 0);
        }

        public void WriteBit(bool value)
        {
            int currentByteIndex = (int)Math.Floor((float)_currentBitIndex / 8);
            int currentBitOffset = _currentBitIndex % 8;
            int val = value ? 1 : 0;
            _data[currentByteIndex] |= (byte)(val << currentBitOffset);
            _currentBitIndex++;
        }

        public void WriteBitStringLength(string str)
        {
            if (str != null && str.Length > 0)
                WriteBitInt(str.Length + 1, 8);
            else
                WriteBitInt(0, 8);
        }

        public void WriteBitInt(int value, int bits)
        {
            for (int i = 0; i < bits; i++)
            {
                int currentByteIndex = (int)Math.Floor((float)_currentBitIndex / 8);
                int currentBitOffset = _currentBitIndex % 8;

                int val = (value >> i) & 1;
                _data[currentByteIndex] |= (byte)(val << currentBitOffset);
                _currentBitIndex++;
            }
        }

        public void WriteAngleRad(float value, int bits)
        {
            var cap = (double)((1 << (bits - 1)) - 1);
            var r = (double)(value * cap);
            var p = r / 6.2831855;
            WriteIntSigned((int)Math.Round(p, MidpointRounding.AwayFromZero), bits);
        }

        public void WriteIntClamped(int value, int min, int max)
        {
            int eax = 0, bits = 0, edx = 0;

            eax = max;
            eax = eax - min;
            if (eax < 0)
                eax = min - max;

            eax++;
            edx = 0x20;
            max = eax;
            bits = 0;

            //find out how many bits value fits in
            while (eax >= 1 << bits && bits < 32)
                bits++;

            edx = bits;

            WriteBitInt(value - min, bits);
        }

        public void WriteBitString(string str)
        {
            if (str != null && str.Length > 0)
            {
                for (int i = 0; i < str.Length; i++)
                    WriteBitInt((int)str[i], 8);
                WriteBitInt(0, 8);
            }
        }

        public void WriteIntSigned(int value, int bits)
        {
            if (value < 0)
            {
                WriteBit(1);
                WriteBitInt(-value, bits - 1);
            }
            else
            {
                WriteBit(0);
                WriteBitInt(value, bits - 1);
            }
        }
        #endregion

        #region methods
        public string ReadString(int size)
        {
            if (_offset + size > _data.Length)
                size = _data.Length - _offset;

            Validate.ArrayRange(_offset, size, _data.Length, nameof(ReadString));

            string result = System.Text.ASCIIEncoding.ASCII.GetString(_data, _offset, size);
            int cstrIndex = result.IndexOf('\0');
            if (cstrIndex > 0)
                result = result.Remove(cstrIndex);

            _offset += size;
            return result;
        }

        public string ReadCString()
        {
            int size = 0;
            for (int i = _offset; i < _data.Length; i++)
            {
                if (_data[i] == '\0')
                    break;
                size++;
            }
            string result = System.Text.ASCIIEncoding.ASCII.GetString(_data, _offset, size);
            _offset += size;
            return result;
        }

        public void Skip(int size)
        {
            Validate.ArrayRange(_offset, size, _data.Length, nameof(Skip));

            _offset += size;
        }

        public byte ReadByte()
        {
            Validate.ArrayRange(_offset, 1, _data.Length, nameof(ReadByte));

            return _data[_offset++];
        }

        public bool Read(byte[] destBuffer, int size)
        {
            //TODO: Check buffer size
            if (size + _offset > _data.Length)
                return false;

            System.Buffer.BlockCopy(_data, _offset, destBuffer, 0, size);
            _offset += size;
            return true;
        }


        public ushort ReadUInt16()
        {
            Validate.ArrayRange(_offset, 2, _data.Length, nameof(ReadUInt16));

            ushort result = (ushort)(_data[_offset] << 8 | _data[_offset + 1]);
            _offset += 2;
            return result;
        }

        public ushort ReadUInt16R()
        {
            Validate.ArrayRange(_offset, 2, _data.Length, nameof(ReadUInt16R));

            ushort result = (ushort)(_data[_offset + 1] << 8 | _data[_offset]);
            _offset += 2;
            return result;
        }

        public bool ReadBool()
        {
            return ReadByte() > 0;
        }

        public uint ReadUInt32()
        {
            Validate.ArrayRange(_offset, 4, _data.Length, nameof(ReadUInt32));

            uint result = (uint)(_data[_offset] << 24 | _data[_offset + 1] << 16 | _data[_offset + 2] << 8 | _data[_offset + 3]);
            _offset += 4;
            return result;
        }


        public int ReadInt32()
        {
            Validate.ArrayRange(_offset, 4, _data.Length, nameof(ReadInt32));

            int result = (int)(_data[_offset] << 24 | _data[_offset + 1] << 16 | _data[_offset + 2] << 8 | _data[_offset + 3]);
            _offset += 4;
            return result;
        }

        public uint ReadUInt32R()
        {
            Validate.ArrayRange(_offset, 4, _data.Length, nameof(ReadUInt32R));

            uint result = (uint)(_data[_offset + 3] << 24 | _data[_offset + 2] << 16 | _data[_offset + 1] << 8 | _data[_offset]);
            _offset += 4;
            return result;
        }

        public long ReadUInt64()
        {
            Validate.ArrayRange(_offset, 8, _data.Length, nameof(ReadUInt64));

            long result = (long)((long)_data[_offset] << 56
                | (long)_data[_offset + 1] << 48
                | (long)_data[_offset + 2] << 40
                | (long)_data[_offset + 3] << 32
                | (long)_data[_offset + 4] << 24
                | (long)_data[_offset + 5] << 16
                | (long)_data[_offset + 6] << 8
                | (long)_data[_offset + 7]);
            _offset += 8;
            return result;
        }

        public long ReadUInt64R()
        {
            Validate.ArrayRange(_offset, 8, _data.Length, nameof(ReadUInt64R));

            long result = (long)((long)_data[_offset + 7] << 56
                | (long)_data[_offset + 6] << 48
                | (long)_data[_offset + 5] << 40
                | (long)_data[_offset + 4] << 32
                | (long)_data[_offset + 3] << 24
                | (long)_data[_offset + 2] << 16
                | (long)_data[_offset + 1] << 8
                | (long)_data[_offset]);
            _offset += 8;
            return result;
        }
        public void WriteByte(byte value)
        {
            CheckSize(1);
            _data[_offset++] = value;
        }

        public void Read(byte[] dest, int destOffset, int size)
        {
            Buffer.BlockCopy(_data, _offset, dest, destOffset, size);
        }
        private void CheckSize(int size)
        {
            if (size + _offset > _data.Length)
            {
                Array.Resize(ref _data, size + _offset + 1024);
            }
        }

        public byte[] _orig;
        public void Write(byte[] data, int srcOffset, int size)
        {
            CheckSize(size);

            System.Buffer.BlockCopy(data, srcOffset, _data, _offset, size);
            _orig = new byte[_data.Length+size];
            System.Buffer.BlockCopy(data, srcOffset, _orig, _offset, size);

            _offset += size;
        }

        public void WriteBool(bool value)
        {
            WriteByte((byte)(value ? 1 : 0));
        }
        public void WriteUInt16(ushort value)
        {
            CheckSize(2);

            _data[_offset] = (byte)(value >> 8);
            _data[_offset + 1] = (byte)(value);
            _offset += 2;
        }
        public void WriteUInt16R(ushort value)
        {
            CheckSize(2);

            _data[_offset] = (byte)(value & 0xFF);
            _data[_offset + 1] = (byte)(value >> 8);
            _offset += 2;
        }

        public void WriteUInt32(UInt32 value)
        {
            CheckSize(4);

            _data[_offset] = (byte)(value >> 24);
            _data[_offset + 1] = (byte)(value >> 16);
            _data[_offset + 2] = (byte)(value >> 8);
            _data[_offset + 3] = (byte)(value);
            _offset += 4;
        }

        public void WriteInt32(int value)
        {
            CheckSize(4);

            _data[_offset] = (byte)(value >> 24);
            _data[_offset + 1] = (byte)(value >> 16);
            _data[_offset + 2] = (byte)(value >> 8);
            _data[_offset + 3] = (byte)(value);
            _offset += 4;
        }

        public void WriteUInt32R(uint value)
        {
            CheckSize(4);

            _data[_offset + 3] = (byte)(value >> 24);
            _data[_offset + 2] = (byte)(value >> 16);
            _data[_offset + 1] = (byte)(value >> 8);
            _data[_offset] = (byte)(value);
            _offset += 4;
        }

        public void WriteFloat(float value)
        {
            CheckSize(4);

            var bytes = BitConverter.GetBytes(value);
            _data[_offset + 3] = bytes[0];
            _data[_offset + 2] = bytes[1];
            _data[_offset + 1] = bytes[2];
            _data[_offset] = bytes[3];
            _offset += 4;
        }

        public void WriteUInt24R(uint value)
        {
            CheckSize(3);

            _data[_offset + 2] = (byte)(value >> 16);
            _data[_offset + 1] = (byte)(value >> 8);
            _data[_offset] = (byte)(value);
            _offset += 3;
        }

        public void WriteUInt64(long value)
        {
            CheckSize(8);

            _data[_offset] = (byte)(value >> 56);
            _data[_offset + 1] = (byte)(value >> 48);
            _data[_offset + 2] = (byte)(value >> 40);
            _data[_offset + 3] = (byte)(value >> 32);
            _data[_offset + 4] = (byte)(value >> 24);
            _data[_offset + 5] = (byte)(value >> 16);
            _data[_offset + 6] = (byte)(value >> 8);
            _data[_offset + 7] = (byte)(value);
            _offset += 8;
        }

        public void WriteUInt64R(long value)
        {
            CheckSize(8);

            _data[_offset + 7] = (byte)(value >> 56);
            _data[_offset + 6] = (byte)(value >> 48);
            _data[_offset + 5] = (byte)(value >> 40);
            _data[_offset + 4] = (byte)(value >> 32);
            _data[_offset + 3] = (byte)(value >> 24);
            _data[_offset + 2] = (byte)(value >> 16);
            _data[_offset + 1] = (byte)(value >> 8);
            _data[_offset] = (byte)(value);
            _offset += 8;
        }

        public string ReadPascalString()
        {
            byte length = ReadByte();
            string msg = "";
            if (length > 0)
            {
                msg = ReadString(length);
            }
            return msg;
        }
        public string ReadPascalString32()
        {
            uint length = ReadUInt32();
            string msg = "";
            if (length > 0)
            {
                msg = ReadString((int)length);
            }
            return msg;
        }

        public byte[] ReadByteArray()
        {
            uint length = ReadUInt32();

            if (length > 0)
            {
                byte[] data = new byte[length];
                Read(data, (int)length);
                return data;
            }
            return null;
        }

        public byte[] ReadByteArray(int length)
        {
            if (length > 0)
            {
                byte[] data = new byte[length];
                Read(data, (int)length);
                return data;
            }
            return null;
        }

        public void WriteByteArray(byte[] data)
        {
            if (data != null)
            {
                CheckSize(data.Length);
                WriteUInt32((uint)data.Length);
                Write(data, 0, (int)data.Length);
            }
            else
            {
                WriteUInt32(0);
            }
        }

        public void WritePascalString(string str)
        {
            if (str == null || str.Length <= 0)
            {
                WriteByte(0);
                return;
            }

            WriteByte((byte)str.Length);
            System.Buffer.BlockCopy(Encoding.ASCII.GetBytes(str), 0, _data, _offset, str.Length);
            _offset += str.Length;
        }

        public void WritePascalString16(string str)
        {
            if (str == null || str.Length <= 0)
            {
               WriteUInt16(0);
                return;
            }

            WriteUInt16((ushort)str.Length);
            System.Buffer.BlockCopy(Encoding.ASCII.GetBytes(str), 0, _data, _offset, str.Length);
            _offset += str.Length;
        }

        public void WritePascalStringC(string str)
        {
            if (str == null || str.Length <= 0)
            {
                WriteByte(0);
                return;
            }

            WriteByte((byte)str.Length);
            System.Buffer.BlockCopy(Encoding.ASCII.GetBytes(str), 0, _data, _offset, str.Length);
            _offset += str.Length;
            WriteByte(0);
        }

        public void WritePascalString32(string str)
        {
            if (str == null || str.Length <= 0)
            {
                WriteUInt32(0);
                return;
            }

            WriteUInt32R((uint)str.Length);
            System.Buffer.BlockCopy(Encoding.ASCII.GetBytes(str), 0, _data, _offset, str.Length);
            _offset += str.Length;
        }

        public void Fill(byte val, int length)
        {
            CheckSize(length);

            for (int i = 0; i < length; i++)
                _data[i + _offset] = val;
            _offset += length;
        }

        public void FillString(string str, int length)
        {
            if (str == null || str.Length <= 0)
            {
                WriteByte(0);
                return;
            }
            CheckSize(length);
            Array.Clear(_data, _offset, length);
            System.Buffer.BlockCopy(Encoding.ASCII.GetBytes(str), 0, _data, _offset, str.Length);

            _offset += length;
        }
        public void WriteString(string str)
        {
            CheckSize(str.Length);
            System.Buffer.BlockCopy(Encoding.ASCII.GetBytes(str), 0, _data, _offset, str.Length);

            _offset += str.Length;
        }

        public void WriteCString(string str)
        {
            if (str == null)
            {
                WriteByte(0);
            }
            else
            {
                var data = Encoding.GetEncoding("iso-8859-1").GetBytes(str);
                Write(data, 0, data.Length);
                WriteByte(0);
            }

        }
        public void WriteString(string str, int maxLength)
        {
            if (str == null || maxLength <= 0)
            {
                //     WriteByte(0);
                return;
            }
            if (maxLength > str.Length)
                maxLength = str.Length;
            //  Array.Clear(_data, _offset, length);
            System.Buffer.BlockCopy(Encoding.ASCII.GetBytes(str), 0, _data, _offset, maxLength);

            _offset += maxLength;
        }

        public void WriteVarUInt32(uint val)
        {
            if (val == 0)
            {
                WriteByte(0);
                return;
            }

            while (val > 0)
            {
                WriteByte((byte)((val & 0x7F) ^ ((val > 0x7F) ? 0x80 : 0x00)));
                val = val >> 7;
            }
        }

        public void WriteVarInt32( int value)
        {


            if (value < 0)
            {
                value = (-value << 1) | 0x1;
            }
            else
                value = value << 1;

            WriteVarUInt32((uint)value);
        }


        public void WriteZigZag(int val)
        {
            byte sign = (byte)((val < 0) ? 1 : 0);
            if (sign == 1)
                val++;
            val = Math.Abs(val);
            WriteByte((byte)(((val << 1) & 0x7F) ^ ((val > 0x3F) ? 0x80 : 0x00) + sign));
            val = val >> 6;

            while (val > 0)
            {
                WriteByte((byte)((val & 0x7F) ^ ((val > 0x7F) ? 0x80 : 0x00)));
                val = val >> 7;
            }
        }
    }

    public class Frame : Packet
    {
        public int Op;
        public int HeaderPacketSize;
        public ushort Header_Sequence;
        public ushort Header_SessionId;
        public ushort Header_Unk1;
        public byte Header_Unk2;
        public bool IsIn = false;
        public DateTime CreatedDate;

        protected virtual void SerializeInternal()
        {
        }

        protected virtual void DeserializeInternal()
        {
        }

        public Frame()
        {
            CreatedDate = DateTime.Now;
        }

        public Frame(int op)
        {
            CreatedDate = DateTime.Now;
            Op = op;
        }

        public Frame(int op, int size) : base(size)
        {
            CreatedDate = DateTime.Now;
            Op = op;
        }

        public virtual byte[] Serialize()
        {
            Offset = 0;
            WriteByte((byte)Op);
            SerializeInternal();
            return _data;
        }

        public string ToServerPacket()
        {
            var data = new byte[Offset + 2];
            MemoryStream ms = new MemoryStream(data);
            BinaryWriter w = new BinaryWriter(ms);
            w.Write((ushort)(Offset - 1));
            w.Write(Data, 0, Offset);
            return "[Server] packet : (0x" + ((int)Op).ToString("X").PadLeft(2, '0') + ") " + ((Protocol.GameOp)Op).ToString() + " Size = " + Offset + " T = " 
                + CreatedDate.ToString("ss:fff") + "\r\n" + Util.Hex(data, 0, data.Length) + "\r\n";
        }

        public string ToClientPacket()
        {
            var data = new byte[Offset + 10];
            MemoryStream ms = new MemoryStream(data);
            BinaryWriter w = new BinaryWriter(ms);

            FrameUtil.WriteUInt16(ms, (ushort)Offset);
            ms.Position += 7;
            w.Write((byte)Op);
            w.Write(Data, 0, Offset);
            return "[Client] packet : (0x" + ((int)Op).ToString("X").PadLeft(2, '0') + ") " + ((Protocol.GameOp)Op).ToString() + " Size = " + (Offset + 10) + " T = " 
                + CreatedDate.ToString("ss:fff") + "\r\n" + Util.Hex(data, 0, data.Length) + "\r\n";
        }

        public void Deserialize()
        {
            Offset = 0;
            DeserializeInternal();
        }
        #endregion
    }
}
