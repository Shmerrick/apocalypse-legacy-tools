
[Client] packet : (0x01) UNKNOWN  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 63 61 00 DD 00 00 00 01 00 5F 94 71 00 00 |..ca......._.q..|
|00 00 00 00 00 00 00 00 04 03 00 00 17 C2 16 64 |...............d|                                                
-------------------------------------------------------------------
[Server] packet : (0x00) UNKNOWN  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 00 00 5F 94 71 AA E5 F7 97 00 00 00 00 00 |...._.q.........|
|00 04 03 00 00 17 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0x6F) F_LOCALIZED_STRING  Size = 13 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0A 6F 00 00 00 00 03 B9 00 00 00 00          |.............   |
-------------------------------------------------------------------
[Server] packet : (0xD8) F_GROUP_STATUS  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 D8 3E C8 01 03 00 02 00 00 00 00 00 01 00 |...>............|
|1B 32 1C 00 25 AF 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0x51) F_ANIMATION  Size = 9 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 06 51 0F E7 00 AC 00 00                      |.........       |
-------------------------------------------------------------------
[Server] packet : (0x51) F_ANIMATION  Size = 9 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 06 51 0D 2B 00 6C 00 00                      |.........       |
-------------------------------------------------------------------
[Server] packet : (0x51) F_ANIMATION  Size = 9 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 06 51 01 3C 00 B0 00 00                      |.........       |
-------------------------------------------------------------------
[Server] packet : (0x51) F_ANIMATION  Size = 9 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 06 51 01 3B 00 AC 00 00                      |.........       |
-------------------------------------------------------------------
[Client] packet : (0x01) UNKNOWN  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 63 76 00 DD 00 00 00 01 00 5F BB B0 00 00 |..cv......._....|
|00 00 00 00 00 00 00 00 06 05 00 00 13 00 DD 16 |................|                                                
-------------------------------------------------------------------
[Server] packet : (0x00) UNKNOWN  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 00 00 5F BB B0 AA E6 1E F2 00 00 00 00 00 |...._...........|
|00 06 05 00 00 13 00                            |.......         |
-------------------------------------------------------------------
[Client] packet : (0x5A) F_PING_DATAGRAM  Size = 13 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 01 63 7B 00 DD FF FF 00 5A 00 79 16          |.............   |
-------------------------------------------------------------------
[Client] packet : (0x16) F_REQUEST_INIT_OBJECT  Size = 16 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 04 63 7F 00 DD 00 00 00 16 11 6A 00 00 EA 40 |..c........j...@|                                                
-------------------------------------------------------------------
[Server] packet : (0x72) F_CREATE_MONSTER  Size = 108 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 69 72 11 6A 00 EB 00 BA 29 D2 00 0F E4 F4 00 |.ir.j....)......|
|10 40 3F 00 00 04 3C 32 07 61 00 00 00 00 00 00 |.@?...<2.a......|
|00 00 00 03 E8 00 00 00 00 89 66 00 08 00 00 01 |..........f.....|
|19 00 47 6F 6C 64 65 6E 77 69 6E 67 20 46 6C 69 |..Goldenwing Fli|
|65 72 5E 6D 00 01 0A 00 00 00 1A 05 00 11 6A 64 |er^m..........jd|
|F4 C0 3F 29 D2 64 21 C8 00 00 00 00 00 EB 00 00 |..?).d!.........|
|C9 63 3A C4 D2 29 C8 11 6A 00 00 00             |............    |
-------------------------------------------------------------------
[Server] packet : (0x51) F_ANIMATION  Size = 9 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 06 51 0F E7 00 AC 00 00                      |.........       |
-------------------------------------------------------------------
[Client] packet : (0x01) UNKNOWN  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 63 8D 00 DD 00 00 00 01 00 5F E3 26 20 07 |..c........_.& .|
|2A 68 90 38 40 04 00 23 3D FF 08 18 FB EB 00 00 |*h.8@..#=.......|                                                
-------------------------------------------------------------------
[Server] packet : (0x00) UNKNOWN  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 00 00 5F E3 26 AA E6 46 75 00 00 00 00 00 |...._.&..Fu.....|
|23 3D FF 08 18 FB 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0x51) F_ANIMATION  Size = 9 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 06 51 01 3C 00 E5 00 00                      |.........       |
-------------------------------------------------------------------
[Client] packet : (0x5A) F_PING_DATAGRAM  Size = 13 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 01 63 9B 00 DD FF FF 00 5A 00 50 1E          |.............   |
-------------------------------------------------------------------
[Server] packet : (0x6F) F_LOCALIZED_STRING  Size = 18 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0F 6F 00 00 00 00 05 3D 00 00 00 01 01 00 02 |..o.....=.......|
|31 00                                           |..              |
-------------------------------------------------------------------
[Server] packet : (0xD8) F_GROUP_STATUS  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 D8 3E C8 01 03 00 02 00 00 00 00 01 01 00 |...>............|
|1B 32 1C 00 25 AF 00                            |.......         |
-------------------------------------------------------------------
[Client] packet : (0x16) F_REQUEST_INIT_OBJECT  Size = 16 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 04 63 A1 00 DD 00 00 00 16 37 BB 00 00 AA F0 |..c.......7.....|                                                
-------------------------------------------------------------------
[Client] packet : (0x16) F_REQUEST_INIT_OBJECT  Size = 16 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 04 63 A2 00 DD 00 00 00 16 34 E0 00 00 00 CC |..c.......4.....|                                                
-------------------------------------------------------------------
[Server] packet : (0x72) F_CREATE_MONSTER  Size = 108 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 69 72 37 BB 00 EB 03 95 29 D6 00 0F D7 5E 00 |.ir7.....)....^.|
|10 40 0B 00 03 04 3C 32 07 61 00 00 00 00 00 00 |.@....<2.a......|
|00 00 00 03 E8 00 00 00 00 89 68 00 03 00 00 01 |..........h.....|
|19 00 47 6F 6C 64 65 6E 77 69 6E 67 20 46 6C 69 |..Goldenwing Fli|
|65 72 5E 6D 00 01 0A 00 00 00 1A 05 00 37 BB 57 |er^m.........7.W|
|5E C0 0B 29 D6 64 21 C8 00 00 00 00 00 EB 00 00 |^..).d!.........|
|2B 57 14 C0 D7 29 C8 37 BB 00 00 00             |............    |
-------------------------------------------------------------------
[Server] packet : (0x72) F_CREATE_MONSTER  Size = 108 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 69 72 34 E0 00 EB 00 A4 24 1F 00 0F D9 42 00 |.ir4.....$....B.|
|10 40 39 FF FC 04 3C 32 07 61 00 00 00 00 00 00 |.@9...<2.a......|
|00 00 00 03 E8 00 00 00 00 89 74 00 05 00 00 01 |..........t.....|
|19 00 47 6F 6C 64 65 6E 77 69 6E 67 20 45 61 67 |..Goldenwing Eag|
|6C 65 5E 6D 00 01 0A 00 00 00 1A 05 00 34 E0 59 |le^m.........4.Y|
|42 C0 39 24 1F 64 21 C8 00 00 00 00 00 EB 00 81 |B.9$.d!.........|
|14 59 ED C0 1C 24 C8 34 E0 00 00 00             |............    |
-------------------------------------------------------------------
[Client] packet : (0x01) UNKNOWN  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 63 A4 00 DD 00 00 00 01 00 60 0A 8F 00 23 |..c........`...#|
|39 B0 08 18 C0 4A 20 07 2B F8 90 38 49 FD 00 00 |9....J .+..8I...|                                                
-------------------------------------------------------------------
[Server] packet : (0x00) UNKNOWN  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 00 00 60 0A 8F AA E6 6D C0 00 00 00 00 20 |....`....m..... |
|07 2B F8 90 38 49 00                            |.......         |
-------------------------------------------------------------------
[Client] packet : (0x16) F_REQUEST_INIT_OBJECT  Size = 16 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 04 63 B0 00 DD 00 00 00 16 10 C2 00 00 B5 AD |..c.............|                                                
-------------------------------------------------------------------
[Server] packet : (0x72) F_CREATE_MONSTER  Size = 108 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 69 72 10 C2 00 EB 0B 93 29 FA 00 0F D0 50 00 |.ir......)....P.|
|10 40 40 00 00 04 3C 32 07 61 00 00 00 00 00 00 |.@@...<2.a......|
|00 00 00 03 E8 00 00 00 00 89 66 00 04 00 00 01 |..........f.....|
|19 00 47 6F 6C 64 65 6E 77 69 6E 67 20 46 6C 69 |..Goldenwing Fli|
|65 72 5E 6D 00 01 0A 00 00 00 1A 05 00 10 C2 50 |er^m...........P|
|50 C0 40 29 FA 64 21 C8 00 00 00 00 00 EB 00 00 |P.@).d!.........|
|AD 59 AE BE FA 29 C8 10 C2 00 00 00             |............    |
-------------------------------------------------------------------
[Server] packet : (0x51) F_ANIMATION  Size = 9 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 06 51 01 3C 00 B0 00 00                      |.........       |
-------------------------------------------------------------------
[Server] packet : (0x6F) F_LOCALIZED_STRING  Size = 18 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0F 6F 00 00 00 00 05 3D 00 00 00 01 01 00 02 |..o.....=.......|
|30 00                                           |..              |
-------------------------------------------------------------------
[Server] packet : (0xD8) F_GROUP_STATUS  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 D8 3E C8 01 03 00 02 00 00 00 00 00 01 00 |...>............|
|1B 32 1C 00 25 AF 00                            |.......         |
-------------------------------------------------------------------
[Client] packet : (0x01) UNKNOWN  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 63 BA 00 DD 00 00 00 01 00 60 32 18 00 00 |..c........`2...|
|00 00 00 00 00 00 00 00 40 02 00 00 18 11 A4 97 |........@.......|                                                
-------------------------------------------------------------------
[Server] packet : (0x00) UNKNOWN  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 00 00 60 32 18 AA E6 95 3B 00 00 00 00 00 |....`2....;.....|
|00 40 02 00 00 18 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0xA9) F_UPDATE_HOT_SPOT  Size = 406 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|01 93 A9 50 02 00 01 00 00 00 00 02 00 00 00 00 |...P............|
|03 00 00 00 00 04 00 00 00 00 05 00 00 00 00 06 |................|
|00 00 00 00 07 00 00 00 00 08 00 00 00 00 09 00 |................|
|00 00 00 0A 00 00 00 00 0B 00 00 00 00 1A 00 00 |................|
|00 00 1B 00 00 00 00 44 00 00 00 00 46 00 00 00 |.......D....F...|
|00 47 00 00 00 00 4A 00 00 00 00 4B 00 00 00 00 |.G....J....K....|
|58 00 00 00 00 64 00 00 00 00 65 00 00 00 00 66 |X....d....e....f|
|00 00 00 00 67 00 00 00 00 68 00 00 00 00 69 00 |....g....h....i.|
|00 00 00 6A 01 00 00 00 6B 00 00 00 00 6C 00 00 |...j....k....l..|
|00 00 6D 00 00 00 00 6E 00 00 00 00 75 00 00 00 |..m....n....u...|
|00 76 00 00 00 00 78 00 00 00 00 81 00 00 00 00 |.v....x.........|
|90 00 00 00 00 91 00 00 00 00 A1 00 00 00 00 A2 |................|
|00 00 00 00 AF 00 00 00 00 B2 00 00 00 00 BE 00 |................|
|00 00 00 BF 00 00 00 00 C0 00 00 00 00 C1 00 00 |................|
|00 00 C6 00 00 00 00 C8 00 00 00 00 C9 00 00 00 |................|
|00 CA 00 00 00 00 CB 01 02 05 00 CC 00 00 00 00 |................|
|CD 00 00 00 00 CE 00 00 00 00 CF 00 00 00 00 D0 |................|
|00 00 00 00 D1 00 00 00 00 D2 00 00 00 00 D3 00 |................|
|00 00 00 D5 00 00 00 00 D7 00 00 00 00 D8 00 00 |................|
|00 00 D9 00 00 00 00 DC 00 00 00 00 DD 00 00 00 |................|
|00 DE 00 00 00 00 E3 00 00 00 00 E4 00 00 00 00 |................|
|FA 00 00 00 00 FB 00 00 00 00 FC 00 00 00 00 FD |................|
|00 00 00 01 15 00 00 00 01 16 00 00 00 01 18 00 |................|
|00 00 01 1B 00 00 00 01 1C 00 00 00 01 1D 00 00 |................|
|00 01 1F 00 00 00 01 22 00 00 00 01 23 00 00 00 |......."....#...|
|01 24 00 00 00 00                               |......          |
-------------------------------------------------------------------
[Client] packet : (0x5A) F_PING_DATAGRAM  Size = 13 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 01 63 BE 00 DD FF FF 00 5A 00 30 1E          |.............   |
-------------------------------------------------------------------