﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
    public class Folder
    {
        public Dictionary<String, Folder> Folders = new Dictionary<String, Folder>();

        public Dictionary<Int64, PatcherAsset> Files = new Dictionary<Int64, PatcherAsset>();
        
        public MYPManager Manager { get; internal set; }

        public PackageType Package { get; internal set; }

        public Int64 PatcherFileID = 0;

        public String Name => Path == null ? Package.ToString().ToLower() : System.IO.Path.GetFileName(Path);

        public String Path { get; internal set; }
        
        public Folder Parent { get; internal set; }

        public Folder Root => RecalculateRoot();

        private Folder RecalculateRoot()
        {
            Folder r = Parent;
            while(r != null)
            {
                if(r.Parent == null)
                { return r; }

                r = r.Parent;
            }
            return this;
        }

        public override String ToString()
        {
            return Name;
        }

        public Folder(MYPManager manager) => Manager = manager;

        public List<PatcherAsset> GetAssets(PackageType packageFilter = PackageType.None)
        {
            var list = new List<PatcherAsset>();
            GetAssetsRecursive(list, packageFilter);
            return list;
        }

        public List<PackageType> GetPackages()
        {
            var list = new List<PatcherAsset>();
            GetAssetsRecursive(list);
            return list.GroupBy(e => e.Package).Select(e => e.Key).ToList();
        }

        private void GetAssetsRecursive(List<PatcherAsset> assets, PackageType packageFilter = PackageType.None)
        {
            assets.AddRange(Files.Values.Where(e => packageFilter == PackageType.None || e.Package == packageFilter));
            foreach(Folder folder in Folders.Values)
                folder.GetAssetsRecursive(assets);
        }
    }
}