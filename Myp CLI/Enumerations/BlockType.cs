﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
    public enum BlockType : Int32
    {
        Unknown = 0,
        Header = 1,
        FileTable = 2,
        Asset = 3,
        Free = 4
    }
}