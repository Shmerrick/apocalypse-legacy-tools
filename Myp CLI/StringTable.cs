﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarHammerOnline.DataMining
{
  public class StringTable
  {
    public List<String> List = new List<String>();
    public Dictionary<String, String> _keyed = new Dictionary<String, String>();

    public StringTable()
    {
    }

    public void Load(Byte[] data)
    {
            String str = System.Text.UnicodeEncoding.Unicode.GetString(data);
            Int32 row = 0;
      foreach(String line in str.Split(new String[] { "\r\n" }, StringSplitOptions.None))
      {
                Int32 index = line.IndexOf('\t');
                String key = line;

        if(index > 0)
        {
          key = line.Substring(0, index).ToLower().Trim();
                    String value = line.Substring(index + 1);
          _keyed[key] = value;
          List.Add(value);
        } else
        {
          _keyed[key] = "";
          List.Add("");
        }
        row++;
      }
    }

    public String GetKeyedValue(String row)
    {
      if(_keyed.ContainsKey(row))
        return _keyed[row];
      return "";
    }

    public String GetValue(Int32 row)
    {
      if(row < List.Count)
        return List[row];
      return "";
    }

    public void SetValue(Int32 row, String value)
    {
      if(row > List.Count)
      {
                Int32 count = row - List.Count + 1;

        for(Int32 i = 0; i < count; i++)
          List.Add("");
      }

      _keyed[row.ToString()] = value;
      List[row] = value;
    }

    public String ToDocument()
    {
      var builder = new StringBuilder();

      for(Int32 i = 0; i < List.Count; i++)
      {
        builder.Append(i + "\t" + List[i] + "\r\n");
      }

      return builder.ToString();
    }
  }
}