﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarHammerOnline.DataMining
{
  public partial class CSVGrid : UserControl
  {
    private CSV _csv;
    private Int32 _skip = 1;
    private Dictionary<Int32, ListViewItem> _lviCache = new Dictionary<Int32, ListViewItem>();
    private Boolean _disableUpdates;
    private Int32 _currentRow = 0;
    private Int32 _currentCol = 0;
    private Boolean _changed = false;
    PatcherAsset _asset;

    public delegate void AssetDelegate(PatcherAsset asset, Byte[] data);
    public event AssetDelegate OnSave;

    public Boolean Changed
    {
      get {
        LoadCSV(txtRaw.Text);
        return _csvOrig != _csv.ToText();
      }
    }

    public CSVGrid()
    {
      InitializeComponent();

      listView1.Controls.Add(txt);
      listView1.FullRowSelect = true;
      txt.Leave += txt_Leave;
    }

    private void txt_Leave(Object sender, EventArgs e)
    {

      txt.Visible = false;
      _csv.RowIndex = _currentRow;
      _csv.WriteCol(_currentCol, txt.Text);
      _disableUpdates = true;

      txtRaw.Text = _csv.ToText();
      CheckChanged();
      if(_lviCache.ContainsKey(_currentRow))
      {
        _lviCache[_currentRow].SubItems[_currentCol].Text = txt.Text;
      }


      _disableUpdates = false;
    }

    private void CheckChanged()
    {
      if(_csv != null)
      {
        _changed = _csvOrig != _csv.ToText();
      }
    }

    private readonly TextBox txt = new TextBox { BorderStyle = BorderStyle.FixedSingle, Visible = false };
    private String _csvOrig = "";

    public delegate void ColumnInfoDelegate(CSVGrid grid, Int32 colIndex, out Int32 width);
    public event ColumnInfoDelegate OnGetColumnWidth;

    public void LoadCSV(Byte[] data)
    {
      listView1.VirtualMode = true;

      _csv = new CSV(System.Text.ASCIIEncoding.ASCII.GetString(data));
      _csvOrig = _csv.ToText();
      LoadCSV(_csv);
    }

    public CSV CSV
    {
      get {
        txtRaw_Leave(null, null);
        return _csv;
      }
    }
    public void LoadCSV(String text)
    {
      _csv = new CSV(text);
      LoadCSV(_csv);
    }
    public List<Int32> GetColWidths()
    {
      var list = new List<Int32>();
      for(Int32 i = 0; i < listView1.Columns.Count; i++)
      {
        list.Add(listView1.Columns[i].Width);
      }
      return list;
    }

    public void LoadCSV(CSV csv)
    {

      listView1.Items.Clear();
      listView1.Columns.Clear();
      _lviCache = new Dictionary<Int32, ListViewItem>();

      _csv = csv;
      _disableUpdates = true;
      if(csv.Lines.Count == 0)
      {
        listView1.VirtualListSize = 0;
        return;
      }

      Dictionary<String, Int32> counts = new Dictionary<String, Int32>();
            Int32 index = 0;
      foreach(String cCol in _csv.Row)
      {
                String csvCol = cCol;
        ColumnHeader col = new ColumnHeader();


        col.Text = csvCol;
        if(!counts.ContainsKey(csvCol))
        {
          counts[csvCol] = 1;
        } else
        {
          counts[csvCol]++;
          csvCol = csvCol + counts[csvCol];
        }

                Int32 width = col.Width;
        if(OnGetColumnWidth != null)
          OnGetColumnWidth(this, index, out width);
        index++;
        if(width != 0)
          col.Width = width;

        listView1.Columns.Add(col);

      }
      _csv.NextRow();
      listView1.VirtualMode = true;
      try
      {
        listView1.VirtualListSize = _csv.Lines.Count;

      } catch
      {
      }
      txtRaw.Text = _csv.ToText();
      _disableUpdates = false;
      _changed = false;
    }

    private void listView1_RetrieveVirtualItem(Object sender, RetrieveVirtualItemEventArgs e)
    {
      if(_disableUpdates)
        return;

      if(_csv != null)
      {
        if(!_lviCache.ContainsKey(e.ItemIndex + _skip))
        {
          listView1_CacheVirtualItems(null, new CacheVirtualItemsEventArgs(e.ItemIndex, e.ItemIndex));
        }
        e.Item = _lviCache[e.ItemIndex];
      }
    }

    private void listView1_CacheVirtualItems(Object sender, CacheVirtualItemsEventArgs e)
    {
      if(_csv != null)
      {
        for(Int32 c = e.StartIndex; c <= e.EndIndex; c++)
        {

          _csv.RowIndex = c;
          if(!_lviCache.ContainsKey(_csv.RowIndex))
          {
                        List<String> rowData = _csv.Row;


            var row = new ListViewItem();
            row.Tag = c;

            if(rowData.Count > 0)
              row.Text = rowData[0];

            for(Int32 i = 1; i < listView1.Columns.Count; i++)
            {
              if(i < rowData.Count)
                row.SubItems.Add(rowData[i]);
              else
                row.SubItems.Add("");
            }
            if(c % 2 == 0)
            {
              row.BackColor = Color.FromArgb(255, 240, 240, 240);
            }
            _lviCache[c] = row;
          }
        }
      }
    }

    private void listView1_MouseDoubleClick(Object sender, MouseEventArgs e)
    {
      ListViewHitTestInfo hit = listView1.HitTest(e.Location);

      Rectangle rowBounds = hit.SubItem.Bounds;
      Rectangle labelBounds = hit.Item.GetBounds(ItemBoundsPortion.Label);

      _currentRow = (Int32)hit.Item.Tag;
      _currentCol = hit.Item.SubItems.IndexOf(hit.SubItem);
            Int32 leftMargin = labelBounds.Left - 1;
      txt.Bounds = new Rectangle(rowBounds.Left + leftMargin, rowBounds.Top, rowBounds.Width - leftMargin - 1, rowBounds.Height);
      txt.Text = hit.SubItem.Text;
      txt.SelectAll();
      txt.Visible = true;
      txt.Focus();
    }

    private void txtRaw_TextChanged(Object sender, EventArgs e)
    {

    }
    public delegate void ChangeDelegate();
    public event ChangeDelegate OnChanged;


    private void deleteToolStripMenuItem_Click(Object sender, EventArgs e)
    {
      _disableUpdates = true;
      List<Int32> toRemove = new List<Int32>();
      foreach(Int32 s in listView1.SelectedIndices)
      {
        toRemove.Add(s);
      }

      _currentRow = 0;
      _csv.Remove(toRemove);
      LoadCSV(_csv.ToText());
      _disableUpdates = false;
    }

    private void txtRaw_Leave(Object sender, EventArgs e)
    {
      _csv = new CSV(txtRaw.Text);
    }

    private void addRowToolStripMenuItem_Click(Object sender, EventArgs e)
    {
      if(listView1.SelectedIndices.Count > 0)
        _currentRow = listView1.Items[listView1.SelectedIndices[0]].Index;
      else
        _currentRow = _csv.Lines.Count - 1;

      _csv.RowIndex = _currentRow;

      _csv.NewRow();
      LoadCSV(_csv);
    }

    private void listView1_SelectedIndexChanged(Object sender, EventArgs e)
    {
      if(listView1.SelectedIndices.Count > 0)
        _currentRow = listView1.Items[listView1.SelectedIndices[0]].Index;
      _csv.RowIndex = _currentRow;
    }

    private void saveToolStripMenuItem_Click(Object sender, EventArgs e)
    {

    }

    private void tabControl1_SelectedIndexChanged(Object sender, EventArgs e)
    {
      if(tabControl1.SelectedIndex == 0)
      {
        LoadCSV(txtRaw.Text);
      }
    }
  }
}
