﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarHammerOnline.DataMining
{
  public partial class frmCSVViewer : Form
  {
    private Myp _myp;
    private String _path;
    public Boolean Changed => csvGrid1.Changed;
    public frmCSVViewer()
    {

      InitializeComponent();


    }
    public void LoadAsset(Myp myp, String path, Byte[] data)
    {
      csvGrid1.LoadCSV(data);
      _myp = myp;
      _path = path;
    }

    private void saveToolStripMenuItem_Click(Object sender, EventArgs e)
    {
      _myp.UpdateAsset(_path, System.Text.ASCIIEncoding.ASCII.GetBytes(csvGrid1.CSV.ToText()), true);
    }
  }
}
