#pragma once

/*
  Copyright 2019 UnexpectedBytes

  Licensed under the EUPL, Version 1.1 only (the "Licence");

  You may not use this work except in compliance with the Licence.
  You may obtain a copy of the Licence at:

  https://joinup.ec.europa.eu/page/eupl-text-11-12

  Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the Licence for the specific language governing permissions and limitations under the Licence.
*/
/*
  You may voluntarily and without renumeration send a gift / donation to the author.
  You may send a donation at:

  https://www.paypal.me/debugged

  Any donations sent by the above link are not a transaction or exchange but a one directional system.
  This includes transfer of digital or physical goods, data or services or any other type of exchange.
*/
/*
  This class extends class enum with these features...
    o Bit masking operators.
    o Modulo operator.

  We do this while remaining strongly typed.

  Usage:

    1) Add or modify the enum class underlying type to have unsigned char, int or long long as the underlying type.
    2) Add this line on the line directly after the enum class closing brace.

      template<> struct EnableAllEnumOperators< EnumClass > { static constexpr bool enable = true; };

        Note: If you want to enable sub sets of operators types ushc as just arithmatic .

      template<> struct EnableEnumArithmaticOperators< EnumClass > { static constexpr bool enable = true; };

        Note: There are also the following sub sets you can enable any number of the sub sets in EnableAll is not set.

      template<> struct EnableEnumBitwiseOperators< EnumClass > { static constexpr bool enable = true; };
      template<> struct EnableEnumBitwiseOperators< EnumClass > { static constexpr bool enable = true; };
      template<> struct EnableEnumBitwiseOperators< EnumClass > { static constexpr bool enable = true; };
      template<> struct EnableEnumArithmeticOperators< EnumClass > { static constexpr bool enable = true; };

  Or

    3) Modify the Default state boolean to be true and remove the individual specializations

  Example:

  enum class Permissions : unsigned
  {
    Read = (1 << 1),
    Write = (1 << 2),
    Execute = (1 << 3),
    RW = Read | Write,
  };
  template<> struct EnableEnumBitMaskOperators<Permissions> { static constexpr bool value = true; }

*/

/* Globally Enabled or Disabled */
template<typename T> struct EnableAllEnumOperators { static constexpr bool value = false; };

/* Enum Bitwise Operators */

/* Globally Enabled or Disabled */
template<typename T> struct EnableEnumBitwiseOperators { static constexpr bool value = false; };

/* operator ^ */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator ^(std::underlying_type_t<E> &lhs, std::underlying_type_t<E> &rhs) {
    return static_cast<E>(lhs ^ rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator ^(E lhs, std::underlying_type_t<E> &rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) ^ rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator ^(std::underlying_type_t<E> lhs, E rhs) {
    return static_cast<E>(lhs ^ static_cast<std::underlying_type_t<E>>(rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator ^(E lhs, E rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) ^ static_cast<std::underlying_type_t<E>>(rhs));
};

/* operator ^= */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator ^=(std::underlying_type_t<E> &lhs, std::underlying_type_t<E> rhs) {
    lhs ^= rhs;
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator ^=(E &lhs, std::underlying_type_t<E> rhs) {
    lhs = static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) ^ rhs);
    return lhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator ^=(std::underlying_type_t<E> &lhs, E rhs) {
    lhs ^= static_cast<std::underlying_type_t<E>>(rhs);
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator ^=(E &lhs, E rhs) {
    lhs = static_cast<E>(lhs ^ static_cast<std::underlying_type_t<E>>(rhs));
    return lhs;
};

/* operator & */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator &(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(lhs & rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator &(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) & rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator &(std::underlying_type_t<E> lhs, E rhs) {
    return static_cast<E>(lhs & static_cast<std::underlying_type_t<E>>(rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator &(E lhs, E rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) & static_cast<std::underlying_type_t<E>>(rhs));
};

/* operator &= */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator &=(std::underlying_type_t<E> &lhs, std::underlying_type_t<E> rhs) {
    lhs &= rhs;
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator &=(E &lhs, std::underlying_type_t<E> rhs) {
    lhs = static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) ^ rhs);
    return lhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator &=(std::underlying_type_t<E> &lhs, E rhs) {
    lhs &= static_cast<std::underlying_type_t<E>>(rhs);
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator &=(E &lhs, E rhs) {
    lhs = static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) ^ static_cast<std::underlying_type_t<E>>(rhs));
    return lhs;
};

/* operator | */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator |(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(lhs | rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator |(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) | rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator |(std::underlying_type_t<E> lhs, E rhs) {
    return static_cast<E>(lhs | static_cast<std::underlying_type_t<E>>(rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator |(E lhs, E rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) | static_cast<std::underlying_type_t<E>>(rhs));
};

/* operator |= */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator |=(std::underlying_type_t<E> &lhs, std::underlying_type_t<E> rhs) {
    lhs |= rhs;
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator |=(E &lhs, std::underlying_type_t<E> rhs) {
    lhs = static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) ^ rhs);
    return lhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator |=(std::underlying_type_t<E> &lhs, E rhs) {
    lhs |= static_cast<std::underlying_type_t<E>>(rhs);
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator |=(E &lhs, E rhs) {
    lhs = static_cast<E>(lhs ^ static_cast<std::underlying_type_t<E>>(rhs));
    return lhs;
};

/* operator ~ */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator ~(std::underlying_type_t<E> rhs) {
    return static_cast<E>(~rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumBitwiseOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator ~(E rhs) {
    return static_cast<E>(~static_cast<std::underlying_type_t<E>>(rhs));
};

/* Enum Stream Operators */

/* Globally Enabled or Disabled */
template<typename T> struct EnableEnumStreamOperators { static constexpr bool value = false; };

/* ostream operator << */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumStreamOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> std::ostream &operator<<(std::ostream& os, const E& rhs) {
    os << static_cast<std::underlying_type_t<E>>(rhs);
    return os;
}

/* istream operator >> */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumStreamOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> std::istream &operator>>(std::istream& is, const E& rhs) {
    is >> static_cast<std::underlying_type_t<E>>(rhs);
    return is;
}

/* Enum Shift Operators */

/* Globally Enabled or Disabled */
template<typename T> struct EnableEnumShiftOperators { static constexpr bool value = false; };

/* operator << */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumShiftOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator <<(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(lhs << rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumShiftOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator <<(std::underlying_type_t<E> lhs, E rhs) {
    return static_cast<E>(lhs << static_cast<std::underlying_type_t<E>>(rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumShiftOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator <<(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) << rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumShiftOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator <<(E lhs, E rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) << static_cast<std::underlying_type_t<E>>(rhs));
};

/* operator >> */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumShiftOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator >>(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(lhs >> rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumShiftOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator >>(std::underlying_type_t<E> lhs, E rhs) {
    return static_cast<E>(lhs >> static_cast<std::underlying_type_t<E>>(rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumShiftOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator >>(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) >> rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumShiftOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator >>(E lhs, E rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) >> static_cast<std::underlying_type_t<E>>(rhs));
};

/* Enum Relational Operators */

/* Globally Enabled or Disabled */
template<typename T> struct EnableEnumRelationalOperators { static constexpr bool value = false; };

/* operator > */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> bool operator >(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return lhs > rhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> bool operator >(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<std::underlying_type_t<E>>(lhs)> rhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> bool operator >(std::underlying_type_t<E> lhs, E rhs) {
    return lhs > static_cast<std::underlying_type_t<E>>(rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> bool operator >(E lhs, E rhs) {
    return static_cast<std::underlying_type_t<E>>(lhs)> static_cast<std::underlying_type_t<E>>(rhs);
};

/* operator < */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator <(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return lhs < rhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator <(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<std::underlying_type_t<E>>(lhs) < rhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator <(std::underlying_type_t<E> lhs, E rhs) {
    return lhs < static_cast<std::underlying_type_t<E>>(rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator <(E lhs, E rhs) {
    return static_cast<std::underlying_type_t<E>>(lhs) < static_cast<std::underlying_type_t<E>>(rhs);
};

/* operator >= */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator >=(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return lhs >= rhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator >=(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<std::underlying_type_t<E>>(lhs) >= rhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator >=(std::underlying_type_t<E> lhs, E rhs) {
    return lhs >= static_cast<std::underlying_type_t<E>>(rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator >=(E lhs, E rhs) {
    return static_cast<std::underlying_type_t<E>>(lhs) >= static_cast<std::underlying_type_t<E>>(rhs);
};

/* operator <= */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator <=(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return lhs <= rhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator <=(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<std::underlying_type_t<E>>(lhs) <= rhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator <=(std::underlying_type_t<E> lhs, E rhs) {
    return lhs <= static_cast<std::underlying_type_t<E>>(rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator <=(E lhs, E rhs) {
    return static_cast<std::underlying_type_t<E>>(lhs) <= static_cast<std::underlying_type_t<E>>(rhs);
};

/* operator == */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator ==(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return lhs == rhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator ==(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<std::underlying_type_t<E>>(lhs) == rhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator ==(std::underlying_type_t<E> lhs, E rhs) {
    return lhs == static_cast<std::underlying_type_t<E>>(rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator ==(E lhs, E rhs) {
    return static_cast<std::underlying_type_t<E>>(lhs) == static_cast<std::underlying_type_t<E>>(rhs);
};

/* operator != */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator !=(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return lhs != rhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator !=(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<std::underlying_type_t<E>>(lhs) != rhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator !=(std::underlying_type_t<E> lhs, E rhs) {
    return lhs != static_cast<std::underlying_type_t<E>>(rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>>  bool operator !=(E lhs, E rhs) {
    return static_cast<std::underlying_type_t<E>>(lhs) != static_cast<std::underlying_type_t<E>>(rhs);
};

/* logical or short-circuit lambda helper */
template<typename L1, typename L2> bool logical_or(L1 &&lhs, L2 &&rhs) { if (lhs()) { return true; } else { return rhs(); } }

/* operator || */
template<class E, typename L1, typename L2, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> bool operator ||(L1 &&lhs, L2 &&rhs) {
    return logical_or(std::forward<L1>(lhs), std::forward<L1>(rhs));
};

/* logical and short-circuit lambda helper */
template<typename L1, typename L2> bool logical_and(L1 &&lhs, L2 &&rhs) { if (lhs()) { return rhs(); } else { return false; } }

/* operator && */
template<class E, typename L1, typename L2, typename _Dummy = std::enable_if_t<(EnableEnumRelationalOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> bool operator &&(L1 &&lhs, L2 &&rhs) {
    return logical_or(std::forward<L1>(lhs), std::forward<L1>(rhs));
};

/* Enum Arithmetic Operators */

/* Globally Enabled or Disabled */
template<typename T> struct EnableEnumArithmeticOperators { static constexpr bool value = false; };

/* operator % */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator %(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs % rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator %(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) % rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator %(std::underlying_type_t<E> lhs, E rhs) {
    return static_cast<E>(lhs % static_cast<std::underlying_type_t<E>>(rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator %(E lhs, E rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) % static_cast<std::underlying_type_t<E>>(rhs));
};

/* operator %= */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator %=(std::underlying_type_t<E> &lhs, std::underlying_type_t<E> rhs) {
    lhs %= rhs;
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator %=(E &lhs, std::underlying_type_t<E> rhs) {
    lhs = static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) % rhs);
    return lhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator %=(std::underlying_type_t<E> &lhs, E rhs) {
    lhs %= static_cast<std::underlying_type_t<E>>(rhs);
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator %=(E &lhs, E rhs) {
    lhs = static_cast<E>(lhs % static_cast<std::underlying_type_t<E>>(rhs));
    return lhs;
};

/* operator + */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator +(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs + rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator +(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) + rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator +(std::underlying_type_t<E> lhs, E rhs) {
    return static_cast<E>(lhs + static_cast<std::underlying_type_t<E>>(rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator +(E lhs, E rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) + static_cast<std::underlying_type_t<E>>(rhs));
};

/* operator += */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator +=(std::underlying_type_t<E> &lhs, std::underlying_type_t<E> rhs) {
    lhs += rhs;
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator +=(E &lhs, std::underlying_type_t<E> rhs) {
    lhs = static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) + rhs);
    return lhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator +=(std::underlying_type_t<E> &lhs, E rhs) {
    lhs += static_cast<std::underlying_type_t<E>>(rhs);
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator +=(E &lhs, E rhs) {
    lhs = static_cast<E>(lhs + static_cast<std::underlying_type_t<E>>(rhs));
    return lhs;
};

/* operator - */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator -(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs - rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator -(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) - rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator -(std::underlying_type_t<E> lhs, E rhs) {
    return static_cast<E>(lhs - static_cast<std::underlying_type_t<E>>(rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator -(E lhs, E rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) - static_cast<std::underlying_type_t<E>>(rhs));
};

/* operator -= */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator -=(std::underlying_type_t<E> &lhs, std::underlying_type_t<E> rhs) {
    lhs -= rhs;
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator -=(E lhs, std::underlying_type_t<E> &rhs) {
    lhs = static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) - rhs);
    return lhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator -=(std::underlying_type_t<E> &lhs, E rhs) {
    lhs -= static_cast<std::underlying_type_t<E>>(rhs);
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator -=(E lhs, E rhs) {
    lhs = static_cast<E>(lhs - static_cast<std::underlying_type_t<E>>(rhs));
    return lhs;
};

/* operator * */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator *(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs * rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator *(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) * rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator *(std::underlying_type_t<E> lhs, E rhs) {
    return static_cast<E>(lhs * static_cast<std::underlying_type_t<E>>(rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator *(E lhs, E rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) * static_cast<std::underlying_type_t<E>>(rhs));
};

/* operator *= */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator *=(std::underlying_type_t<E> &lhs, std::underlying_type_t<E> rhs) {
    lhs *= rhs;
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator *=(E &lhs, std::underlying_type_t<E> rhs) {
    lhs = static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) * rhs);
    return lhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator *=(std::underlying_type_t<E> &lhs, E rhs) {
    lhs *= static_cast<std::underlying_type_t<E>>(rhs);
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator *=(E &lhs, E rhs) {
    lhs = static_cast<E>(lhs * static_cast<std::underlying_type_t<E>>(rhs));
    return lhs;
};

/* operator / */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator /(std::underlying_type_t<E> lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs / rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator /(E lhs, std::underlying_type_t<E> rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) / rhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator /(std::underlying_type_t<E> lhs, E rhs) {
    return static_cast<E>(lhs / static_cast<std::underlying_type_t<E>>(rhs));
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E operator /(E lhs, E rhs) {
    return static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) / static_cast<std::underlying_type_t<E>>(rhs));
};

/* operator /= */
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator /=(std::underlying_type_t<E> &lhs, std::underlying_type_t<E> rhs) {
    lhs /= rhs;
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator /=(E &lhs, std::underlying_type_t<E> rhs) {
    lhs = static_cast<E>(static_cast<std::underlying_type_t<E>>(lhs) / rhs);
    return lhs;
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator /=(std::underlying_type_t<E> &lhs, E rhs) {
    lhs /= static_cast<std::underlying_type_t<E>>(rhs);
    return static_cast<E&>(lhs);
};
template<class E, typename _Dummy = std::enable_if_t<(EnableEnumArithmeticOperators<E>::value || EnableAllEnumOperators<E>::value) && std::is_enum<E>::value>> E &operator /=(E &lhs, E rhs) {
    lhs = static_cast<E>(lhs / static_cast<std::underlying_type_t<E>>(rhs));
    return lhs;
};
