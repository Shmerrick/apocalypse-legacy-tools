﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Launcher
{
    public class JsonFilter : IResponseFilter
    {
        private List<byte> overflow = new List<byte>();

        public string GetOverFlow()
        {
            return UTF8Encoding.UTF8.GetString(overflow.ToArray());
        }

        public FilterStatus Filter(System.IO.Stream dataIn, out long dataInRead, System.IO.Stream dataOut, out long dataOutWritten)
        {
            if (dataIn == null)
            {
                dataInRead = 0;
                dataOutWritten = 0;

                return FilterStatus.Done;
            }

            //We'll read all the data
            dataInRead = dataIn.Length;
            dataOutWritten = Math.Min(dataInRead, dataOut.Length);

            if (dataIn.Length > 0)
            {
                //Copy all the existing data first
                dataIn.CopyTo(dataOut);

                UTF8Encoding.UTF8.GetString(overflow.ToArray());
            }

            // If we have overflow data and remaining space in the buffer then write the overflow.
            if (overflow.Count > 0)
            {
                // Number of bytes remaining in the output buffer.
                long remainingSpace = dataOut.Length - dataOutWritten;
                // Maximum number of bytes we can write into the output buffer.
                long maxWrite = Math.Min(overflow.Count, remainingSpace);

                // Write the maximum portion that fits in the output buffer.
                if (maxWrite > 0)
                {
                    dataOut.Write(overflow.ToArray(), 0, (int)maxWrite);
                    dataOutWritten += maxWrite;
                }

                if (maxWrite == 0 && overflow.Count > 0)
                {
                    //We haven't yet got space to append our data
                    return FilterStatus.NeedMoreData;
                }

                if (maxWrite < overflow.Count)
                {
                    // Need to write more bytes than will fit in the output buffer. 
                    // Remove the bytes that were written already
                    overflow.RemoveRange(0, (int)(maxWrite - 1));
                }
                else
                {
                    overflow.Clear();
                }
            }

            if (overflow.Count > 0)
            {
                return FilterStatus.NeedMoreData;
            }

            return FilterStatus.Done;
        }

        public bool InitFilter()
        {
            return true;
        }

        public void Dispose()
        {

        }
    }
}
