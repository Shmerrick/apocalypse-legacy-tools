﻿using CefSharp;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Launcher
{
    public class JsonHandler : ResourceHandler
    {
        public override bool ProcessRequestAsync(IRequest request, ICallback callback)
        {
            Task.Factory.StartNew(() =>
            {
                using (callback)
                {
                    HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://samples.mplayerhq.hu/SWF/zeldaADPCM5bit.swf");

                    HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                    // Get the stream associated with the response.
                    Stream receiveStream = httpWebResponse.GetResponseStream();
                    string mime = httpWebResponse.ContentType;

                    MemoryStream stream = new MemoryStream();
                    receiveStream.CopyTo(stream);
                    httpWebResponse.Close();

                    //Reset the stream position to 0 so the stream can be copied into the underlying unmanaged buffer
                    stream.Position = 0;

                    //Populate the response values - No longer need to implement GetResponseHeaders (unless you need to perform a redirect)
                    ResponseLength = stream.Length;
                    MimeType = mime;
                    StatusCode = (int)HttpStatusCode.OK;
                    Stream = stream;

                    callback.Continue();
                }
            }).ConfigureAwait(false);

            return true;
        }
    }
}
