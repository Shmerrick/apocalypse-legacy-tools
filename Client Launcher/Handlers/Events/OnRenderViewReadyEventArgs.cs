﻿using CefSharp;

namespace Launcher
{
    public class OnRenderViewReadyEventArgs : RequestEventArgs
    {
        public OnRenderViewReadyEventArgs(IWebBrowser browserControl, IBrowser browser) : base(browserControl, browser)
        {
        }
    }
}
