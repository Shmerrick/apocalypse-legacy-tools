﻿using CefSharp;
using CefSharp.WinForms;
using System;
using System.Diagnostics;
using System.IO;
using System.Media;
using System.Runtime.CompilerServices;
using System.Runtime.ExceptionServices;
using System.Security.Permissions;
using System.Windows.Forms;
using Vlc.DotNet.Forms;

namespace Launcher
{
    internal static class Program
    {
        public static string libcef;
        public static string subprocess;
        public static string locales;
        public static string resources;
        public static string cache;
        public static string useragent;
        public static string docs;
#if DEBUG
        public static string log;
#endif

        public static BrowserProcessHandler browserProcessHandler;

        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain), STAThread]
        public static void Main()
        {
            useragent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3218.0 Safari/537.36";

            libcef = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"libcef.dll");
            subprocess = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"CefSharp.BrowserSubprocess.exe");
            locales = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"cef\locales");
            resources = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"cef\resources");
            cache = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"cef\cache");
            docs = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"assets\bindings.html");

            CefLibraryHandle libraryLoader = new CefLibraryHandle(libcef);
            bool isValid = !libraryLoader.IsInvalid;
            Console.WriteLine($"Library is valid: {isValid}");

            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            CEFBrowserForm form = new CEFBrowserForm(docs);

            //VlcControl vlcControl1 = new Vlc.DotNet.Forms.VlcControl();
            //vlcControl1.BeginInit();
            //vlcControl1.VlcLibDirectory = new DirectoryInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory));
            //vlcControl1.Spu = -1;
            //vlcControl1.EndInit();
            //vlcControl1.VlcMediaplayerOptions = new string[] { "" };
            //vlcControl1.SetMedia(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"assets\music\music.ogg"));
            //vlcControl1.Play();

            Application.Run(form);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void OnApplicationExit(object sender, EventArgs e)
        {
            Cef.Shutdown();
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void InitializeCef()
        {
            browserProcessHandler = new BrowserProcessHandler();
#if DEBUG
            log = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"cef-debug.log");
#endif
            CefSettings cefSettings = new CefSettings
            {
#if DEBUG
                LogSeverity = LogSeverity.Verbose,
                LogFile = log,
                UncaughtExceptionStackSize = 64,
#else
                UncaughtExceptionStackSize = 0,
#endif
                BackgroundColor = Cef.ColorSetARGB(0x00, 0xff, 0x00, 0xff),
                CachePath = cache,
                LocalesDirPath = locales,
                ResourcesDirPath = resources,
                UserAgent = useragent,
                BrowserSubprocessPath = subprocess,
                PersistUserPreferences = true,
                PersistSessionCookies = true,
                WindowlessRenderingEnabled = false,
                MultiThreadedMessageLoop = true
            };
            cefSettings.ExternalMessagePump = !cefSettings.MultiThreadedMessageLoop;
            cefSettings.CefCommandLineArgs.Add("no-proxy-server", "1");
#if DEBUG
            cefSettings.CefCommandLineArgs.Add("enable-logging", "1");
            cefSettings.RemoteDebuggingPort = 80808;
#endif
            cefSettings.CefCommandLineArgs.Add("enable-media-stream", "1");

#if DEBUG
            if (!Cef.Initialize(cefSettings, true, browserProcessHandler))
#else
            if (!Cef.Initialize(cefSettings, false, browserProcessHandler))
#endif
            {
                throw new InvalidOperationException("Cef::Initialize() failed");
            }

            Cef.EnableHighDPISupport();

            if (CefSharpSettings.ShutdownOnExit)
            {
                Application.ApplicationExit += OnApplicationExit;
            }

            CefSharpSettings.SubprocessExitIfParentProcessClosed = true;
            CefSharpSettings.WcfEnabled = false;

            Cef.AddCrossOriginWhitelistEntry("warapoc.com", "https", "youtube.com", true);
            Cef.AddCrossOriginWhitelistEntry("warapoc.com", "https", "google.com", true);
            Cef.AddCrossOriginWhitelistEntry("localhost", "https", "youtube.com", true);
            Cef.AddCrossOriginWhitelistEntry("localhost", "https", "google.com", true);
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Debug.WriteLine(e.Exception);
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Exception exception = (Exception)e.ExceptionObject;
                Debug.WriteLine(exception);
            }
            catch (AggregateException exception)
            {
                ExceptionDispatchInfo.Capture(exception).Throw();
            }
        }
        private static void CurrentDomain_FirstChanceException(object source, FirstChanceExceptionEventArgs e)
        {
            Debug.WriteLine(e);
        }
    }
}
