﻿using System;

namespace Launcher
{
    public class RedirectEventArgs : EventArgs
    {
        public Uri Location { get; protected set; }

        public RedirectEventArgs(string url) : base()
        {
            Location = new Uri(url, UriKind.Absolute);
        }

        public RedirectEventArgs() : base()
        {
            Location = null;
        }
    }
}