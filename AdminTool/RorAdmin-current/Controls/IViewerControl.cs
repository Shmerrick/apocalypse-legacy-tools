﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.IViewerControl
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

namespace RorAdmin.Controls
{
  public interface IViewerControl
  {
    bool Changed { get; }

    void Save();

    void BeforeUnload();
  }
}
