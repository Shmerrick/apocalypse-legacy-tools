﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmLocalizedStringEdit
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
  public class frmLocalizedStringEdit : PersistentForm
  {
    private Dictionary<Language, LocalizatedItem> _languages = new Dictionary<Language, LocalizatedItem>();
    private IContainer components = (IContainer) null;
    private string _filename;
    private MYPManager _manager;
    private int _rowId;
    private MythicPackage _package;
    private GroupBox groupBox1;
    private Panel panel1;
    private Button btnCancel;
    private Button btnOK;
    private TextBox txtID;
    private Panel panel2;

    public frmLocalizedStringEdit()
    {
      this.InitializeComponent();
    }

    private void frmLocalizedStringEdit_Load(object sender, EventArgs e)
    {
    }

    public void LoadIntEntry(
      MythicPackage package,
      MYPManager manager,
      string filename,
      int rowId)
    {
      this.txtID.Text = "RowID: " + (object) rowId + " (" + filename + ")";
      this._package = package;
      this._filename = filename;
      this._manager = manager;
      this._rowId = rowId;
      string[] strArray = filename.Split(new char[1]{ '/' }, StringSplitOptions.RemoveEmptyEntries);
      string str = "";
      for (int index = 3; index < strArray.Length; ++index)
        str = str + "/" + strArray[index];
      Language language1 = (Language) Enum.Parse(typeof (Language), strArray[2]);
      List<Language> list = Enum.GetValues(typeof (Language)).Cast<Language>().ToList<Language>();
      list.Reverse();
      foreach (Language index in list)
      {
        string tableName = "data/strings/" + index.ToString() + str;
        List<string> stringTable = manager.GetStringTable(index, tableName, MythicPackage.DEV);
        if (!this._languages.ContainsKey(index))
        {
          this._languages[index] = new LocalizatedItem();
          this._languages[index].Dock = DockStyle.Top;
          this._languages[index].Filename = tableName;
          this._languages[index].Language = index;
          this.panel2.Controls.Add((Control) this._languages[index]);
        }
        LocalizatedItem language2 = this._languages[index];
        if (rowId < stringTable.Count)
        {
          language2.Value = stringTable[rowId];
          if (language1 == index)
            language2.BackColor = Color.LightBlue;
          else
            language2.BackColor = this.BackColor;
        }
      }
    }

    private void btnOK_Click(object sender, EventArgs e)
    {
      foreach (LocalizatedItem localizatedItem in this._languages.Values)
      {
        if (localizatedItem.Changed)
        {
          this._manager.SetString(localizatedItem.Language, localizatedItem.Filename, this._rowId, localizatedItem.Value, MythicPackage.DEV);
          string s = this._manager.GetString(localizatedItem.Language, localizatedItem.Filename);
          this._manager.UpdateAsset(this._package, localizatedItem.Filename, Encoding.Unicode.GetBytes(s), false);
        }
      }
      this.DialogResult = DialogResult.OK;
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.groupBox1 = new GroupBox();
      this.txtID = new TextBox();
      this.panel1 = new Panel();
      this.btnCancel = new Button();
      this.btnOK = new Button();
      this.panel2 = new Panel();
      this.groupBox1.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      this.groupBox1.Controls.Add((Control) this.txtID);
      this.groupBox1.Dock = DockStyle.Top;
      this.groupBox1.Location = new Point(0, 0);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(844, 45);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Localization File";
      this.txtID.Dock = DockStyle.Top;
      this.txtID.Location = new Point(3, 16);
      this.txtID.Name = "txtID";
      this.txtID.ReadOnly = true;
      this.txtID.Size = new Size(838, 20);
      this.txtID.TabIndex = 3;
      this.txtID.TabStop = false;
      this.panel1.Controls.Add((Control) this.btnCancel);
      this.panel1.Controls.Add((Control) this.btnOK);
      this.panel1.Dock = DockStyle.Bottom;
      this.panel1.Location = new Point(0, 735);
      this.panel1.Name = "panel1";
      this.panel1.Size = new Size(844, 31);
      this.panel1.TabIndex = 1;
      this.btnCancel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.btnCancel.Location = new Point(685, 3);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new Size(75, 23);
      this.btnCancel.TabIndex = 1;
      this.btnCancel.Text = "&Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
      this.btnOK.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.btnOK.Location = new Point(766, 3);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new Size(75, 23);
      this.btnOK.TabIndex = 0;
      this.btnOK.Text = "&Save";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new EventHandler(this.btnOK_Click);
      this.panel2.AutoScroll = true;
      this.panel2.Dock = DockStyle.Fill;
      this.panel2.Location = new Point(0, 45);
      this.panel2.Name = "panel2";
      this.panel2.Size = new Size(844, 690);
      this.panel2.TabIndex = 2;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(844, 766);
      this.Controls.Add((Control) this.panel2);
      this.Controls.Add((Control) this.panel1);
      this.Controls.Add((Control) this.groupBox1);
      this.Name = nameof (frmLocalizedStringEdit);
      this.Text = "Localized Field Edit";
      this.Load += new EventHandler(this.frmLocalizedStringEdit_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);
    }
  }
}
