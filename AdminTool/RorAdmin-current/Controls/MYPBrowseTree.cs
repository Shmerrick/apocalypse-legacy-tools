﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.MYPBrowseTree
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using MYPLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
    public class MYPBrowseTree : UserControl, IPersistControl
    {
        private string _currentOp = "";
        private Dictionary<string, MYPManager> _managers = new Dictionary<string, MYPManager>();
        private Dictionary<MYPManager, TreeNode> _treeTOManager = new Dictionary<MYPManager, TreeNode>();
        private List<TreeNode> _waitingLoad = new List<TreeNode>();
        private IContainer components = (IContainer)null;
        private TreeNode _serverRoot;
        private TreeNode _serverLive;
        private TreeNode _serverTest;
        private TreeNode _serverDev;
        private TreeNode _localRoot;
        private TreeView treeView1;
        private Timer timer1;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem addLocalWarInstanceToolStripMenuItem;
        private ToolStripMenuItem collapseAllToolStripMenuItem;
        private PropertyGrid propertyGrid1;
        private Splitter splitter1;
        private ToolStripMenuItem extractToDiskToolStripMenuItem;
        private ToolStripMenuItem syncWithServerToolStripMenuItem;
        private ToolStripMenuItem devToolStripMenuItem;
        private ToolStripMenuItem testToolStripMenuItem;
        private ToolStripMenuItem liveToolStripMenuItem;
        private ToolStripMenuItem openInNewWindowToolStripMenuItem;
        private ToolStripMenuItem saveChangesToolStripMenuItem;
        private ToolStripMenuItem addFilesToolStripMenuItem;
        private ToolStripMenuItem copyToolStripMenuItem;
        private ToolStripMenuItem pasteToolStripMenuItem;
        private ToolStripMenuItem launchToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem deleteToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripMenuItem compareToToolStripMenuItem;
        private ToolStripMenuItem newToolStripMenuItem;
        private ToolStripMenuItem createDEVMYPToolStripMenuItem1;
        private ToolStripMenuItem abilityBinToolStripMenuItem;
        private ToolStripMenuItem abilityComponentBinToolStripMenuItem;

        public MYPBrowseTree()
        {
            this.InitializeComponent();
            this.DoubleBuffered = true;
            this.InitNodes();
        }

        private void CreateDistroNodes(TreeNode node)
        {
        }

        private void InitNodes()
        {
            this.treeView1.Nodes.Clear();
            this._serverRoot = new TreeNode("Server");
            this._serverLive = new TreeNode("Live");
            this._serverTest = new TreeNode("Test");
            this._serverDev = new TreeNode("Dev");
            this._serverRoot.Nodes.Add(this._serverLive);
            this._serverRoot.Nodes.Add(this._serverTest);
            this._serverRoot.Nodes.Add(this._serverDev);
            this._localRoot = new TreeNode("Local");
            this.treeView1.Nodes.Add(this._serverRoot);
            this.treeView1.Nodes.Add(this._localRoot);
            this.CreateDistroNodes(this._serverLive);
            this.CreateDistroNodes(this._serverTest);
            this.CreateDistroNodes(this._serverDev);
        }

        private void GetNodes(string path, TreeNode parent, Dictionary<string, TreeNode> nodes)
        {
            nodes[path + "." + parent.Text] = parent;
            foreach (TreeNode node in parent.Nodes)
                this.GetNodes(path + "." + parent.Text, node, nodes);
        }

        public Task LoadNodeAsync(TreeNode node, Action ac)
        {
            return Task.Run((Func<Task>)(async () =>
           {
               string currentName = "";
               await AdminUtil.UITask((Action<Task>)(task =>
         {
             currentName = node.Text;
             node.Text += " (Loading...)";
             Application.DoEvents();
         }));
               ac();
               await AdminUtil.UITask((Action<Task>)(task =>
         {
             node.Text = currentName;
             Application.DoEvents();
         }));
           }));
        }

        private async Task LoadLocalMYPNode(MYPManager manager, string mypPath, TreeNode mypsNode)
        {
            MythicPackage package = MythicPackage.NONE;
            Enum.TryParse<MythicPackage>(Path.GetFileNameWithoutExtension(mypPath).ToUpper(), out package);
            if ((uint)package <= 0U)
                return;
            TreeNode mypNode = new TreeNode(package.ToString() + ".MYP (" + (object)manager.Packages[package].Assets.Count + ")");
            mypNode.Tag = (object)manager.Packages[package];
            mypNode.Nodes.Add(new TreeNode("<LOAD>"));
            mypsNode.Nodes.Add(mypNode);
            mypNode = (TreeNode)null;
        }

        private async Task LoadLocalInstall(string installName, string installpath)
        {
            Program.Log.Append(string.Format("Loading local install files {0} ({1})", (object)installName, (object)installpath));
            TreeNode localinstallNode = new TreeNode(installName);
            this.EnumFolder(localinstallNode, installpath);
            MYPManager manager = new MYPManager(Program.Log, installpath, "dehash.txt", Array.Empty<MythicPackage>())
            {
                Name = installName
            };
            this._treeTOManager[manager] = localinstallNode;
            manager.OnMYPUpdated += new MYPManager.MYPChangeDelegate(this.Manager_OnMYPUpdated);
            manager.OnMYPSaved += new MYPManager.MYPChangeDelegate(this.Manager_OnMYPSaved);
            this._managers[installpath] = manager;
            this.AddNodeAsync(this._localRoot, localinstallNode);
            localinstallNode.Tag = (object)manager;
            TreeNode mypsNode = new TreeNode("<MYPS>");
            mypsNode.Tag = (object)manager;
            string[] strArray = Directory.GetFiles(installpath, "*.myp");
            for (int index = 0; index < strArray.Length; ++index)
            {
                string mypPath = strArray[index];
                this.LoadLocalMYPNode(manager, mypPath, mypsNode);
                mypPath = (string)null;
                mypPath = (string)null;
            }
            strArray = (string[])null;
            await AdminUtil.UITask((Action<Task>)(task => localinstallNode.Nodes.Add(mypsNode)));
        }

        public async Task LoadLocalInstalls()
        {
            await AdminUtil.UITask((Action<Task>)(task => this._localRoot.Nodes.Clear()));
            this.LoadNodeAsync(this._localRoot, (Action)(() =>
           {
               foreach (Config.KeyValue keyValue in Program.Config.GetSectionValue("WarInstalls", "Install"))
               {
                   string[] strArray = keyValue.Value.Split('|');
                   if (strArray.Length > 1)
                       this.LoadLocalInstall(strArray[0], strArray[1]);
               }
               AdminUtil.UITask((Action<Task>)(task => this.LoadSettings(Program.UIConfig)));
           })).RunAsync();
        }

        private void Manager_OnMYPSaved(MYPManager manager, MYP myp)
        {
            AdminUtil.UITask((Action<Task>)(task =>
           {
               if (!this._treeTOManager.ContainsKey(manager))
                   return;
               this._treeTOManager[manager].BackColor = Color.White;
               this._treeTOManager[manager].ForeColor = Color.Black;
               this._treeTOManager[manager].Text = manager.Name;
           }));
        }

        private void Manager_OnMYPUpdated(MYPManager manager, MYP myp)
        {
            AdminUtil.UITask((Action<Task>)(task =>
           {
               if (!this._treeTOManager.ContainsKey(manager))
                   return;
               this._treeTOManager[manager].BackColor = Color.Red;
               this._treeTOManager[manager].ForeColor = Color.White;
               this._treeTOManager[manager].Text = manager.Name + " (Unsaved Changes)";
           }));
        }

        public void AddNodeAsync(TreeNode parent, TreeNode node)
        {
            AdminUtil.UITask((Action<Task>)(task => parent.Nodes.Add(node)));
        }

        private void LoadMyp(
          MYPManager manager,
          TreeNode node,
          MYPManager.Folder folder,
          MythicPackage package)
        {
            List<TreeNode> treeNodeList1 = new List<TreeNode>();
            foreach (KeyValuePair<string, MYPManager.Folder> keyValuePair in (IEnumerable<KeyValuePair<string, MYPManager.Folder>>)folder.Folders.OrderBy<KeyValuePair<string, MYPManager.Folder>, string>((Func<KeyValuePair<string, MYPManager.Folder>, string>)(e => e.Value.Name)))
            {
                TreeNode node1 = new TreeNode(keyValuePair.Value.Name);
                node1.Tag = (object)keyValuePair.Value;
                this.LoadMyp(manager, node1, keyValuePair.Value, package);
                if (node1.Nodes.Count > 0)
                    node.Nodes.Add(node1);
            }
            List<TreeNode> treeNodeList2 = new List<TreeNode>();
            List<List<MYP.AssetInfo>> list;
            if (package == MythicPackage.NONE)
            {
                list = folder.Files.Values.ToList<List<MYP.AssetInfo>>();
            }
            else
            {
                if (!folder.FilesByPackage.ContainsKey(package))
                    return;
                list = folder.FilesByPackage[package].Values.ToList<List<MYP.AssetInfo>>();
            }
            foreach (MYP.AssetInfo assetInfo in list.SelectMany<List<MYP.AssetInfo>, MYP.AssetInfo>((Func<List<MYP.AssetInfo>, IEnumerable<MYP.AssetInfo>>)(e => (IEnumerable<MYP.AssetInfo>)e)).OrderBy<MYP.AssetInfo, string>((Func<MYP.AssetInfo, string>)(e => e.Name)).ToList<MYP.AssetInfo>())
            {
                string text = Path.GetFileName(assetInfo.Name);
                if (package == MythicPackage.NONE)
                    text = text + " [" + (object)assetInfo.Package + "]";
                node.Nodes.Add(new TreeNode(text)
                {
                    Tag = (object)new MYPBrowseTree.MYPNode()
                    {
                        Asset = assetInfo,
                        Manager = manager,
                        MYP = manager.Packages[assetInfo.Package]
                    }
                });
            }
        }

        private async Task LoadMYPNodeAsync(TreeNode node)
        {
            string path = node.Tag.ToString();
            string folder = Path.GetDirectoryName(path);
            List<TreeNode> waitingLoad = this._waitingLoad;
            this.LoadNodeAsync(node, (Action)(() =>
           {
               Program.Log.Append(string.Format("Initializing MYP manager '{0}'", (object)folder));
               try
               {
                   waitingLoad.Remove(node);
                   List<TreeNode> list = new List<TreeNode>();
                   foreach (MYPManager.Folder folder1 in this._managers[folder].Root.Values)
                   {
                       if (folder1.Name != null)
                       {
                           TreeNode node1 = new TreeNode(folder1.Name);
                           node1.Tag = (object)folder1;
                           this.LoadMyp(this._managers[folder], node1, folder1, MythicPackage.NONE);
                           if (node1.Nodes.Count > 0)
                               list.Add(node1);
                       }
                   }
                   AdminUtil.UITask((Action<Task>)(task =>
             {
                 this.treeView1.SuspendLayout();
                 node.Nodes.AddRange(list.ToArray());
                 this.treeView1.ResumeLayout();
             }));
               }
               catch (Exception ex)
               {
                   Program.Log.Exception("Error loading myps in folder " + folder, ex);
               }
           })).RunAsync();
        }

        private void EnumFolder(TreeNode node, string path)
        {
            string[] files = Directory.GetFiles(path, "*.myp");
            int index = 0;
            if (index >= files.Length)
                return;
            string str = files[index];
            node.Nodes.Add(new TreeNode("<ASSETS>")
            {
                Tag = (object)str,
                Nodes = {
          "<NO_DEHASH>"
        }
            });
        }

        public void LoadArchives()
        {
            Task.Run((Func<Task>)(async () =>
           {
               await AdminUtil.UITask((Action<Task>)(task => this.timer1.Enabled = true));
               await this.LoadLocalInstalls();
               await AdminUtil.UITask((Action<Task>)(task => this.timer1.Enabled = false));
               this.LoadSettings(Program.UIConfig);
           })).RunAsync();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
        }

        private void MYPBrowseTree_Load(object sender, EventArgs e)
        {
            if (this.FindForm() == null)
                return;
            this.FindForm().FormClosing += new FormClosingEventHandler(this.MYPBrowseTree_FormClosing);
        }

        private void MYPBrowseTree_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (MYPManager mypManager in this._managers.Values)
                mypManager.Close();
        }

        public void SaveSettings(Config config)
        {
            foreach (TreeNode node in this.treeView1.Nodes)
            {
                Dictionary<string, TreeNode> nodes = new Dictionary<string, TreeNode>();
                this.GetNodes(this.Name, node, nodes);
                this.Save(config, nodes);
            }
        }

        private void Save(Config config, Dictionary<string, TreeNode> nodes)
        {
            foreach (string key in nodes.Keys)
            {
                if (nodes[key].IsSelected)
                    config[key + "_IsSelected", ""] = (object)true;
                if (nodes[key].IsExpanded)
                    config[key + "_IsExpanded", ""] = (object)true;
                else
                    config.DeleteKey(key + "_IsExpanded", "");
                if (nodes[key].IsSelected)
                    config[key + "_IsSelected", ""] = (object)true;
                else
                    config.DeleteKey(key + "_IsSelected", "");
            }
        }

        public void LoadSettings(Config config, TreeNode node)
        {
            Dictionary<string, TreeNode> nodes = new Dictionary<string, TreeNode>();
            this.GetNodes(this.Name, node, nodes);
            foreach (string key in nodes.Keys)
            {
                if (config[key + "_IsExpanded", ""] != null && config.GetBool(key + "_IsExpanded"))
                    nodes[key].Expand();
                if (config[key + "_IsSelected", ""] != null && config.GetBool(key + "_IsSelected"))
                    this.treeView1.SelectedNode = nodes[key];
            }
        }

        public void LoadSettings(Config config)
        {
            AdminUtil.UITask((Action<Task>)(task =>
           {
               this.treeView1.SuspendLayout();
               foreach (TreeNode node in this.treeView1.Nodes)
                   this.LoadSettings(config, node);
               this.treeView1.ResumeLayout();
           }));
        }

        private void addLocalWarInstanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLocalInstall frmLocalInstall = new frmLocalInstall();
            if (frmLocalInstall.ShowDialog() != DialogResult.OK)
                return;
            Program.Config["Install", "WarInstalls"] = (object)(frmLocalInstall.InstallName + "|" + frmLocalInstall.Path);
            this.LoadLocalInstalls();
        }

        private void treeView1_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
        {
        }

        private void collapseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.treeView1.CollapseAll();
        }

        private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if (e.Node.Text == "<ASSETS>" && e.Node.Nodes.Count == 1)
                this.LoadMYPNodeAsync(e.Node);
            if (e.Node.Nodes.Count <= 0 || !(e.Node.Nodes[0].Text == "<LOAD>"))
                return;
            if (e.Node.Tag is MYP)
            {
                MYP tag = (MYP)e.Node.Tag;
                foreach (MYPManager.Folder folder in tag.Manager.Root.Values)
                {
                    TreeNode node = new TreeNode(folder.Name);
                    node.Tag = (object)folder;
                    this.LoadMyp(tag.Manager, node, folder, tag.Package);
                    if (node.Nodes.Count > 0)
                        e.Node.Nodes.Add(node);
                }
            }
            e.Node.Nodes[0].Remove();
        }

        public event MYPBrowseTree.MypNodeDelegate OnMypNodeSelected;

        public event MYPBrowseTree.MypNodeDelegate OnMypNodeNewWindow;

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            this.propertyGrid1.SelectedObject = (object)null;
            if (e.Node.Tag is MYPBrowseTree.MYPNode && this.OnMypNodeSelected != null)
            {
                this.propertyGrid1.SelectedObject = (object)((MYPBrowseTree.MYPNode)e.Node.Tag).Asset;
                this.OnMypNodeSelected(((MYPBrowseTree.MYPNode)e.Node.Tag).Manager, ((MYPBrowseTree.MYPNode)e.Node.Tag).Asset);
            }
            else
                this.propertyGrid1.SelectedObject = e.Node.Tag;
        }

        private void openInNewWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!(this.treeView1.SelectedNode.Tag is MYPBrowseTree.MYPNode) || this.OnMypNodeNewWindow == null)
                return;
            this.OnMypNodeNewWindow(((MYPBrowseTree.MYPNode)this.treeView1.SelectedNode.Tag).Manager, ((MYPBrowseTree.MYPNode)this.treeView1.SelectedNode.Tag).Asset);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            this.openInNewWindowToolStripMenuItem.Visible = this.treeView1.SelectedNode != null && this.treeView1.SelectedNode.Tag is MYP.AssetInfo;
            this.saveChangesToolStripMenuItem.Visible = true;
            this.addFilesToolStripMenuItem.Visible = this.treeView1.SelectedNode != null && this.treeView1.SelectedNode.Tag != null && !(this.treeView1.SelectedNode.Tag is MYPBrowseTree.MYPNode);
            this.copyToolStripMenuItem.Visible = this.treeView1.SelectedNode.Tag is MYPBrowseTree.MYPNode || this.treeView1.SelectedNode.Tag is MYPManager.Folder || this.treeView1.SelectedNode.Tag is MYP;
            this.deleteToolStripMenuItem.Visible = this.treeView1.SelectedNode.Tag is MYPBrowseTree.MYPNode || this.treeView1.SelectedNode.Tag is MYPManager.Folder || this.treeView1.SelectedNode.Tag is MYP;
            this.launchToolStripMenuItem.Visible = this.treeView1.SelectedNode.Tag is MYPManager;
            this.pasteToolStripMenuItem.Visible = (this.treeView1.SelectedNode.Tag is MYPManager.Folder || this.treeView1.SelectedNode.Tag is MYP) && Program._assetClipBoard != null;
            this.createDEVMYPToolStripMenuItem1.Visible = this.treeView1.SelectedNode != null && this.treeView1.SelectedNode.Text == "<MYPS>";
            this.compareToToolStripMenuItem.Visible = this.treeView1.SelectedNode.Tag is MYP;
            this.compareToToolStripMenuItem.DropDownItems.Clear();
            if (this.treeView1.SelectedNode.Tag is MYP)
            {
                MYP tag = (MYP)this.treeView1.SelectedNode.Tag;
                foreach (MYP myp in tag.Manager.Packages.Values)
                {
                    if (tag != myp)
                    {
                        ToolStripItem toolStripItem = this.compareToToolStripMenuItem.DropDownItems.Add(myp.Package.ToString());
                        toolStripItem.Tag = (object)myp;
                        toolStripItem.Click += new EventHandler(this.MYPBrowseTree_Click);
                    }
                }
            }
            if (this.treeView1.SelectedNode.Tag is MYPManager.Folder && ((MYPManager.Folder)this.treeView1.SelectedNode.Tag).Path == "data\\bin")
            {
                this.abilityBinToolStripMenuItem.Visible = true;
                this.abilityComponentBinToolStripMenuItem.Visible = true;
            }
            else
            {
                this.abilityBinToolStripMenuItem.Visible = false;
                this.abilityComponentBinToolStripMenuItem.Visible = false;
            }
        }

        private void MYPBrowseTree_Click(object sender, EventArgs e)
        {
            if (!(this.treeView1.SelectedNode.Tag is MYP))
                return;
            MYP tag1 = (MYP)this.treeView1.SelectedNode.Tag;
            frmMYPDiff frmMypDiff = new frmMYPDiff();
            MYP tag2 = (MYP)((ToolStripItem)sender).Tag;
            frmMypDiff.LoadCompare(tag1, tag2);
            frmMypDiff.Text = "Comparing " + tag1.Filename + " to " + tag2.Filename;
            frmMypDiff.Show();
        }

        private void saveChangesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (MYPManager mypManager in this._managers.Values)
                mypManager.Save();
        }

        private void addFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void createDEVMYPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MYPManager tag = (MYPManager)this.treeView1.SelectedNode.Tag;
            MYP myp = tag.ValidateCreateMyp(MythicPackage.DEV);
            this.LoadLocalMYPNode(tag, myp.Filename, this.treeView1.SelectedNode);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!(this.treeView1.SelectedNode.Tag is MYPBrowseTree.MYPNode) && !(this.treeView1.SelectedNode.Tag is MYPManager.Folder) && !(this.treeView1.SelectedNode.Tag is MYP))
                return;
            Program._assetClipBoard = this.treeView1.SelectedNode.Tag;
        }

        private T FindParentType<T>(TreeNode node)
        {
            for (TreeNode parent = node.Parent; parent != null; parent = parent.Parent)
            {
                if (parent != null && parent.Tag is T)
                    return (T)parent.Tag;
            }
            return default(T);
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MYPManager sourceManager = (MYPManager)null;
            MYPManager destManager = (MYPManager)null;
            MYP destMyp = (MYP)null;
            string str = "";
            List<Tuple<MYPManager, MYP.AssetInfo>> assets = new List<Tuple<MYPManager, MYP.AssetInfo>>();
            if (Program._assetClipBoard == null)
                return;
            if (Program._assetClipBoard is MYP)
            {
                MYP assetClipBoard = (MYP)Program._assetClipBoard;
                sourceManager = assetClipBoard.Manager;
                foreach (MYP.AssetInfo assetInfo in assetClipBoard.Assets.Values)
                    assets.Add(new Tuple<MYPManager, MYP.AssetInfo>(sourceManager, assetInfo));
            }
            if (Program._assetClipBoard is MYPManager.Folder)
            {
                MYPManager.Folder assetClipBoard = (MYPManager.Folder)Program._assetClipBoard;
                sourceManager = assetClipBoard.Manager;
                List<MythicPackage> packages = assetClipBoard.GetPackages();
                if (packages.Count == 0)
                    return;
                MythicPackage packageFilter = packages.First<MythicPackage>();
                if (packages.Count > 1)
                {
                    frmMYPSelect frmMypSelect = new frmMYPSelect();
                    frmMypSelect.Packages = packages;
                    if (frmMypSelect.ShowDialog() != DialogResult.OK)
                        return;
                    packageFilter = frmMypSelect.SelectedPackage;
                }
                foreach (MYP.AssetInfo asset in assetClipBoard.GetAssets(packageFilter))
                    assets.Add(new Tuple<MYPManager, MYP.AssetInfo>(assetClipBoard.Manager, asset));
            }
            if (Program._assetClipBoard is MYPBrowseTree.MYPNode)
            {
                MYPBrowseTree.MYPNode assetClipBoard = (MYPBrowseTree.MYPNode)Program._assetClipBoard;
                sourceManager = assetClipBoard.Manager;
                assets.Add(new Tuple<MYPManager, MYP.AssetInfo>(assetClipBoard.Manager, assetClipBoard.Asset));
            }
            if (this.treeView1.SelectedNode.Tag is MYPManager.Folder)
            {
                str = "";
                destManager = ((MYPManager.Folder)this.treeView1.SelectedNode.Tag).Manager;
                destMyp = this.FindParentType<MYP>(this.treeView1.SelectedNode);
            }
            if (this.treeView1.SelectedNode.Tag is MYP)
            {
                str = "";
                destMyp = (MYP)this.treeView1.SelectedNode.Tag;
                destManager = ((MYP)this.treeView1.SelectedNode.Tag).Manager;
            }
            if (assets.Count <= 0)
                return;
            foreach (Tuple<MYPManager, MYP.AssetInfo> tuple in assets)
            {
                if (tuple.Item2.Name != null && str.Length > 0)
                {
                    tuple.Item2.Name = str + "/" + tuple.Item2.Name;
                    tuple.Item2.Hash = MYPManager.Hash(tuple.Item2.Name);
                }
            }
            frmProgress p = new frmProgress();
            Task.Run((Action)(() =>
           {
               destManager.UpdateAssets(destMyp.Package, assets, (MYPManager.AssetUpdated)((manager, a, index, total) =>
         {
             p.UpdateCurrent(string.Format("Copying {0}/{1}.myp/{2} TO {3}/{4}", (object)sourceManager.MYPFolder, (object)a.Package, (object)a.Name, (object)destMyp.Filename, (object)a.Name), index, total);
             Program.Log.Append(string.Format("Copying {0}/{1}.myp/{2} TO {3}/{4}", (object)sourceManager.MYPFolder, (object)a.Package, (object)a.Name, (object)destMyp.Filename, (object)a.Name));
         }));
               p.UpdateCurrent("Packing " + destMyp.Package.ToString() + ".myp...", 0, 0);
               destMyp.Save();
               Program.Log.Append("Finished");
               AdminUtil.UITask((Action<Task>)(task => p.Close()));
           })).RunAsync();
            int num = (int)p.ShowDialog();
            this.LoadLocalInstalls();
        }

        private void launchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!(this.treeView1.SelectedNode.Tag is MYPManager))
                return;
            frmLogin frmLogin = new frmLogin();
            if (frmLogin.ShowDialog() != DialogResult.OK)
                return;
            MYPManager tag = (MYPManager)this.treeView1.SelectedNode.Tag;
            tag.Close();
            Directory.SetCurrentDirectory(tag.MYPFolder);
            Process process = new Process();
            string str = "259CAE186DE11A54B3579DA65BB2AE9A";
            process.StartInfo.FileName = Path.Combine(tag.MYPFolder, "war.exe");
            process.StartInfo.Arguments = " --acctname=" + Convert.ToBase64String(Encoding.ASCII.GetBytes(frmLogin.Username)) + " --sesstoken=" + str + " --loadlog --mapgen --nothreading";
            process.Start();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.treeView1.SelectedNode.Tag is MYP)
            {
                MYP tag = (MYP)this.treeView1.SelectedNode.Tag;
                foreach (MYP.AssetInfo assetInfo in tag.Assets.Values)
                {
                    tag.Delete(assetInfo.Hash);
                    Program.Log.Append(string.Format("Deleting asset {0}/{1}", (object)tag.Filename, (object)(assetInfo.Name ?? assetInfo.Hash.ToString())));
                }
                tag.Save();
                this.LoadLocalInstalls();
            }
            if (!(this.treeView1.SelectedNode.Tag is MYPBrowseTree.MYPNode))
                return;
            MYPBrowseTree.MYPNode tag1 = (MYPBrowseTree.MYPNode)this.treeView1.SelectedNode.Tag;
            tag1.MYP.Delete(tag1.Asset.Hash);
            Program.Log.Append(string.Format("Deleting asset {0}/{1}", (object)tag1.MYP.Filename, (object)(tag1.Asset.Name ?? tag1.Asset.Hash.ToString())));
            this.LoadLocalInstalls();
        }

        private void abilityBinToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void abilityComponentBinToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = (IContainer)new Container();
            this.treeView1 = new TreeView();
            this.contextMenuStrip1 = new ContextMenuStrip(this.components);
            this.addLocalWarInstanceToolStripMenuItem = new ToolStripMenuItem();
            this.collapseAllToolStripMenuItem = new ToolStripMenuItem();
            this.toolStripSeparator3 = new ToolStripSeparator();
            this.extractToDiskToolStripMenuItem = new ToolStripMenuItem();
            this.openInNewWindowToolStripMenuItem = new ToolStripMenuItem();
            this.addFilesToolStripMenuItem = new ToolStripMenuItem();
            this.newToolStripMenuItem = new ToolStripMenuItem();
            this.createDEVMYPToolStripMenuItem1 = new ToolStripMenuItem();
            this.abilityBinToolStripMenuItem = new ToolStripMenuItem();
            this.abilityComponentBinToolStripMenuItem = new ToolStripMenuItem();
            this.toolStripSeparator2 = new ToolStripSeparator();
            this.copyToolStripMenuItem = new ToolStripMenuItem();
            this.pasteToolStripMenuItem = new ToolStripMenuItem();
            this.deleteToolStripMenuItem = new ToolStripMenuItem();
            this.toolStripSeparator1 = new ToolStripSeparator();
            this.syncWithServerToolStripMenuItem = new ToolStripMenuItem();
            this.devToolStripMenuItem = new ToolStripMenuItem();
            this.testToolStripMenuItem = new ToolStripMenuItem();
            this.liveToolStripMenuItem = new ToolStripMenuItem();
            this.launchToolStripMenuItem = new ToolStripMenuItem();
            this.toolStripSeparator4 = new ToolStripSeparator();
            this.saveChangesToolStripMenuItem = new ToolStripMenuItem();
            this.compareToToolStripMenuItem = new ToolStripMenuItem();
            this.timer1 = new Timer(this.components);
            this.propertyGrid1 = new PropertyGrid();
            this.splitter1 = new Splitter();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            this.treeView1.ContextMenuStrip = this.contextMenuStrip1;
            this.treeView1.Dock = DockStyle.Fill;
            this.treeView1.HideSelection = false;
            this.treeView1.Location = new Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new Size(368, 134);
            this.treeView1.TabIndex = 1;
            this.treeView1.BeforeCollapse += new TreeViewCancelEventHandler(this.treeView1_BeforeCollapse);
            this.treeView1.BeforeExpand += new TreeViewCancelEventHandler(this.treeView1_BeforeExpand);
            this.treeView1.AfterSelect += new TreeViewEventHandler(this.treeView1_AfterSelect);
            this.contextMenuStrip1.Items.AddRange(new ToolStripItem[17]
            {
        (ToolStripItem) this.addLocalWarInstanceToolStripMenuItem,
        (ToolStripItem) this.collapseAllToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator3,
        (ToolStripItem) this.extractToDiskToolStripMenuItem,
        (ToolStripItem) this.openInNewWindowToolStripMenuItem,
        (ToolStripItem) this.addFilesToolStripMenuItem,
        (ToolStripItem) this.newToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator2,
        (ToolStripItem) this.copyToolStripMenuItem,
        (ToolStripItem) this.pasteToolStripMenuItem,
        (ToolStripItem) this.deleteToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator1,
        (ToolStripItem) this.syncWithServerToolStripMenuItem,
        (ToolStripItem) this.launchToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator4,
        (ToolStripItem) this.saveChangesToolStripMenuItem,
        (ToolStripItem) this.compareToToolStripMenuItem
            });
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new Size(233, 314);
            this.contextMenuStrip1.Opening += new CancelEventHandler(this.contextMenuStrip1_Opening);
            this.addLocalWarInstanceToolStripMenuItem.Name = "addLocalWarInstanceToolStripMenuItem";
            this.addLocalWarInstanceToolStripMenuItem.Size = new Size(232, 22);
            this.addLocalWarInstanceToolStripMenuItem.Text = "Add Local War Install Instance";
            this.addLocalWarInstanceToolStripMenuItem.Click += new EventHandler(this.addLocalWarInstanceToolStripMenuItem_Click);
            this.collapseAllToolStripMenuItem.Name = "collapseAllToolStripMenuItem";
            this.collapseAllToolStripMenuItem.Size = new Size(232, 22);
            this.collapseAllToolStripMenuItem.Text = "Collapse All";
            this.collapseAllToolStripMenuItem.Click += new EventHandler(this.collapseAllToolStripMenuItem_Click);
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new Size(229, 6);
            this.extractToDiskToolStripMenuItem.Name = "extractToDiskToolStripMenuItem";
            this.extractToDiskToolStripMenuItem.Size = new Size(232, 22);
            this.extractToDiskToolStripMenuItem.Text = "Extract To Disk";
            this.openInNewWindowToolStripMenuItem.Name = "openInNewWindowToolStripMenuItem";
            this.openInNewWindowToolStripMenuItem.Size = new Size(232, 22);
            this.openInNewWindowToolStripMenuItem.Text = "Open In New Window";
            this.openInNewWindowToolStripMenuItem.Visible = false;
            this.openInNewWindowToolStripMenuItem.Click += new EventHandler(this.openInNewWindowToolStripMenuItem_Click);
            this.addFilesToolStripMenuItem.Name = "addFilesToolStripMenuItem";
            this.addFilesToolStripMenuItem.Size = new Size(232, 22);
            this.addFilesToolStripMenuItem.Text = "Add Files";
            this.addFilesToolStripMenuItem.Visible = false;
            this.addFilesToolStripMenuItem.Click += new EventHandler(this.addFilesToolStripMenuItem_Click);
            this.newToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[3]
            {
        (ToolStripItem) this.createDEVMYPToolStripMenuItem1,
        (ToolStripItem) this.abilityBinToolStripMenuItem,
        (ToolStripItem) this.abilityComponentBinToolStripMenuItem
            });
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new Size(232, 22);
            this.newToolStripMenuItem.Text = "New";
            this.createDEVMYPToolStripMenuItem1.Name = "createDEVMYPToolStripMenuItem1";
            this.createDEVMYPToolStripMenuItem1.Size = new Size(189, 22);
            this.createDEVMYPToolStripMenuItem1.Text = "DEV.MYP";
            this.createDEVMYPToolStripMenuItem1.Click += new EventHandler(this.createDEVMYPToolStripMenuItem_Click);
            this.abilityBinToolStripMenuItem.Name = "abilityBinToolStripMenuItem";
            this.abilityBinToolStripMenuItem.Size = new Size(189, 22);
            this.abilityBinToolStripMenuItem.Text = "AbilityBin";
            this.abilityBinToolStripMenuItem.Visible = false;
            this.abilityBinToolStripMenuItem.Click += new EventHandler(this.abilityBinToolStripMenuItem_Click);
            this.abilityComponentBinToolStripMenuItem.Name = "abilityComponentBinToolStripMenuItem";
            this.abilityComponentBinToolStripMenuItem.Size = new Size(189, 22);
            this.abilityComponentBinToolStripMenuItem.Text = "AbilityComponentBin";
            this.abilityComponentBinToolStripMenuItem.Visible = false;
            this.abilityComponentBinToolStripMenuItem.Click += new EventHandler(this.abilityComponentBinToolStripMenuItem_Click);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new Size(229, 6);
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new Size(232, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Visible = false;
            this.copyToolStripMenuItem.Click += new EventHandler(this.copyToolStripMenuItem_Click);
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new Size(232, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Visible = false;
            this.pasteToolStripMenuItem.Click += new EventHandler(this.pasteToolStripMenuItem_Click);
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new Size(232, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Visible = false;
            this.deleteToolStripMenuItem.Click += new EventHandler(this.deleteToolStripMenuItem_Click);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new Size(229, 6);
            this.syncWithServerToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[3]
            {
        (ToolStripItem) this.devToolStripMenuItem,
        (ToolStripItem) this.testToolStripMenuItem,
        (ToolStripItem) this.liveToolStripMenuItem
            });
            this.syncWithServerToolStripMenuItem.Name = "syncWithServerToolStripMenuItem";
            this.syncWithServerToolStripMenuItem.Size = new Size(232, 22);
            this.syncWithServerToolStripMenuItem.Text = "Sync with Server";
            this.devToolStripMenuItem.Name = "devToolStripMenuItem";
            this.devToolStripMenuItem.Size = new Size(96, 22);
            this.devToolStripMenuItem.Text = "Dev";
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new Size(96, 22);
            this.testToolStripMenuItem.Text = "Test";
            this.liveToolStripMenuItem.Name = "liveToolStripMenuItem";
            this.liveToolStripMenuItem.Size = new Size(96, 22);
            this.liveToolStripMenuItem.Text = "Live";
            this.launchToolStripMenuItem.Name = "launchToolStripMenuItem";
            this.launchToolStripMenuItem.Size = new Size(232, 22);
            this.launchToolStripMenuItem.Text = "Launch";
            this.launchToolStripMenuItem.Visible = false;
            this.launchToolStripMenuItem.Click += new EventHandler(this.launchToolStripMenuItem_Click);
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new Size(229, 6);
            this.saveChangesToolStripMenuItem.Name = "saveChangesToolStripMenuItem";
            this.saveChangesToolStripMenuItem.Size = new Size(232, 22);
            this.saveChangesToolStripMenuItem.Text = "Save Changes";
            this.saveChangesToolStripMenuItem.Click += new EventHandler(this.saveChangesToolStripMenuItem_Click);
            this.compareToToolStripMenuItem.Name = "compareToToolStripMenuItem";
            this.compareToToolStripMenuItem.Size = new Size(232, 22);
            this.compareToToolStripMenuItem.Text = "Compare To";
            this.timer1.Tick += new EventHandler(this.timer1_Tick);
            this.propertyGrid1.Dock = DockStyle.Bottom;
            this.propertyGrid1.LineColor = SystemColors.ControlDark;
            this.propertyGrid1.Location = new Point(0, 134);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new Size(368, 221);
            this.propertyGrid1.TabIndex = 2;
            this.splitter1.Dock = DockStyle.Bottom;
            this.splitter1.Location = new Point(0, 355);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new Size(368, 3);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Controls.Add((Control)this.treeView1);
            this.Controls.Add((Control)this.propertyGrid1);
            this.Controls.Add((Control)this.splitter1);
            this.Name = nameof(MYPBrowseTree);
            this.Size = new Size(368, 358);
            this.Load += new EventHandler(this.MYPBrowseTree_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        public class MYPNode
        {
            public MYPManager Manager;
            public MYP MYP;
            public MYP.AssetInfo Asset;
            public object Data1;
            public object Data2;
            public object Data3;
        }

        public delegate void MypNodeDelegate(MYPManager manager, MYP.AssetInfo entry);
    }
}
