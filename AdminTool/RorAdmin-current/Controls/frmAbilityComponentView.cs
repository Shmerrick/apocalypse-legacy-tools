﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.frmAbilityComponentView
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using MYPLib;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
  public class frmAbilityComponentView : BaseViewer, IPersistControl
  {
    private AbilityComponentExport _ab = new AbilityComponentExport();
    private IContainer components = (IContainer) null;
    private RORListView lvKeys;
    private ColumnHeader columnHeader1;
    private ColumnHeader columnHeader2;
    private ColumnHeader columnHeader4;

    public frmAbilityComponentView()
    {
      this.InitializeComponent();
    }

    public void SaveSettings(Config config)
    {
      foreach (ColumnHeader column in this.lvKeys.Columns)
      {
        config[this.lvKeys.Name + "_" + this._entry.Name + "_" + (object) column.Index + "_Width", ""] = (object) column.Width;
        config[this.lvKeys.Name + "_" + this._entry.Name + "_" + (object) column.Index + "_Index", ""] = (object) column.DisplayIndex;
      }
    }

    public void LoadSettings(Config config)
    {
      int defaultValue = 0;
      foreach (ColumnHeader column in this.lvKeys.Columns)
      {
        column.Width = config.GetInt(this.lvKeys.Name + "_" + this._entry.Name + "_" + (object) column.Index + "_Width", 100);
        column.DisplayIndex = config.GetInt(this.lvKeys.Name + "_" + this._entry.Name + "_" + (object) column.Index + "_Index", defaultValue);
        ++defaultValue;
      }
    }

    public override void LoadEntry(MYPManager manager, MYP.AssetInfo entry)
    {
      base.LoadEntry(manager, entry);
      this.lvKeys.Columns.Clear();
      this._ab.Load(manager, (Stream) new MemoryStream(manager.GetAsset(entry)));
      this.lvKeys.LoadList<AbilityComponent>(entry.Name, (ListView) this.lvKeys, this._ab.Components);
      this.LoadSettings(Program.UIConfig);
      this.lvKeys.Invalidate();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.lvKeys = new RORListView();
      this.columnHeader1 = new ColumnHeader();
      this.columnHeader2 = new ColumnHeader();
      this.columnHeader4 = new ColumnHeader();
      this.SuspendLayout();
      this.lvKeys.AllowColumnReorder = true;
      this.lvKeys.Columns.AddRange(new ColumnHeader[3]
      {
        this.columnHeader1,
        this.columnHeader2,
        this.columnHeader4
      });
      this.lvKeys.Dock = DockStyle.Fill;
      this.lvKeys.Font = new Font("Consolas", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.lvKeys.FullRowSelect = true;
      this.lvKeys.Location = new Point(0, 24);
      this.lvKeys.Name = "lvKeys";
      this.lvKeys.Size = new Size(600, 409);
      this.lvKeys.TabIndex = 3;
      this.lvKeys.UseCompatibleStateImageBehavior = false;
      this.lvKeys.View = View.Details;
      this.columnHeader1.Text = "ID";
      this.columnHeader1.Width = 76;
      this.columnHeader2.Text = "Name";
      this.columnHeader2.Width = 100;
      this.columnHeader4.Text = "Description";
      this.columnHeader4.Width = 296;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.lvKeys);
      this.Name = "frmAbilityRequirmentView";
      this.Size = new Size(600, 433);
      this.Controls.SetChildIndex((Control) this.lvKeys, 0);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
