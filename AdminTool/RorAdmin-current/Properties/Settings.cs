﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Properties.Settings
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace RorAdmin.Properties
{
  [CompilerGenerated]
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        Settings defaultInstance = Settings.defaultInstance;
        Settings settings = defaultInstance;
        return settings;
      }
    }
  }
}
