﻿// Decompiled with JetBrains decompiler
// Type: MypLib.MYP
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using MYPLib;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;

namespace MypLib
{
  public class MYP : IDisposable
  {
    private IntPtr _mypPtr = IntPtr.Zero;
    private Dictionary<ulong, Tuple<bool, byte[]>> _dataChanges = new Dictionary<ulong, Tuple<bool, byte[]>>();
    private static ILogger Log;
    private string _filename;
    private bool _changed;
    private static MYP.LogDelegate _logDelegate;

    public Dictionary<ulong, MYP.AssetInfo> Assets { get; private set; } = new Dictionary<ulong, MYP.AssetInfo>();

    public MythicPackage Package { get; private set; }

    public MYPManager Manager { get; set; }

    public bool Changed
    {
      get
      {
        return this._changed;
      }
    }

    [SuppressUnmanagedCodeSecurity]
    [DllImport("MypFast.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern IntPtr LoadMyp(MYP.LogDelegate log, string path);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("MypFast.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern IntPtr CreateMyp(MYP.LogDelegate log, string path);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("MypFast.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern void UnloadMyp(IntPtr ptr);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("MypFast.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern uint GetAssetSize(IntPtr ptr, string assetPath);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("MypFast.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern uint GetAssetSizeByHash(IntPtr ptr, ulong hash);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("MypFast.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern int GetAssetCount(IntPtr ptr);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("MypFast.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern int GetAssetData(IntPtr ptr, string assetPath, [In, Out] byte[] data);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("MypFast.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern int GetAssetDataByHash(IntPtr ptr, ulong hash, [In, Out] byte[] data);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("MypFast.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern int UpdateAssetByHash(
      IntPtr ptr,
      ulong hash,
      [In, Out] byte[] data,
      uint size,
      bool compressed);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("MypFast.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern bool Save(IntPtr ptr);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("MypFast.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern bool DeleteAssetByHash(IntPtr ptr, ulong hash);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("MypFast.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    private static extern int GetAssetsInfo(
      IntPtr ptr,
      [In, Out] ulong[] hashes,
      [In, Out] uint[] CompressedSize,
      [In, Out] uint[] UnCompressedSize,
      [In, Out] uint[] compressed,
      [In, Out] uint[] crc);

    public static void MypLog(int level, string msg)
    {
      Console.WriteLine(msg);
      if (level == 1)
        return;
      MYP.Log.Error(msg);
    }

    public void Dispose()
    {
      if (!(this._mypPtr != IntPtr.Zero))
        return;
      MYP.UnloadMyp(this._mypPtr);
      this._mypPtr = IntPtr.Zero;
    }

    public string Filename
    {
      get
      {
        return this._filename;
      }
    }

    public MYP(ILogger log, MythicPackage package, string filename, bool create = false)
    {
      this._filename = filename;
      MYP.Log = log;
      if (MYP._logDelegate == null)
        MYP._logDelegate = new MYP.LogDelegate(MYP.MypLog);
      if (create)
        MYP.CreateMyp(MYP._logDelegate, filename);
      this._mypPtr = MYP.LoadMyp(MYP._logDelegate, filename);
      if (this._mypPtr == IntPtr.Zero)
        throw new Exception(string.Format("Error loading '{0}'", (object) filename));
      this.Package = package;
      int assetCount = MYP.GetAssetCount(this._mypPtr);
      ulong[] hashes = new ulong[assetCount];
      uint[] CompressedSize = new uint[assetCount];
      uint[] UnCompressedSize = new uint[assetCount];
      uint[] crc = new uint[assetCount];
      uint[] compressed = new uint[assetCount];
      MYP.GetAssetsInfo(this._mypPtr, hashes, CompressedSize, UnCompressedSize, compressed, crc);
      for (int index = 0; index < assetCount; ++index)
      {
        MYP.AssetInfo assetInfo = new MYP.AssetInfo()
        {
          Package = package,
          Hash = hashes[index],
          CompressedSize = CompressedSize[index],
          UncompressedSize = UnCompressedSize[index],
          Compressed = compressed[index] > 0U,
          CRC = crc[index]
        };
        this.Assets.Add(assetInfo.Hash, assetInfo);
      }
    }

    public void UpdateAsset(string name, byte[] data, bool compress, bool memCached = true)
    {
      this.UpdateAsset(MYP.HashWAR(name), data, compress, memCached);
    }

    public void UpdateAsset(ulong hash, byte[] data, bool compress, bool memCached = true)
    {
      this._changed = true;
      if (memCached)
        this._dataChanges[hash] = new Tuple<bool, byte[]>(compress, data);
      MYP.UpdateAssetByHash(this._mypPtr, hash, data, (uint) data.Length, compress);
    }

    public bool Save()
    {
      this._dataChanges.Clear();
      if (!MYP.Save(this._mypPtr))
        return false;
      this._changed = false;
      return true;
    }

    public bool Delete(string name)
    {
      return this.Delete(MYP.HashWAR(name));
    }

    public bool Delete(ulong hash)
    {
      if (!MYP.DeleteAssetByHash(this._mypPtr, hash))
        return false;
      this._changed = true;
      return true;
    }

    public byte[] GetAssetData(string name)
    {
      return this.GetAssetData(MYP.HashWAR(name));
    }

    public byte[] GetAssetData(ulong hash)
    {
      if (this._dataChanges.ContainsKey(hash))
        return this._dataChanges[hash].Item2;
      uint assetSizeByHash = MYP.GetAssetSizeByHash(this._mypPtr, hash);
      if (assetSizeByHash <= 0U)
        return (byte[]) null;
      byte[] data = new byte[(int) assetSizeByHash];
      MYP.GetAssetDataByHash(this._mypPtr, hash, data);
      return data;
    }

    public static ulong HashWAR(string s)
    {
      uint ph = 0;
      uint sh = 0;
      MYP.HashWAR(s, 3735928559U, out ph, out sh);
      return ((ulong) ph << 32) + (ulong) sh;
    }

    public static void HashWAR(string s, uint seed, out uint ph, out uint sh)
    {
      uint num1 = 0;
      uint num2 = 0;
      int num3;
      uint num4 = (uint) (num3 = 0);
      num2 = (uint) num3;
      num1 = (uint) num3;
      uint num5 = (uint) num3;
      int num6;
      uint num7 = (uint) (num6 = s.Length + (int) seed);
      uint num8 = (uint) num6;
      uint num9 = (uint) num6;
      int index;
      for (index = 0; index + 12 < s.Length; index += 12)
      {
        uint num10 = ((uint) ((int) s[index + 7] << 24 | (int) s[index + 6] << 16 | (int) s[index + 5] << 8) | (uint) s[index + 4]) + num8;
        uint num11 = ((uint) ((int) s[index + 11] << 24 | (int) s[index + 10] << 16 | (int) s[index + 9] << 8) | (uint) s[index + 8]) + num7;
        uint num12 = (uint) (((int) s[index + 3] << 24 | (int) s[index + 2] << 16 | (int) s[index + 1] << 8 | (int) s[index]) - (int) num11 + (int) num9 ^ (int) (num11 >> 28) ^ (int) num11 << 4);
        uint num13 = num11 + num10;
        uint num14 = (uint) ((int) num10 - (int) num12 ^ (int) (num12 >> 26) ^ (int) num12 << 6);
        uint num15 = num12 + num13;
        uint num16 = (uint) ((int) num13 - (int) num14 ^ (int) (num14 >> 24) ^ (int) num14 << 8);
        uint num17 = num14 + num15;
        uint num18 = (uint) ((int) num15 - (int) num16 ^ (int) (num16 >> 16) ^ (int) num16 << 16);
        uint num19 = num16 + num17;
        uint num20 = (uint) ((int) num17 - (int) num18 ^ (int) (num18 >> 13) ^ (int) num18 << 19);
        num9 = num18 + num19;
        num7 = (uint) ((int) num19 - (int) num20 ^ (int) (num20 >> 28) ^ (int) num20 << 4);
        num8 = num20 + num9;
      }
      if (s.Length - index > 0)
      {
        switch (s.Length - index)
        {
          case 1:
            num9 += (uint) s[index];
            break;
          case 2:
            num9 += (uint) s[index + 1] << 8;
            goto case 1;
          case 3:
            num9 += (uint) s[index + 2] << 16;
            goto case 2;
          case 4:
            num9 += (uint) s[index + 3] << 24;
            goto case 3;
          case 5:
            num8 += (uint) s[index + 4];
            goto case 4;
          case 6:
            num8 += (uint) s[index + 5] << 8;
            goto case 5;
          case 7:
            num8 += (uint) s[index + 6] << 16;
            goto case 6;
          case 8:
            num8 += (uint) s[index + 7] << 24;
            goto case 7;
          case 9:
            num7 += (uint) s[index + 8];
            goto case 8;
          case 10:
            num7 += (uint) s[index + 9] << 8;
            goto case 9;
          case 11:
            num7 += (uint) s[index + 10] << 16;
            goto case 10;
          case 12:
            num7 += (uint) s[index + 11] << 24;
            goto case 11;
        }
        uint num10 = (uint) (((int) num7 ^ (int) num8) - ((int) (num8 >> 18) ^ (int) num8 << 14));
        uint num11 = (uint) (((int) num10 ^ (int) num9) - ((int) (num10 >> 21) ^ (int) num10 << 11));
        uint num12 = (uint) (((int) num8 ^ (int) num11) - ((int) (num11 >> 7) ^ (int) num11 << 25));
        uint num13 = (uint) (((int) num10 ^ (int) num12) - ((int) (num12 >> 16) ^ (int) num12 << 16));
        uint num14 = (uint) (((int) num13 ^ (int) num11) - ((int) (num13 >> 28) ^ (int) num13 << 4));
        uint num15 = (uint) (((int) num12 ^ (int) num14) - ((int) (num14 >> 18) ^ (int) num14 << 14));
        uint num16 = (uint) (((int) num13 ^ (int) num15) - ((int) (num15 >> 8) ^ (int) num15 << 24));
        ph = num15;
        sh = num16;
      }
      else
      {
        ph = num7;
        sh = num5;
      }
    }

    public class AssetInfo
    {
      public MythicPackage Package { get; internal set; }

      public ulong Hash { get; set; }

      public uint CompressedSize { get; internal set; }

      public uint UncompressedSize { get; internal set; }

      public bool Compressed { get; set; }

      public string Name { get; set; }

      public uint CRC { get; set; }
    }

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    private delegate void LogDelegate(int level, string msg);
  }
}
