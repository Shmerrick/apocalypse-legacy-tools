﻿// Decompiled with JetBrains decompiler
// Type: MypLib.ZoneNif
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System.Collections.Generic;
using System.Reflection;

namespace MypLib
{
  public class ZoneNif
  {
    public string TextualName = "";
    public string FileName = "";
    public int ID;
    public int AmbientOnly;
    public int Shadow;
    public int Color;
    public int Animate;
    public int Collide;
    public float MinAngle;
    public float MaxAngle;
    public float MaxScale;
    public float Radius;
    public int LOD1;
    public int LOD2;
    public int LOD3;
    public int LOD4;
    public float RefHeight;
    public float RefWidth;
    public int Unique;
    public int Local;
    public int Terrain;
    public object Data;

    public List<string> Textures { get; set; }

    public ZoneNif(string[] data)
    {
      FieldInfo[] fields = this.GetType().GetFields();
      for (int index = 0; index < fields.Length && index < data.Length; ++index)
      {
        FieldInfo fieldInfo = fields[index];
        if (fieldInfo.FieldType == typeof (int))
        {
          int result = 0;
          int.TryParse(data[index], out result);
          fieldInfo.SetValue((object) this, (object) result);
        }
        if (fieldInfo.FieldType == typeof (float))
        {
          float result = 0.0f;
          float.TryParse(data[index], out result);
          fieldInfo.SetValue((object) this, (object) result);
        }
        else if (fieldInfo.FieldType == typeof (string))
        {
          string str = data[index];
          fieldInfo.SetValue((object) this, (object) str);
        }
      }
    }
  }
}
