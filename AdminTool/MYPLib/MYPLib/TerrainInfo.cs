﻿// Decompiled with JetBrains decompiler
// Type: MypLib.TerrainInfo
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System.IO;

namespace MypLib
{
  public class TerrainInfo
  {
    public int ScaleFactor;
    public int OffsetFctor;
    public byte[,] TerrainData;
    public byte[,] OffsetData;
    public int Width;
    public int Height;

    public TerrainInfo(Stream sector, Stream terrain, Stream offset)
    {
      if (sector != null)
      {
        StreamReader streamReader = new StreamReader(sector);
        while (!streamReader.EndOfStream)
        {
          string str = streamReader.ReadLine();
          if (str.StartsWith("scalefactor="))
            this.ScaleFactor = int.Parse(str.Replace("scalefactor=", ""));
          else if (str.StartsWith("offsetfactor"))
            this.OffsetFctor = int.Parse(str.Replace("offsetfactor=", ""));
        }
      }
      if (terrain.Length > 0L)
        this.TerrainData = this.LoadData(terrain);
      if (offset.Length <= 0L)
        return;
      this.OffsetData = this.LoadData(offset);
    }

    private void Load(string[] data)
    {
      foreach (string str in data)
      {
        if (str.StartsWith("scalefactor="))
          this.ScaleFactor = int.Parse(str.Replace("scalefactor=", ""));
        else if (str.StartsWith("offsetfactor"))
          this.OffsetFctor = int.Parse(str.Replace("offsetfactor=", ""));
      }
    }

    private byte[,] LoadData(Stream pcx)
    {
      PCX pcx1 = new PCX();
      pcx1.Load(pcx);
      this.Width = 1024;
      this.Height = 1024;
      byte[,] numArray = new byte[this.Width + 1, this.Height + 1];
      for (int x = 0; x < pcx1.Width; ++x)
      {
        for (int y = 0; y < pcx1.Height; ++y)
          numArray[x, y] = pcx1.GetValue(x, y);
      }
      return numArray;
    }
  }
}
