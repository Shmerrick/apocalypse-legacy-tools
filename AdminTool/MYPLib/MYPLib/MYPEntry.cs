﻿// Decompiled with JetBrains decompiler
// Type: MypLib.MYPEntry
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

namespace MypLib
{
  public class MYPEntry
  {
    public int ID { get; set; }

    public ulong Hash { get; set; }

    public MythicPackage ArchiveID { get; set; }

    public string Path { get; set; }
  }
}
