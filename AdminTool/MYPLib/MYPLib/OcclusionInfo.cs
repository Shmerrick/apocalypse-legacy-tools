﻿// Decompiled with JetBrains decompiler
// Type: MypLib.OcclusionInfo
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

namespace MypLib
{
  public struct OcclusionInfo
  {
    public int Result;
    public float HitX;
    public float HitY;
    public float HitZ;
    public float SafeX;
    public float SafeY;
    public float SafeZ;
    public int FixtureID;
    public int SurfaceType;
    public float WaterDepth;
  }
}
