﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.AbilityBin
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using MypLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MYPLib
{
  public class AbilityBin
  {
    public string Flags = "";
    public string DamageType = "";
    public List<AbilityComponent> Components = new List<AbilityComponent>();
    public bool RequiresPet;
    public bool IsBuff;
    public bool IsDebuff;
    public bool IsDamaging;
    public bool IsHealing;
    public bool IsDefensive;
    public bool IsOffensive;
    public bool IsStatsBuff;
    public bool IsGranted;
    public bool IsPassive;
    public byte A59_MoraleLevel;
    public ushort A170;

    public ushort A40_AbilityID { get; internal set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public uint A00_Castime { get; set; }

    public uint A04_Cooldown { get; set; }

    public TacticType A08_TacticType { get; set; }

    public TargetType A12_TargetType { get; set; }

    public AbilityType A16_AbilityType { get; set; }

    public uint A20 { get; set; }

    public uint A24 { get; set; }

    public CareerLine A28_CareerID { get; set; }

    public uint A32 { get; set; }

    public uint A36_Flags { get; set; }

    public override string ToString()
    {
      return this.Name.ToString();
    }

    public ushort A42_EffectID { get; set; }

    public ushort A44 { get; set; }

    public ushort A46_Range { get; set; }

    public ushort A48_Angle { get; set; }

    public ushort A50_MoraleCost { get; set; }

    public ushort A52_ChannelInterval { get; set; }

    public ushort A54 { get; set; }

    public ushort A56_ScaleStatMult { get; set; }

    public byte A58_NumTacticSlots { get; set; }

    public byte A60_AP { get; set; }

    public byte A61 { get; set; }

    public byte A62 { get; set; }

    public byte A63 { get; set; }

    public byte A64_AbilityImprovementThreshold { get; set; }

    public byte A65_AbilityImprovementCap { get; set; }

    public byte A66_Specialization { get; set; }

    public byte A67_StanceOrder { get; set; }

    public byte A68 { get; set; }

    public byte A69_MinLevel { get; set; }

    public byte A70 { get; set; }

    public byte A71 { get; set; }

    public ushort[] ComponentIDs { get; set; } = new ushort[10];

    public uint[] A92 { get; set; } = new uint[10];

    public byte[] A132 { get; set; } = new byte[8];

    public uint A136 { get; set; }

    public ushort A140 { get; set; }

    public ushort[] Labels1 { get; set; } = new ushort[4];

    public uint A142C { get; set; }

    public ushort[] Labels2 { get; set; } = new ushort[5];

    public byte[] A160 { get; set; } = new byte[10];

    public List<ExtData> Data { get; set; } = new List<ExtData>();

    public AbilityBin()
    {
    }

    public AbilityBin(
      ushort ID,
      AbilityBin cloneFrom,
      MYPManager manager,
      MythicPackage package,
      AbilityComponentExport compExpSource,
      AbilityComponentExport compExpDest)
    {
      this.A40_AbilityID = ID;
      if (cloneFrom == null)
        return;
      this.Name = cloneFrom.Name;
      this.Description = cloneFrom.Description;
      this.A00_Castime = cloneFrom.A00_Castime;
      this.A04_Cooldown = cloneFrom.A04_Cooldown;
      this.A08_TacticType = cloneFrom.A08_TacticType;
      this.A12_TargetType = cloneFrom.A12_TargetType;
      this.A16_AbilityType = cloneFrom.A16_AbilityType;
      this.A20 = cloneFrom.A20;
      this.A24 = cloneFrom.A24;
      this.A28_CareerID = cloneFrom.A28_CareerID;
      this.A32 = cloneFrom.A32;
      this.A36_Flags = cloneFrom.A36_Flags;
      this.A42_EffectID = cloneFrom.A42_EffectID;
      this.A44 = cloneFrom.A44;
      this.A46_Range = cloneFrom.A46_Range;
      this.A48_Angle = cloneFrom.A48_Angle;
      this.A50_MoraleCost = cloneFrom.A50_MoraleCost;
      this.A52_ChannelInterval = cloneFrom.A52_ChannelInterval;
      this.A54 = cloneFrom.A54;
      this.A56_ScaleStatMult = cloneFrom.A56_ScaleStatMult;
      this.A58_NumTacticSlots = cloneFrom.A58_NumTacticSlots;
      this.A59_MoraleLevel = cloneFrom.A59_MoraleLevel;
      this.A60_AP = cloneFrom.A60_AP;
      this.A61 = cloneFrom.A61;
      this.A62 = cloneFrom.A62;
      this.A63 = cloneFrom.A63;
      this.A64_AbilityImprovementThreshold = cloneFrom.A64_AbilityImprovementThreshold;
      this.A65_AbilityImprovementCap = cloneFrom.A65_AbilityImprovementCap;
      this.A66_Specialization = cloneFrom.A66_Specialization;
      this.A67_StanceOrder = cloneFrom.A67_StanceOrder;
      this.A68 = cloneFrom.A68;
      this.A69_MinLevel = cloneFrom.A69_MinLevel;
      this.A70 = cloneFrom.A70;
      this.A71 = cloneFrom.A71;
      for (int index = 0; index < 10; ++index)
      {
        if (cloneFrom.ComponentIDs[index] != (ushort) 0 && compExpSource.CompHash.ContainsKey(cloneFrom.ComponentIDs[index]))
        {
          AbilityComponent abilityComponent = compExpDest.NewComponent(manager, package, this, compExpSource.CompHash[cloneFrom.ComponentIDs[index]]);
          this.ComponentIDs[index] = abilityComponent.A11_ComponentID;
        }
      }
      for (int index = 0; index < 10; ++index)
        this.A92[index] = cloneFrom.A92[index];
      for (int index = 0; index < 8; ++index)
        this.A132[index] = cloneFrom.A132[index];
      this.A136 = cloneFrom.A136;
      this.A140 = cloneFrom.A140;
      for (int index = 0; index < 4; ++index)
        this.Labels1[index] = cloneFrom.Labels1[index];
      this.A142C = cloneFrom.A142C;
      for (int index = 0; index < 5; ++index)
        this.Labels2[index] = cloneFrom.Labels2[index];
      for (int index = 0; index < 10; ++index)
        this.A160[index] = cloneFrom.A160[index];
      this.A170 = cloneFrom.A170;
      foreach (ExtData loadFrom in cloneFrom.Data)
        this.Data.Add(new ExtData(loadFrom));
      foreach (Language language in Enum.GetValues(typeof (Language)))
      {
        string str1 = "data/strings/" + (object) language + "/abilitynames.txt";
        string str2 = "data/strings/" + (object) language + "/abilitydesc.txt";
        string str3 = manager.GetString(language, str1, (int) cloneFrom.A40_AbilityID, package);
        if (str3 != null && str3.Length > 0)
        {
          manager.SetString(language, str1, (int) this.A40_AbilityID, str3, package);
          string s = manager.GetString(language, str1);
          manager.UpdateAsset(package, str1, Encoding.Unicode.GetBytes(s), true);
        }
        string str4 = manager.GetString(language, str2, (int) cloneFrom.A40_AbilityID, package);
        if (str4 != null && str4.Length > 0)
        {
          manager.SetString(language, str2, (int) this.A40_AbilityID, str4, package);
          string s = manager.GetString(language, str2);
          manager.UpdateAsset(package, str2, Encoding.Unicode.GetBytes(s), true);
        }
      }
    }

    public static string ListToString(List<object> arr)
    {
      string str = "";
      foreach (object obj in arr)
      {
        if (obj is uint)
        {
          uint num = (uint) obj;
          str = num == 2863311530U ? str + "| " : str + num.ToString() + " ";
        }
        else if (obj is ushort)
        {
          ushort num = (ushort) obj;
          str = str + num.ToString() + " ";
        }
        else if (obj is byte)
        {
          byte num = (byte) obj;
          str = str + num.ToString() + " ";
        }
      }
      return str;
    }

    public static string ListToString(byte[] arr)
    {
      string str = "";
      foreach (byte num in arr)
        str = str + num.ToString() + " ";
      return str;
    }

    public static string ListToString(ushort[] arr)
    {
      string str = "";
      foreach (ushort num in arr)
        str = str + num.ToString() + " ";
      return str;
    }

    public static string ListToString(short[] arr)
    {
      string str = "";
      foreach (short num in arr)
        str = str + num.ToString() + " ";
      return str;
    }

    public static string ListToString(uint[] arr)
    {
      string str = "";
      foreach (uint num in arr)
        str = str + num.ToString() + " ";
      return str;
    }

    public AbilityBin(Stream stream)
    {
      long position1 = stream.Position;
      this.A00_Castime = PacketUtil.GetUint32R(stream);
      this.A04_Cooldown = PacketUtil.GetUint32R(stream);
      this.A08_TacticType = (TacticType) PacketUtil.GetUint32R(stream);
      this.A12_TargetType = (TargetType) PacketUtil.GetUint32R(stream);
      this.A16_AbilityType = (AbilityType) PacketUtil.GetUint32R(stream);
      this.A20 = PacketUtil.GetUint32R(stream);
      this.A24 = PacketUtil.GetUint32R(stream);
      this.A28_CareerID = (CareerLine) PacketUtil.GetUint32R(stream);
      this.A32 = PacketUtil.GetUint32R(stream);
      this.A36_Flags = PacketUtil.GetUint32R(stream);
      this.Flags = Convert.ToString((long) this.A36_Flags, 2).PadLeft(32, '0');
      this.RequiresPet = ((int) this.A36_Flags & 256) == 256;
      this.IsBuff = ((int) this.A36_Flags & 4) == 4;
      this.IsDebuff = ((int) this.A36_Flags & 8) == 8;
      this.IsDamaging = ((int) this.A36_Flags & 16) == 16;
      this.IsHealing = ((int) this.A36_Flags & 32) == 32;
      this.IsDefensive = ((int) this.A36_Flags & 64) == 64;
      this.IsOffensive = ((int) this.A36_Flags & 128) == 128;
      this.IsStatsBuff = ((int) this.A36_Flags & 262144) == 262144;
      this.IsGranted = ((int) this.A36_Flags & 1) == 1;
      this.IsPassive = ((int) this.A36_Flags & 2) == 2;
      this.A40_AbilityID = PacketUtil.GetUint16R(stream);
      this.A42_EffectID = PacketUtil.GetUint16R(stream);
      this.A44 = PacketUtil.GetUint16R(stream);
      this.A46_Range = PacketUtil.GetUint16R(stream);
      this.A48_Angle = PacketUtil.GetUint16R(stream);
      this.A50_MoraleCost = PacketUtil.GetUint16R(stream);
      this.A52_ChannelInterval = PacketUtil.GetUint16R(stream);
      this.A54 = PacketUtil.GetUint16R(stream);
      this.A56_ScaleStatMult = PacketUtil.GetUint16R(stream);
      this.A58_NumTacticSlots = PacketUtil.GetUint8(stream);
      this.A59_MoraleLevel = PacketUtil.GetUint8(stream);
      this.A60_AP = PacketUtil.GetUint8(stream);
      this.A61 = PacketUtil.GetUint8(stream);
      this.A62 = PacketUtil.GetUint8(stream);
      this.A63 = PacketUtil.GetUint8(stream);
      this.A64_AbilityImprovementThreshold = PacketUtil.GetUint8(stream);
      this.A65_AbilityImprovementCap = PacketUtil.GetUint8(stream);
      this.A66_Specialization = PacketUtil.GetUint8(stream);
      this.A67_StanceOrder = PacketUtil.GetUint8(stream);
      this.A68 = PacketUtil.GetUint8(stream);
      this.A69_MinLevel = PacketUtil.GetUint8(stream);
      for (int index = 0; index < 10; ++index)
        this.ComponentIDs[index] = PacketUtil.GetUint16R(stream);
      for (int index = 0; index < 10; ++index)
        this.A92[index] = PacketUtil.GetUint32R(stream);
      this.A132 = PacketUtil.GetByteArray(stream, 8);
      this.A136 = PacketUtil.GetUint32R(stream);
      this.A140 = PacketUtil.GetUint16R(stream);
      for (int index = 0; index < 4; ++index)
        this.Labels1[index] = PacketUtil.GetUint16R(stream);
      for (int index = 0; index < 5; ++index)
        this.Labels2[index] = PacketUtil.GetUint16R(stream);
      this.A160 = PacketUtil.GetByteArray(stream, 10);
      this.A170 = PacketUtil.GetUint16R(stream);
      for (int index = 0; index < 5; ++index)
      {
        if (PacketUtil.GetUint32R(stream) == 2863311530U)
          this.Data.Add(new ExtData()
          {
            Val1 = (int) PacketUtil.GetUint32R(stream),
            Val2 = (int) PacketUtil.GetUint32R(stream),
            Val3 = (int) PacketUtil.GetUint32R(stream),
            Val4 = (int) PacketUtil.GetUint32R(stream),
            Val5 = (int) PacketUtil.GetUint32R(stream),
            Val6 = (int) PacketUtil.GetUint32R(stream),
            Val7 = (int) PacketUtil.GetUint32R(stream),
            Val8 = (int) PacketUtil.GetUint32R(stream),
            Val9 = PacketUtil.GetUint8(stream)
          });
        else
          this.Data.Add(new ExtData()
          {
            Val1 = 0,
            Val2 = 0,
            Val3 = 0,
            Val4 = 0,
            Val5 = 0,
            Val6 = 0,
            Val7 = 0,
            Val8 = 0,
            Val9 = (byte) 0
          });
      }
      long position2 = stream.Position;
    }

    public void Save(Stream stream)
    {
      PacketUtil.WriteUInt32R(stream, this.A00_Castime);
      PacketUtil.WriteUInt32R(stream, this.A04_Cooldown);
      PacketUtil.WriteUInt32R(stream, (uint) this.A08_TacticType);
      PacketUtil.WriteUInt32R(stream, (uint) this.A12_TargetType);
      PacketUtil.WriteUInt32R(stream, (uint) this.A16_AbilityType);
      PacketUtil.WriteUInt32R(stream, this.A20);
      PacketUtil.WriteUInt32R(stream, this.A24);
      PacketUtil.WriteUInt32R(stream, (uint) this.A28_CareerID);
      PacketUtil.WriteUInt32R(stream, this.A32);
      PacketUtil.WriteUInt32R(stream, this.A36_Flags);
      PacketUtil.WriteUInt16R(stream, this.A40_AbilityID);
      PacketUtil.WriteUInt16R(stream, this.A42_EffectID);
      PacketUtil.WriteUInt16R(stream, this.A44);
      PacketUtil.WriteUInt16R(stream, this.A46_Range);
      PacketUtil.WriteUInt16R(stream, this.A48_Angle);
      PacketUtil.WriteUInt16R(stream, this.A50_MoraleCost);
      PacketUtil.WriteUInt16R(stream, this.A52_ChannelInterval);
      PacketUtil.WriteUInt16R(stream, this.A54);
      PacketUtil.WriteUInt16R(stream, this.A56_ScaleStatMult);
      PacketUtil.WriteByte(stream, this.A58_NumTacticSlots);
      PacketUtil.WriteByte(stream, this.A59_MoraleLevel);
      PacketUtil.WriteByte(stream, this.A60_AP);
      PacketUtil.WriteByte(stream, this.A61);
      PacketUtil.WriteByte(stream, this.A62);
      PacketUtil.WriteByte(stream, this.A63);
      PacketUtil.WriteByte(stream, this.A64_AbilityImprovementThreshold);
      PacketUtil.WriteByte(stream, this.A65_AbilityImprovementCap);
      PacketUtil.WriteByte(stream, this.A66_Specialization);
      PacketUtil.WriteByte(stream, this.A67_StanceOrder);
      PacketUtil.WriteByte(stream, this.A68);
      PacketUtil.WriteByte(stream, this.A69_MinLevel);
      for (int index = 0; index < 10; ++index)
        PacketUtil.WriteUInt16R(stream, this.ComponentIDs[index]);
      for (int index = 0; index < 10; ++index)
        PacketUtil.WriteUInt32R(stream, this.A92[index]);
      PacketUtil.WriteBytes(stream, this.A132);
      PacketUtil.WriteUInt32R(stream, this.A136);
      PacketUtil.WriteUInt16R(stream, this.A140);
      for (int index = 0; index < 4; ++index)
        PacketUtil.WriteUInt16R(stream, this.Labels1[index]);
      for (int index = 0; index < 5; ++index)
        PacketUtil.WriteUInt16R(stream, this.Labels2[index]);
      PacketUtil.WriteBytes(stream, this.A160);
      PacketUtil.WriteUInt16R(stream, this.A170);
      for (int index = 0; index < 5; ++index)
      {
        if (!this.Data[index].isEmpty())
        {
          PacketUtil.WriteUInt32R(stream, 2863311530U);
          this.Data[index].Save(stream);
        }
        else
          PacketUtil.WriteUInt32R(stream, 0U);
      }
    }
  }
}
