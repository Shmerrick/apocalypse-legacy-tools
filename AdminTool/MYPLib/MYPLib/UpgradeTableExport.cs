﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.UpgradeTableExport
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System.Collections.Generic;
using System.IO;

namespace MYPLib
{
  public class UpgradeTableExport
  {
    public List<UpgradeTable> Upgrades = new List<UpgradeTable>();
    public uint Header;
    public uint Size;

    public void Load(Stream stream)
    {
      this.Header = PacketUtil.GetUint32R(stream);
      this.Size = PacketUtil.GetUint32R(stream);
      while (stream.Position < stream.Length)
        this.Upgrades.Add(new UpgradeTable(stream));
    }

    public void Save(Stream stream)
    {
      PacketUtil.WriteUInt32R(stream, 1U);
      PacketUtil.WriteUInt32R(stream, (uint) this.Upgrades.Count);
      for (int index = 0; index < this.Upgrades.Count; ++index)
        this.Upgrades[index].Save(stream);
    }
  }
}
