﻿// Decompiled with JetBrains decompiler
// Type: MypLib.FixtureInfo
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

namespace MypLib
{
  public struct FixtureInfo
  {
    public float X1;
    public float Y1;
    public float Z1;
    public float X2;
    public float Y2;
    public float Z2;
    public int SurfaceType;
    public int UniqueID;

    public float Area { get; set; }

    public float Width
    {
      get
      {
        return this.X2 - this.X1;
      }
    }

    public float Height
    {
      get
      {
        return this.Y2 - this.Y1;
      }
    }

    public float Depth
    {
      get
      {
        return this.Z2 - this.Z1;
      }
    }
  }
}
