﻿using System.Runtime.InteropServices;

namespace WarFigleaf.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct CharacterMesh
    {
        public uint SourceIndex;
        public uint A02;
        public uint A03;
        public uint A04;
        public uint A05;
    }

    public class CharacterMeshView
    {
        public int Index { get; set; }
        public string Name { get; set; }
        public CharacterMesh CharacterMesh;

        public CharacterMeshView(CharacterMesh mesh)
        {
            CharacterMesh = mesh;
        }

        public uint SourceIndex { get => CharacterMesh.SourceIndex; set => CharacterMesh.SourceIndex = value; }
        public uint A02{ get => CharacterMesh.A02; set => CharacterMesh.A02 = value; }
        public uint A03{ get => CharacterMesh.A03; set => CharacterMesh.A03 = value; }
        public uint A04{ get => CharacterMesh.A04; set => CharacterMesh.A04 = value; }
        public uint A05 { get => CharacterMesh.A05; set => CharacterMesh.A05 = value; }
    }
}
