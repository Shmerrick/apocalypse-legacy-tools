﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WarFigleaf.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct FigString
    {
        public int Index;
        public uint Unk1;
        public byte Type;
        public string Value;
        public byte[] Raw;
        public byte Unk3;
    }

    public class FigStringView
    {
        public FigString FigString;
        public FigStringView(FigString figString)
        {
            FigString = figString;
        }

        public int Index { get => FigString.Index; set => FigString.Index = value; }
        public uint Unk1 { get => FigString.Unk1; set => FigString.Unk1 = value; }
        public byte Type { get => FigString.Type; set => FigString.Type = value; }
        public String Value { get => FigString.Value; set => FigString.Value = value; }
        public byte[] Raw { get => FigString.Raw; set => FigString.Raw = value; }

        public byte Unk3 { get => FigString.Unk3; set => FigString.Unk3 = value; }

        public override string ToString()
        {
            return Value;
        }
    }
}
