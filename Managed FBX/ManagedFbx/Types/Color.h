#pragma once

#include <fbxsdk.h>
#include "../Property.h"

using namespace System;

namespace UnexpectedBytes
{
	public value struct Colour
	{
	public:

		property double R;
		property double G;
		property double B;
		property double A;

		Colour(double r, double g, double b, double a)
		{
			R = r;
			G = g;
			B = b;
			A = a;
		}
		operator FbxColor()
		{
			return FbxColor(R, G, B, A);
		}

		virtual String ^ToString() override
		{
			return String::Format("{0}, {1}, {2}, {3}", Math::Round(R, 3), Math::Round(G, 3), Math::Round(B, 3), Math::Round(A, 3));
		}

	internal:

		Colour(FbxColor fbxColour)
		{
			R = fbxColour.mRed;
			G = fbxColour.mGreen;
			B = fbxColour.mBlue;
			A = fbxColour.mAlpha;
		}
	};
}
