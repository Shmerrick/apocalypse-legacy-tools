﻿namespace ManagedFbx.Viewer
{
    public class RegionNames
    {
        public const string FilesOpenedRegion = "FilesOpenedRegion";
        public const string ContentViewRegion = "ContentViewRegion";
    }
}
