﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Strings2FilenameLists
{
    class Program
    {
        static void Main(string[] args)
        {
            int capacity = 1000;
            int min_linelen = 9;
            string source = "myps1";


            long counter = 0;
            long read = 0;
            long skipped = 0;

            int dds_counter = 0;
            int stx_counter = 0;
            int nifdata_counter = 0;
            int nif_counter = 0;
            int xml_counter = 0;
            int ems_counter = 0;
            int csv_counter = 0;
            int txt_counter = 0;
            int xac_counter = 0;
            int xsm_counter = 0;
            int lua_counter = 0;
            int kf_counter = 0;
            int kfm_counter = 0;
            int bin_counter = 0;
            int dat_counter = 0;
            int db_counter = 0; ;
            int patch_counter = 0;
            int mask_counter = 0;
            int diffuse_counter = 0;
            int tint_counter = 0;
            int specular_counter = 0;
            int glow_counter = 0;
            int paths_counter = 0;
            int max_counter = 0;

            if (!Directory.Exists($"output_{source}"))
                Directory.CreateDirectory($"output_{source}");

            System.IO.StreamWriter dds_files = new System.IO.StreamWriter($"output_{source}\\dds.txt");
            System.IO.StreamWriter stx_files = new System.IO.StreamWriter($"output_{source}\\stx.txt");
            object nifdata_files = new object();// = new System.IO.StreamWriter($"output_{source}\\nifdata.txt");
            System.IO.StreamWriter nif_files = new System.IO.StreamWriter($"output_{source}\\nif.txt");
            System.IO.StreamWriter xml_files = new System.IO.StreamWriter($"output_{source}\\xml.txt");
            System.IO.StreamWriter ems_files = new System.IO.StreamWriter($"output_{source}\\ems.txt");
            System.IO.StreamWriter csv_files = new System.IO.StreamWriter($"output_{source}\\csv.txt");
            System.IO.StreamWriter txt_files = new System.IO.StreamWriter($"output_{source}\\txt.txt");
            System.IO.StreamWriter xac_files = new System.IO.StreamWriter($"output_{source}\\xac.txt");
            System.IO.StreamWriter xsm_files = new System.IO.StreamWriter($"output_{source}\\xsm.txt");
            System.IO.StreamWriter lua_files = new System.IO.StreamWriter($"output_{source}\\lua.txt");
            System.IO.StreamWriter kf_files = new System.IO.StreamWriter($"output_{source}\\kf.txt");
            System.IO.StreamWriter kfm_files = new System.IO.StreamWriter($"output_{source}\\kfm.txt");
            System.IO.StreamWriter bin_files = new System.IO.StreamWriter($"output_{source}\\bin.txt");
            System.IO.StreamWriter dat_files = new System.IO.StreamWriter($"output_{source}\\dat.txt");
            System.IO.StreamWriter db_files = new System.IO.StreamWriter($"output_{source}\\db.txt");
            System.IO.StreamWriter patch_files = new System.IO.StreamWriter($"output_{source}\\patch.txt");
            System.IO.StreamWriter mask_files = new System.IO.StreamWriter($"output_{source}\\mask.txt");
            System.IO.StreamWriter diffuse_files = new System.IO.StreamWriter($"output_{source}\\diffuse.txt");
            System.IO.StreamWriter tint_files = new System.IO.StreamWriter($"output_{source}\\tint.txt");
            System.IO.StreamWriter specular_files = new System.IO.StreamWriter($"output_{source}\\specular.txt");
            System.IO.StreamWriter glow_files = new System.IO.StreamWriter($"output_{source}\\glow.txt");
            System.IO.StreamWriter paths_files = new System.IO.StreamWriter($"output_{source}\\path.txt");
            System.IO.StreamWriter max_files = new System.IO.StreamWriter($"output_{source}\\max.txt");

            HashSet<string> dds = new HashSet<string>();
            HashSet<string> stx = new HashSet<string>();
            HashSet<string> nifdata = new HashSet<string>();
            HashSet<string> nif = new HashSet<string>();
            HashSet<string> xml = new HashSet<string>();
            HashSet<string> ems = new HashSet<string>();
            HashSet<string> csv = new HashSet<string>();
            HashSet<string> txt = new HashSet<string>();
            HashSet<string> xac = new HashSet<string>();
            HashSet<string> xsm = new HashSet<string>();
            HashSet<string> lua = new HashSet<string>();
            HashSet<string> kf = new HashSet<string>();
            HashSet<string> kfm = new HashSet<string>();
            HashSet<string> bin = new HashSet<string>();
            HashSet<string> dat = new HashSet<string>();
            HashSet<string> db = new HashSet<string>();
            HashSet<string> patch = new HashSet<string>();
            HashSet<string> mask = new HashSet<string>();
            HashSet<string> diffuse = new HashSet<string>();
            HashSet<string> tint = new HashSet<string>();
            HashSet<string> specular = new HashSet<string>();
            HashSet<string> glow = new HashSet<string>();
            HashSet<string> paths = new HashSet<string>();
            HashSet<string> max = new HashSet<string>();

            List<string> lines = new List<string>(capacity);

            Task.Run(() =>
            {
                while (true)
                {
                    unchecked
                    {
                        Console.Title = $"WAaaAGHhhH TOolz WOT iZ 4 MeKAniks =>< {read} read, {Interlocked.Read(ref skipped)} skipped, {Interlocked.Read(ref counter)} processed, {paths_counter} paths, {max_counter} max";
                    }
                    Thread.Yield();
                    Thread.Sleep(1000);
                }
            });

            //using (FileStream fs = File.Open(@"myps2.txt", FileMode.Open, FileAccess.Read, FileShare.None))
            {
                //using (BufferedStream bs = new BufferedStream(fs))
                {
                    //using (StreamReader sr = new StreamReader(bs))
                    using (StreamReader sr = File.OpenText($"{source}.txt"))
                    {
                        bool run = true;

                        string line =  new string((char)0x20, 1024);

                        unchecked
                        {
                            while (run)
                            {
                                lines.Clear();
                                for (int i = 0; i < capacity; ++i)
                                {
                                    line = sr.ReadLine();
                                    if (sr.EndOfStream)
                                    {
                                        run = false;
                                    }
                                    else if (!string.IsNullOrWhiteSpace(line))
                                    {
                                        lines.Add(line);
                                        ++read;
                                    }
                                    else
                                        ++skipped;
                                }

                                Parallel.ForEach(lines,  /* new ParallelOptions { MaxDegreeOfParallelism = 512 }, */ (pline) =>
                                {
                                    string lline = string.Empty;

                                    if (pline.Length > min_linelen)
                                    {
                                        int colon = pline.IndexOf(':', 2);
                                        if (colon != -1)
                                            lline = pline.Substring(colon + 1).Trim(new char[] { '\t', ' ', '"', '!', '`', '#' }).ToLower();
                                        else
                                            lline = pline.Trim(new char[] { '\t', ' ', '\"', '!', '`', '#' }).ToLower();
                                    }

                                    if (lline.Length > min_linelen)
                                    {

                                        if (lline.Contains(".dds"))
                                        {
                                            lock (dds_files)
                                            {
                                                if (!dds.Contains(lline))
                                                {
                                                    dds.Add(lline);
                                                    dds_files.WriteLine(lline);
                                                    ++dds_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".stx"))
                                        {
                                            lock (stx_files)
                                            {
                                                if (!stx.Contains(lline))
                                                {
                                                    stx.Add(lline);
                                                    stx_files.WriteLine(lline);
                                                    ++stx_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".nifdata"))
                                        {
                                            lock (nifdata_files)
                                            {
                                                //if (!nifdata.Contains(lline))
                                                // {
                                                //   nifdata.Add(lline);
                                                //nifdata_files.WriteLine(lline);
                                                ++nifdata_counter;
                                                //}
                                            }
                                        }
                                        else if (lline.Contains(".nif"))
                                        {
                                            lock (nif_files)
                                            {
                                                if (!nif.Contains(lline))
                                                {
                                                    nif.Add(lline);
                                                    nif_files.WriteLine(lline);
                                                    ++nif_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".xml"))
                                        {
                                            lock (xml_files)
                                            {
                                                if (!xml.Contains(lline))
                                                {
                                                    xml.Add(lline);
                                                    xml_files.WriteLine(lline);
                                                    ++xml_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".ems"))
                                        {
                                            lock (ems_files)
                                            {
                                                if (!ems.Contains(lline))
                                                {
                                                    ems.Add(lline);
                                                    ems_files.WriteLine(lline);
                                                    ++ems_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".csv"))
                                        {
                                            lock (csv_files)
                                            {
                                                if (!csv.Contains(lline))
                                                {
                                                    csv.Add(lline);
                                                    csv_files.WriteLine(lline);
                                                    ++csv_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".txt"))
                                        {
                                            lock (txt_files)
                                            {
                                                if (!txt.Contains(lline))
                                                {
                                                    txt.Add(lline);
                                                    txt_files.WriteLine(lline);
                                                    ++txt_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".xac"))
                                        {
                                            lock (xac_files)
                                            {
                                                if (!xac.Contains(lline))
                                                {
                                                    xac.Add(lline);
                                                    xac_files.WriteLine(lline);
                                                    ++xac_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".xsm"))
                                        {
                                            lock (xsm_files)
                                            {
                                                if (!xsm.Contains(lline))
                                                {
                                                    xsm.Add(lline);
                                                    xsm_files.WriteLine(lline);
                                                    ++xsm_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".lua"))
                                        {
                                            lock (lua_files)
                                            {
                                                if (!lua.Contains(lline))
                                                {
                                                    lua.Add(lline);
                                                    lua_files.WriteLine(lline);
                                                    ++lua_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".kfm"))
                                        {
                                            lock (kfm_files)
                                            {
                                                if (!kfm.Contains(lline))
                                                {
                                                    kfm.Add(lline);
                                                    kfm_files.WriteLine(lline);
                                                    ++kfm_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".kf"))
                                        {
                                            lock (kf_files)
                                            {
                                                if (!kf.Contains(lline))
                                                {
                                                    kf.Add(lline);
                                                    kf_files.WriteLine(lline);
                                                    ++kf_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".bin"))
                                        {
                                            lock (bin_files)
                                            {
                                                if (!bin.Contains(lline))
                                                {
                                                    bin.Add(lline);
                                                    bin_files.WriteLine(lline);
                                                    ++bin_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".dat"))
                                        {
                                            lock (dat_files)
                                            {
                                                if (!dat.Contains(lline))
                                                {
                                                    dat.Add(lline);
                                                    dat_files.WriteLine(lline);
                                                    ++dat_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".db"))
                                        {
                                            lock (db_files)
                                            {
                                                if (!db.Contains(lline))
                                                {
                                                    db.Add(lline);
                                                    db_files.WriteLine(lline);
                                                    ++db_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".dat"))
                                        {
                                            lock (dat_files)
                                            {
                                                if (!dat.Contains(lline))
                                                {
                                                    dat.Add(lline);
                                                    dat_files.WriteLine(lline);
                                                    ++dat_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".patch"))
                                        {
                                            lock (patch_files)
                                            {
                                                if (!patch.Contains(lline))
                                                {
                                                    patch.Add(lline);
                                                    patch_files.WriteLine(lline);
                                                    ++patch_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".glow"))
                                        {
                                            lock (glow_files)
                                            {
                                                if (!glow.Contains(lline))
                                                {
                                                    glow.Add(lline);
                                                    glow_files.WriteLine(lline);
                                                    ++glow_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".mask"))
                                        {
                                            lock (mask_files)
                                            {
                                                if (!mask.Contains(lline))
                                                {
                                                    mask.Add(lline);
                                                    mask_files.WriteLine(lline);
                                                    ++mask_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".diffuse"))
                                        {
                                            lock (diffuse_files)
                                            {
                                                if (!diffuse.Contains(lline))
                                                {
                                                    diffuse.Add(lline);
                                                    diffuse_files.WriteLine(lline);
                                                    ++diffuse_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".tint"))
                                        {
                                            lock (tint_files)
                                            {
                                                if (!tint.Contains(lline))
                                                {
                                                    tint.Add(lline);
                                                    tint_files.WriteLine(lline);
                                                    ++tint_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".specular"))
                                        {
                                            lock (specular_files)
                                            {
                                                if (!specular.Contains(lline))
                                                {
                                                    specular.Add(lline);
                                                    specular_files.WriteLine(lline);
                                                    ++specular_counter;
                                                }
                                            }
                                        }
                                        else if (lline.Contains(".max"))
                                        {
                                            lock (max_files)
                                            {
                                                if (!max.Contains(lline))
                                                {
                                                    max.Add(lline);
                                                    max_files.WriteLine(lline);
                                                    ++max_counter;
                                                }
                                            }
                                        }

                                        if (lline.Contains("/") && (lline.Contains("art") || lline.Contains("assetdb") || lline.Contains("interface") || lline.Contains("fixtures") || lline.Contains("zones") || lline.Contains("data")))
                                        {
                                            var s2 = lline.Replace("/", "");
                                            var hits = lline.Length - s2.Length;
                                            if (hits > 1)
                                            {
                                                lock (paths_files)
                                                {
                                                    if (!paths.Contains(lline))
                                                    {
                                                        paths.Add(lline);
                                                        paths_files.WriteLine(lline);
                                                        ++paths_counter;
                                                    }
                                                }
                                            }
                                        }
                                        Interlocked.Increment(ref counter);
                                    }
                                    else
                                        Interlocked.Increment(ref skipped);
                                });
                            }
                        }
                    }
                }
            }

            max_files.Close();
            paths_files.Close();
            patch_files.Close();
            glow_files.Close();
            mask_files.Close();
            diffuse_files.Close();
            specular_files.Close();
            tint_files.Close();
            db_files.Close();
            dat_files.Close();
            bin_files.Close();
            kfm_files.Close();
            kf_files.Close();
            lua_files.Close();
            xsm_files.Close();
            xac_files.Close();
            txt_files.Close();
            csv_files.Close();
            ems_files.Close();
            xml_files.Close();
            nif_files.Close();
            // nifdata_files.Close();
            stx_files.Close();
            dds_files.Close();

            System.Console.ReadLine();

        }
    }
}
