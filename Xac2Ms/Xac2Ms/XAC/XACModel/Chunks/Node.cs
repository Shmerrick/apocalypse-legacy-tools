﻿using System.IO;
using System.Runtime.InteropServices;

namespace UnexpectedBytes
{

    public class Node
    {
        public Quaternion mRotation;
        public Quaternion mBindPoseRotation;
        public Vector3F mPosition;
        public Vector3F mScale;
        public Vector3F mBindPosePosition;
        public int mLODMask;
        public int mParentID;
        public int mChildNodeCount;
        public uint mNodeFlags;
        public MatrixF44 mTransform;
        public string mName;
        public uint mUnknown1;
        public float mUnknown2;

        public Node()
        {
            mRotation = new Quaternion();
            mBindPoseRotation = new Quaternion();
            mPosition = new Vector3F();
            mScale = new Vector3F();
            mBindPosePosition = new Vector3F();
            mLODMask = 0;
            mParentID = 0;
            mChildNodeCount = 0;
            mNodeFlags = 0;
            mUnknown1 = 0;
            mUnknown2 = 0;
            mTransform = new MatrixF44();
            mName = string.Empty;
        }

        public void ReadIn(BinaryReader iStream, Chunk iChunk)
        {
            mRotation.ReadIn(iStream);
            mBindPoseRotation.ReadIn(iStream);
            mPosition.ReadIn(iStream);
            mScale.ReadIn(iStream);
            mBindPosePosition.ReadIn(iStream);
            mLODMask = iStream.ReadInt32();
            if (iChunk.mType == ChunkType.Nodes)
            {
                mChildNodeCount = iStream.ReadInt32();
                mParentID = iStream.ReadInt32();
                mNodeFlags = iStream.ReadUInt32();
                mUnknown1 = iStream.ReadUInt32();
                mTransform.ReadIn(iStream);
                mUnknown2 = iStream.ReadSingle();
            }
            else
                mParentID = iStream.ReadInt32();
            mName = Common.ReadString(iStream);
        }

        public void WriteOut(BinaryWriter iStream, Chunk iChunk)
        {
            mRotation.WriteOut(iStream);
            mBindPoseRotation.WriteOut(iStream);
            mPosition.WriteOut(iStream);
            mScale.WriteOut(iStream);
            mBindPosePosition.WriteOut(iStream);
            iStream.Write(mLODMask);
            if (iChunk.mType == ChunkType.Nodes)
            {
                iStream.Write(mChildNodeCount);
                iStream.Write(mParentID);
                iStream.Write(mNodeFlags);
                iStream.Write(mUnknown1);
                mTransform.WriteOut(iStream);
                iStream.Write(mUnknown2);
            }
            else
                iStream.Write(mParentID);
            Common.WriteString(iStream, mName);
        }

        public long GetSize(Chunk iChunk)
        {
            long vSize = 0;
            vSize += mRotation.GetSize();
            vSize += mBindPoseRotation.GetSize();
            vSize += mPosition.GetSize();
            vSize += mScale.GetSize();
            vSize += mBindPosePosition.GetSize();
            vSize += Marshal.SizeOf(mLODMask);
            vSize += Marshal.SizeOf(mParentID);
            if (iChunk.mType == ChunkType.Nodes)
            {
                vSize += Marshal.SizeOf(mChildNodeCount);
                vSize += Marshal.SizeOf(mNodeFlags);
                vSize += Marshal.SizeOf(mUnknown1);
                vSize += mTransform.GetSize();
                vSize += Marshal.SizeOf(mUnknown2);
            }
            vSize += sizeof(uint) + mName.Length;
            return vSize;
        }
    }
}
