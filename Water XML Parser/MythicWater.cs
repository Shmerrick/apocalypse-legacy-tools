﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace WarShared
{

    public class MythicWater
    {
        public static PointF ParsePointF(string csv)
        {

            var tok = csv.Split(new char[] { ',' });
            if (tok.Length > 1)
            {
                return new PointF(float.Parse(tok[0]), float.Parse(tok[1]));
            }
            return new PointF();

        }
        public class Water
        {
            public HEADER Header { get; set; }
            public List<WaterBody> WaterBody { get; set; }
            public Water()
            {
            }
        }

        public class FringeControlPoint
        {
            public int index { get; set; }
            public string pos { get; set; }

            public PointF Pos
            {
                get
                {
                    return MythicWater.ParsePointF(pos);
                }
            }
        }

        public class WaterFringe
        {
            public string name { get; set; }
            public string type { get; set; }
            public string looped { get; set; }
            public FringeControlPoints ControlPoints { get; set; }
        }
        public class Fringes
        {
            public List<WaterFringe> WaterFringe { get; set; }
        }
        public class WaterBodyPointPair
        {
            public int index { get; set; }
            public string left { get; set; }
            public string right { get; set; }

            public PointF Left
            {
                get
                {
                    return MythicWater.ParsePointF(left);
                }
            }

            public PointF Right
            {
                get
                {
                    return MythicWater.ParsePointF(right);
                }
            }
        }

        public class WaterControlPoints
        {
            public List<WaterBodyPointPair> WaterBodyPointPair { get; set; }
        }

        public class FringeControlPoints
        {
            public List<FringeControlPoint> FringeControlPoint { get; set; }
        }


        public class WaterBody
        {
            public string name { get; set; }
            public string type { get; set; }
            public float height { get; set; }
            public int ID;
            public int interior { get; set; }
            public SurfaceType LiquidType
            {
                get
                {
                    if (type != null)
                        return GetLiquidType(type);
                    return SurfaceType.WATER_GENERIC;
                }
            }
            public WaterControlPoints ControlPoints { get; set; }
            public Fringes Fringes { get; set; }
            public WaterBody()
            {
            }
        }
        public class HEADER
        {
            public string version { get; set; }
        }

        public Water WaterSystem { get; set; }
        public MythicWater()
        {
        }

        public static SurfaceType GetLiquidType(string type)
        {
            string name = type.ToLower();

            if (name.Contains("poison"))
                return SurfaceType.WATER_POISON;
            if (name.Contains("river"))
                return SurfaceType.WATER_RIVER;
            if (name.Contains("marsh"))
                return SurfaceType.WATER_MARSH;
            if (name.Contains("_tar_"))
                return SurfaceType.TAR;
            if (name.Contains("muck"))
                return SurfaceType.WATER_MUCK;
            if (name.Contains("dirty"))
                return SurfaceType.WATER_DIRTY;
            if (name.Contains("lava"))
                return SurfaceType.LAVA;
            if (name.Contains("magma"))
                return SurfaceType.LAVA_MAGMA;
            if (name.Contains("ocean"))
                return SurfaceType.WATER_OCEAN;
            if (name.Contains("hotspring"))
                return SurfaceType.WATER_HOTSPRING;
            if (name.Contains("ice") || name.Contains("icy"))
                return SurfaceType.WATER_ICY;
            if (name.Contains("lake"))
                return SurfaceType.WATER_LAKE;
            if (name.Contains("bog"))
                return SurfaceType.WATER_BOG;
            if (name.Contains("stream"))
                return SurfaceType.WATER_STREAM;
            if (name.Contains("tainted"))
                return SurfaceType.WATER_TAINTED;
            if (name.Contains("death"))
                return SurfaceType.INSTANT_DEATH;

            return SurfaceType.WATER_GENERIC;
        }
        public static MythicWater Load(Stream stream)
        {
            var doc = new XmlDocument();
            doc.Load(stream);
            var fringes = doc.GetElementsByTagName("Fringes");
            if (fringes != null)
            {
                foreach (XmlElement fringe in fringes)
                {
                    fringe.AppendChild(doc.CreateElement("WaterFringe"));
                }
            }

            var wb = (XmlElement)doc.GetElementsByTagName("WaterSystem")[0];
            if (wb != null)
            {

                wb.AppendChild(doc.CreateElement("WaterBody"));

            }
            var txt = JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.Indented);
            var water = JsonConvert.DeserializeObject<MythicWater>(Regex.Replace(txt, @"(\s*)""@(.*)"":", @"$1""$2"":", RegexOptions.IgnoreCase));
            if (water.WaterSystem.WaterBody != null)
                water.WaterSystem.WaterBody.RemoveAt(water.WaterSystem.WaterBody.Count - 1);

            if (water.WaterSystem.WaterBody != null)
            {
                int i = 1;
                foreach (var w in water.WaterSystem.WaterBody)
                {
                    if (w != null)
                    {
                        w.ID = i;
                        if (w.Fringes != null && w.Fringes.WaterFringe != null)
                            w.Fringes.WaterFringe.RemoveAt(w.Fringes.WaterFringe.Count - 1);
                        i++;
                    }

                }
            }
            else
                return null;


            return water;
        }
    }
}
