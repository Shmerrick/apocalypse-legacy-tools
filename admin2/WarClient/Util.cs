﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WarClient
{
    public static class TaskExtensions
    {
        //-----------------------------------------------------------------------------
        public static void RunInBackground(this Task task)
        {
            task.ContinueWith(t => { }, TaskContinuationOptions.OnlyOnFaulted);
        }

    }

    public static class Util
    {
        public static void CompareArray(byte[] arr1, byte[] arr2, int from = 0)
        {
            if (arr1.Length != arr2.Length)
            {
                Console.WriteLine("Difference in length Diff:" + Math.Abs(arr1.Length - arr2.Length));
                //throw new Exception("Invalid Length");
            }


            for (int i = from; i < arr1.Length; i++)
            {
                if (arr1[i] != arr2[i])
                    throw new Exception($"Invalid at:{i} ");
            }
        }

        public static string Hex(byte[] dump, int start, int len, int? current = null, int cols = 16, bool includeHeader = true)
        {
            var hexDump = new StringBuilder();

            if (includeHeader)
            {
                hexDump.AppendLine($"|{ new String('-', cols * 2 + cols)}|{ new String('-', cols)}|");
                for (int i = 0; i < cols; i++)
                {
                    string s1 = "";
                    string s2 = "";


                    s1 += i.ToString("X").PadLeft(2, '0') + " ";
                    s2 += (i % 16).ToString("X");

                    hexDump.AppendLine($"|{s1}|{s2}|");
                    hexDump.AppendLine($"|{ new String('-', cols * 2 + cols)}|{ new String('-', cols)}|");
                }
            }


            int end = start + len;
            for (int i = start; i < end; i += cols)
            {
                StringBuilder text = new StringBuilder();
                StringBuilder hex = new StringBuilder();


                for (int j = 0; j < cols; j++)
                {
                    if (j + i < end)
                    {
                        byte val = dump[j + i];

                        if (current.HasValue && current.Value == j + i)
                            hex.Append("[" + dump[j + i].ToString("X2") + "]");
                        else
                            hex.Append(dump[j + i].ToString("X2"));

                        hex.Append(" ");
                        if (val >= 32 && val < 127)
                        {
                            if (current.HasValue && current.Value == j + i)
                                text.Append("[" + (char)val + "]");
                            else
                                text.Append((char)val);
                        }
                        else
                        {
                            if (current.HasValue && current.Value == j + i)
                                text.Append("[.]");
                            else
                                text.Append(".");
                        }
                    }
                }

                hexDump.AppendLine("|" + hex.ToString().PadRight(cols * 2 + cols) + "|" + text.ToString().PadRight(cols) + "|");
            }

            if (includeHeader)
                hexDump.AppendLine($"-{ new String('-', cols * 2 + cols)}-{ new String('-', cols)}-");
            return hexDump.ToString();
        }

    }
}
