﻿using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class FixtureTable : FigTable<Fixture>
    {
        private const int HeaderPosition = 0x78;
        private const int RecordSize = 0x48;

        public FixtureTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (int i = 0; i < EntryCount; i++)
            {
                var fixture = new Fixture(_db, i);
                fixture.SourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                fixture.Unk1 = reader.ReadUInt32();
                fixture.Unk2 = reader.ReadUInt32();
                fixture.Unk3 = reader.ReadUInt32();
                fixture.Unk4 = reader.ReadUInt32();
                fixture.Unk5 = reader.ReadUInt32();
                fixture.Unk6 = reader.ReadUInt32();
                fixture.Unk7 = reader.ReadUInt32();
                fixture.Unk8 = reader.ReadUInt32();
                fixture.Unk9 = reader.ReadUInt32();
                fixture.Unk10 = reader.ReadUInt32();
                fixture.Unk11 = reader.ReadUInt32();
                fixture.Unk12 = reader.ReadUInt32();
                fixture.Unk13 = reader.ReadUInt32();
                fixture.CollisionSourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                fixture.Unk15 = reader.ReadUInt32();
                fixture.Unk16 = reader.ReadUInt32();
                fixture.Unk17 = reader.ReadUInt32();

                Records.Add(fixture);
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for (int i = 0; i < Records.Count; i++)
            {
                var fixture = Records[i];
                writer.Write((uint)fixture.SourceIndex);
                writer.Write(fixture.Unk1);
                writer.Write(fixture.Unk2);
                writer.Write(fixture.Unk3);
                writer.Write(fixture.Unk4);
                writer.Write(fixture.Unk5);
                writer.Write(fixture.Unk6);
                writer.Write(fixture.Unk7);
                writer.Write(fixture.Unk8);
                writer.Write(fixture.Unk9);
                writer.Write(fixture.Unk10);
                writer.Write(fixture.Unk11);
                writer.Write(fixture.Unk12);
                writer.Write(fixture.Unk13);
                writer.Write((uint)fixture.CollisionSourceIndex);
                writer.Write(fixture.Unk15);
                writer.Write(fixture.Unk16);
                writer.Write(fixture.Unk17);
            }

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)Records.Count);
            writer.Write((uint)(Records.Count > 0 ? pos : 0));
            var size = (uint)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }

    public class Fixture : FigRecord
    {
        public int Index { get; }
        public FigStringRef SourceIndex{ get; set; }
        public uint Unk1{ get; set; }
        public uint Unk2{ get; set; }
        public uint Unk3{ get; set; }
        public uint Unk4{ get; set; }
        public uint Unk5{ get; set; }
        public uint Unk6{ get; set; }
        public uint Unk7{ get; set; }
        public uint Unk8{ get; set; }
        public uint Unk9{ get; set; }
        public uint Unk10{ get; set; }
        public uint Unk11{ get; set; }
        public uint Unk12{ get; set; }
        public uint Unk13{ get; set; }
        public FigStringRef CollisionSourceIndex{ get; set; }
        public uint Unk15{ get; set; }
        public uint Unk16{ get; set; }
        public uint Unk17{ get; set; }
        public Fixture(FigleafDB db, int index) : base(db) { Index = index; }
    }
}

