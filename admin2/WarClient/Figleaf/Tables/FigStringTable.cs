﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WarClient.Figleaf.Tables
{
    public class FigString : FigRecord
    {
        public int Index { get; }
        public uint Unk1 { get; set; }
        public byte Type { get; set; }
        public String Value { get; set; }
        public byte Unk3 { get; set; }

        public override string ToString()
        {
            return Value;
        }

        public FigString(FigleafDB db, int index) : base(db) { Index = index; }
    }

    public class FigStringRef
    {
        private FigleafDB _db;

        public int SourceIndex { get; set; }

        private FigString _ref;
        public FigString Ref
        {
            get
            {
                if (_ref == null)
                {
                    var _ref = _db.TableStrings1.GetString(SourceIndex);
                    if (_ref == null)
                        return null;
                    return _ref;
                }
                else
                {
                    return _ref;
                }
            }
        }
        public string Value
        {
            get
            {
                if (_ref == null)
                {
                    var _ref = _db.TableStrings1.GetString(SourceIndex);
                    if (_ref == null)
                        return $"[{SourceIndex}]";
                    return _ref.Value;
                }
                else
                {
                     return _ref.Value;
                }
            }
        }

        public static implicit operator int(FigStringRef r)
        {
            return r.SourceIndex;
        }

        public static implicit operator uint(FigStringRef r)
        {
            return (uint)r.SourceIndex;
        }

           public  string FigString()
        {
            return Value;
        }

        public override string ToString()
        {
            return Value + " [" + SourceIndex + "]";
        }
        public FigStringRef(FigleafDB db, int sourceIndex)
        {
            _db = db;
            SourceIndex = sourceIndex;
        }

         public FigStringRef(FigleafDB db, uint sourceIndex)
        {
            _db = db;
            SourceIndex = (int)sourceIndex;

        }
    }
    public class FigStringTable : FigTable<FigString>
    {
        private const int HeaderPosition = 0xC;
        private Dictionary<string, FigString> _hash = new Dictionary<string, FigString>();
        private FigString _invalidString;
        public FigStringTable(FigleafDB db) : base(db)
        {
            _invalidString = new FigString(_db, -1);
        }

        public FigString GetString(int index)
        {
            if (index >= 0 && index < Records.Count)
                return Records[index];
            return null;
        }

        public int GetStringIndex(string str)
        {
            if (_hash.ContainsKey(str))
                return _hash[str].Index;
            return -1;
        }

        public bool HasString(string value)
        {
            if (_hash.ContainsKey(value))
                return true;
            return false;
        }

        public FigString AddString(string value)
        {
            if (HasString(value))
            {
                _hash[value].Value = value;
                return _hash[value];
            }
            else
            {
                var str = new FigString(_db, Records.Count)
                {
                    Value = value
                };
                _hash[value] = str;
                Records.Add(str);
                return str;
            }
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;
            byte[] str = new byte[0xFFFF];


            for (int i = 0; i < EntryCount; i++)
            {
                var Unk1 = reader.ReadUInt32();
                var type = reader.ReadByte();
                var offset = reader.BaseStream.Position;

                int size = 0;
                for (size = 0; size < 0xFFFF; size++)
                {
                    byte c = (byte)reader.ReadByte();
                    if (c == '\0')
                        break;
                    str[size] = c;
                }

                var fs = new FigString(_db, i)
                {
                    Unk1 = Unk1,
                    Type = type,
                    Value = Encoding.GetEncoding(437).GetString(str, 0, size),
                    Unk3 = reader.ReadByte(),
                };
                _hash[fs.Value] = fs;

                Records.Add(fs);
            }

            var b = reader.ReadByte();

        }

        public override void Save(BinaryWriter writer)
        {
            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)0);
            writer.Write((uint)0);
            writer.Write((uint)0);

            var pos = writer.BaseStream.Position;
            writer.BaseStream.Position = 0x33c;

            for (int i = 0; i < Records.Count; i++)
            {
                var starPos = writer.BaseStream.Position;
                var entry = Records[i];

                writer.Write((uint)entry.Unk1);
                writer.Write((byte)entry.Type);

                if (!String.IsNullOrEmpty(entry.Value))
                    writer.Write(Encoding.GetEncoding(437).GetBytes(entry.Value), 0, entry.Value.Length);

                writer.Write((byte)0);
                writer.Write((byte)entry.Unk3);
            }

            writer.Write((byte)0);
            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)(Records.Count));
            writer.Write((uint)(0x33c));
            writer.Write((uint)(endPos -0x33c));

            writer.BaseStream.Position = endPos;
        }
    }
}
