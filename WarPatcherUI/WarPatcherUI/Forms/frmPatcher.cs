﻿using System;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarPatcherUI
{
    public partial class frmPatcher : MythicForm
    {
        #region members
        public override string _bgName { get; set; } = "mainbackground";
        public override string _windowName { get; set; } = "mainwindow";
        private Patcher _patcher;
        public LayeredWindowForm ContainerForm = null;
        private readonly Config Config;
        #endregion

        #region properties
        public string Title
        {
            get
            {
                return lblTitle.Text;
            }
            set
            {
                lblTitle.Text = value;
                Text = value;
            }
        }
        #endregion

        public frmPatcher(Config config = null)
        {
            InitializeComponent();
            Title = "Patcher";
            Config = config;
        }

        public frmPatcher(Config config, string username, string password)
        {
            if (DesignMode)
                return;

            Config = config;

            Util.UIThread = TaskScheduler.FromCurrentSynchronizationContext();

            var clientPatcher = new WarPatcherWrapper.ClientPatcher();
            _patcher = new Patcher(Config, username, password, clientPatcher, new Downloader(Config));
            _patcher.OnMOTD += _patcher_OnMOTD;
            _patcher.OnStateChanged += _patcher_OnStateChanged;
            clientPatcher.OnLog += Patcher_OnLog;

            InitializeComponent();

            InitResource(
                "WarPatcherUI.Resources.patch.txt",
                "WarPatcherUI.Resources.layout.xml",
                "WarPatcherUI.Resources.patcher.png",
                "WarPatcherUI.Resources.ageofreckoning.ttf");



            lblTotal.Text = "";
            lblFile.Text = "";
            patchNotes.BackColor = Color.FromArgb(16,16,17);
            patchNotes.ForeColor = Color.White;
            patchNotes.Multiline = true;
            patchNotes.WordWrap = false;
            patchNotes.Text = "";

            Title = "Patcher";
        }

        #region methods
        private void _patcher_OnStateChanged(Patcher patcher, PatcherState state)
        {
            Util.UITask(t =>
            {
                btnPlay.Enabled = state == PatcherState.Ready || state == PatcherState.Playing;
                if (state == PatcherState.Playing)
                {
                    btnMinimize_Click(null, null);
                }
            });
        }

        private void _patcher_OnMOTD(Patcher patcher, string msg)
        {
            Util.UITask(t =>
            {
                patchNotes.Text = msg;
            });
        }

        private void Patcher_OnLog(WarPatcherWrapper.LogType type, WarPatcherWrapper.LogLevel level, string msg)
        {
            Log.Info(msg);
        }


        private void btnPlay_Click(object sender, EventArgs e)
        {
            _patcher.Play();
        }


        private void frmPatcher_Load(object sender, EventArgs e)
        {
            var thread = new Thread(() =>
            {
                _patcher.UpdateState().Wait();
            });

            thread.IsBackground = true;
            thread.Start();
                         ContainerForm = alphaFormTransformer1.TransformForm2(255);
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            if(ContainerForm != null)
            ContainerForm.WindowState = FormWindowState.Minimized;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
             pbFile.Value = 0;
            mTotalProgress.Value = 0;
            if (_patcher.State == PatcherState.GetServerStatus)
            {
                lblFile.Text = "Retreiving server status";
            }
            else if (_patcher.State == PatcherState.InitializeArchives && _patcher.TotalSize > 0)
            {
                lblFile.Text = $"Initializing {_patcher.CurrentItemName} ({_patcher.TotalCurrent}/{_patcher.TotalSize})";
                pbFile.Value = _patcher.CurrentPercent;
            }
            else if (_patcher.State == PatcherState.RequestManifest)
            {
                lblFile.Text = "Requesting Manifest";
            }
            else if (_patcher.State == PatcherState.RequestAssets)
            {
                lblFile.Text = $"{_patcher.CurrentItemName}";
                 pbFile.Value = 0;

                lblTotal.Text = $"Downloading {(float)Math.Round(_patcher.TotalPercent, 0)}% ({Util.FormatSizeString(_patcher.TotalSize)})";
                mTotalProgress.Value = (float)Math.Round(_patcher.TotalPercent,2);
            }
            else if (_patcher.State == PatcherState.ValidateFiles)
            {
                lblFile.Text = "Verifying";
            }
            else if (_patcher.State == PatcherState.Ready)
            {
                lblFile.Text = "Ready";
                lblTotal.Text = "";
            }
            else
            {
                if (lblFile.Text != "")
                    lblFile.Text = "";
            }

            mTotalProgress.Paint();
            pbFile.Paint();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion
    }
}
