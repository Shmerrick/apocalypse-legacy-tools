﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarPatcherUI
{
    public partial class frmNewAccount : Form
    {
        public string Username { get { return txtUsername.Text.Trim(); } set { txtPassword1.Text = value; } }
        public string Email { get { return txtEmail.Text; } set { txtEmail.Text = value; } }
        public string Password { get { return txtPassword1.Text; } }

        public frmNewAccount()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (Username.Length < 4)
            {
                txtUsername.Focus();
                MessageBox.Show("Username must be between 4 and 45 characters");
                return;
            }

            if (Email.Length < 4)
            {
                txtEmail.Focus();
                MessageBox.Show("Email must be greater than 4 characters.");
                return;
            }

            if (txtPassword1.Text.Length < 4)
            {
                txtPassword1.Focus();
                MessageBox.Show("Password must be between 4 and 45 characters");
                return;
            }


            if (txtPassword1.Text != txtPassword2.Text)
            {
                txtPassword1.Focus();
                MessageBox.Show("Passwords do not match");
                return;
            }
            DialogResult = DialogResult.OK;

        }

        private void frmNewAccount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                btnCancel_Click(null, null);
            else if (e.KeyCode == Keys.Enter)
                btnOK_Click(null, null);
        }
    }
}
