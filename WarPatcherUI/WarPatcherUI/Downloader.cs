﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WarPatcherUI.Types;

namespace WarPatcherUI
{
    public class Downloader
    {
        private Queue<ManifestItem> _downloadQueue = new Queue<ManifestItem>();
        private HttpUtil _httpUtil;
        private long TotalSize;
        private long Current;
        public delegate void ProgressDelegate(Downloader downloader, string asset, long current, long currentTotal, long overallCurrent, long overallTotal, int rate);
        public ProgressDelegate OnDownloadProgress;
        public int Remain = 0;


        public delegate Task DownloadDelegate();
        public DownloadDelegate _OnDownloadQueueFinished;

        public delegate Task AssetDelegate(ManifestAssetItem asset);
        public AssetDelegate _OnAssetDownloaded;

        public delegate Task FileDelegate(ManifestFileItem asset);
        public FileDelegate _OnFileDownloaded;


        public Downloader(HttpUtil httpUtil)
        {
            _httpUtil = httpUtil;
        }

        public void AddAsset(ManifestItem file)
        {
            lock (_downloadQueue)
            {
                TotalSize += file.Size;
                _downloadQueue.Enqueue(file);
            }
        }

        private async Task DownloadFile(ManifestFileItem item)
        {
           Log.Info($"Starting download of FILE {item.Name}");

            Stopwatch watch = new Stopwatch();
            watch.Start();

            item.TempFileName = Guid.NewGuid().ToString().Replace("-", "");
            int rate = 0;

            using (item.Stream = new FileStream(item.TempFileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
            {
                 _httpUtil.RequestStream(Program.PatcherServer, new REQUEST_FILE() { ID = item.ID }, item.Stream, (current, total, read) =>
                  {
                      Current += read;

                      if (total != item.Size)
                      {
                          throw new Exception($"Wrong size {item.Name} ({total}:{item.Size})");
                      }

                      if (OnDownloadProgress != null)
                          OnDownloadProgress(this, item.Name, current, total, Current, TotalSize, rate);

                      if (watch.ElapsedMilliseconds < 1000)
                      {
                          rate += read;
                      }
                      else
                      {
                          rate -= read;
                          watch.Reset();
                          watch.Restart();
                      }
                  });
            }
            Remain--;
            await _OnFileDownloaded(item);

            if (Remain == 0 && _OnDownloadQueueFinished != null)
                await _OnDownloadQueueFinished();            
        }
        public static byte[] DeCompress(byte[] data, int offset = 0)
        {
            var outB = new MemoryStream(data);
            outB.Position = offset;
            using (DeflateStream archive = new DeflateStream(outB, CompressionMode.Decompress, true))
            {
                var dest = new MemoryStream();
                archive.CopyTo(dest);
                return dest.ToArray();
            }
        }

        int rate = 0;
        int mRate = 0;
        Stopwatch watch = new Stopwatch();

        private async Task DownloadAsset(ManifestAssetItem item)
        {

            if (!watch.IsRunning)
                watch.Start();
  
            var ms = new MemoryStream();

            _httpUtil.RequestStream(Program.PatcherServer, new REQUEST_ASSET() { Archive = item.Archive, Hash = item.Hash }, ms, (current, total, read) =>
            {
                Current += read;
                if (total != item.CompressedSize)
                {
                    throw new Exception($"Wrong size {item.Name} ({total}:{item.CompressedSize})");
                }

                if (OnDownloadProgress != null)
                    OnDownloadProgress(this, (item.Name??item.Hash.ToString("x2")) + " " + item.Archive.ToString(), current, total, Current, TotalSize, mRate);

                if (watch.ElapsedMilliseconds < 1000)
                {
                    rate += read;
                }
                else
                {
                    mRate = rate;
                    rate = 0;
                    watch.Reset();
                    watch.Restart();
                }
            });


            Remain--;
            if (ms.Length != item.CompressedSize)
            {
                throw new Exception($"Wrong size ({ms.Length}:{item.Size})");
            }
            item.Data = ms.ToArray();

            if (item.Compressed)
            {
                item.Data = DeCompress(item.Data, 2);
                if (item.Data.Length != item.Size)
                    throw new Exception($"Wrong extracted size (recv:{item.Data.Length } expected:{item.Size})");
            }
   
            if (_OnAssetDownloaded != null)
                await _OnAssetDownloaded(item);

            if (Remain == 0 && _OnDownloadQueueFinished != null)
                await _OnDownloadQueueFinished();

        }

        public async Task ProcessQueue()
        {
            var downloadTasks = new List<Task>();
            List<ManifestItem> items = new List<ManifestItem>();
            TotalSize = 0;
            Current = 0;

            lock (_downloadQueue)
            {
                items = _downloadQueue.ToList();
                _downloadQueue.Clear();
            }

            if (items.Count > 0)
            {
                Remain = items.Count;
                foreach (var item in items)
                {
                    if (item is ManifestAssetItem)
                    {
                        TotalSize += ((ManifestAssetItem)item).CompressedSize;
                    }
                    else if (item is ManifestFileItem)
                    {
                        TotalSize += ((ManifestFileItem)item).Size;
                    }
                }


                foreach (var item in items)
                {
                    if (item is ManifestAssetItem)
                    {
                        downloadTasks.Add(DownloadAsset((ManifestAssetItem)item));
                    }
                    else if (item is ManifestFileItem)
                    {
                        downloadTasks.Add(DownloadFile((ManifestFileItem)item));
                    }
                }

                await Task.WhenAll(downloadTasks);
            }
        }
    }
}
