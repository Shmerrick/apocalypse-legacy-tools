﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WarPatcherUI.Types;

namespace WarPatcherUI
{
    public class Downloader
    {
        #region events
        public delegate Task DownloadDelegate();
        public DownloadDelegate OnDownloadQueueFinished;

        public delegate Task AssetDelegate(ManifestAssetItem asset);
        public AssetDelegate OnAssetDownloaded;

        public delegate Task FileDelegate(ManifestFileItem asset);
        public FileDelegate OnFileDownloaded;

        public delegate void ProgressDelegate(Downloader downloader, string asset, long current, long currentTotal, long overallCurrent, long overallTotal, int rate);
        public ProgressDelegate OnDownloadProgress;
        #endregion

        #region members
        private Queue<ManifestItem> _downloadQueue = new Queue<ManifestItem>();
        private long _totalSize;
        private long _current;
        private int _remain = 0;
        private int _rate = 0;
        private int _mRate = 0;
        private readonly Config Config;
        private Stopwatch _watch = new Stopwatch();
        #endregion

        public Downloader(Config config)
        {
            Config = config;
        }

        #region methods

        public void AddAsset(ManifestItem file)
        {
            lock (_downloadQueue)
            {
                _totalSize += file.Size;
                _downloadQueue.Enqueue(file);
            }
        }

        private async Task DownloadFile(ManifestFileItem item)
        {
            Log.Info($"Starting download of FILE {item.Name}");

            Stopwatch watch = new Stopwatch();
            watch.Start();

            item.TempFileName = Guid.NewGuid().ToString().Replace("-", "");
            int rate = 0;

            using (item.Stream = new FileStream(item.TempFileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
            {
                HttpUtil.RequestStream(Config.PatcherServer, new REQUEST_FILE() { ID = item.ID }, item.Stream, (current, total, read) =>
                 {
                     _current += read;

                     if (total != item.Size)
                     {
                         throw new Exception($"Wrong size {item.Name} ({total}:{item.Size})");
                     }

                     if (OnDownloadProgress != null)
                         OnDownloadProgress(this, item.Name, current, total, _current, _totalSize, rate);

                     if (watch.ElapsedMilliseconds < 1000)
                     {
                         rate += read;
                     }
                     else
                     {
                         rate -= read;
                         watch.Reset();
                         watch.Restart();
                     }
                 });
            }

            _remain--;

            await OnFileDownloaded(item);

            if (_remain == 0 && OnDownloadQueueFinished != null)
                await OnDownloadQueueFinished();
        }

        private async Task DownloadAsset(ManifestAssetItem item)
        {

            if (!_watch.IsRunning)
                _watch.Start();
  
            var ms = new MemoryStream();

            HttpUtil.RequestStream(Config.PatcherServer, new REQUEST_ASSET() { Archive = item.Archive, Hash = item.Hash }, ms, (current, total, read) =>
            {
                _current += read;
                if (total != item.CompressedSize)
                {
                    throw new Exception($"Wrong size {item.Name} ({total}:{item.CompressedSize})");
                }

                if (OnDownloadProgress != null)
                    OnDownloadProgress(this, (item.Name??item.Hash.ToString("x2")) + " " + item.Archive.ToString(), current, total, _current, _totalSize, _mRate);

                if (_watch.ElapsedMilliseconds < 1000)
                {
                    _rate += read;
                }
                else
                {
                    _mRate = _rate;
                    _rate = 0;
                    _watch.Reset();
                    _watch.Restart();
                }
            });


            _remain--;
            if (ms.Length != item.CompressedSize)
            {
                throw new Exception($"Wrong size ({ms.Length}:{item.Size})");
            }
            item.Data = ms.ToArray();

            if (item.Compressed)
            {
                item.Data = Util.DeCompress(item.Data, 2);
                if (item.Data.Length != item.Size)
                    throw new Exception($"Wrong extracted size (recv:{item.Data.Length } expected:{item.Size})");
            }
   
            if (OnAssetDownloaded != null)
                await OnAssetDownloaded(item);

            if (_remain == 0 && OnDownloadQueueFinished != null)
                await OnDownloadQueueFinished();

        }

        public async Task ProcessQueue()
        {
            var downloadTasks = new List<Task>();
            List<ManifestItem> items = new List<ManifestItem>();
            _totalSize = 0;
            _current = 0;

            lock (_downloadQueue)
            {
                items = _downloadQueue.ToList();
                _downloadQueue.Clear();
            }

            if (items.Count > 0)
            {
                _remain = items.Count;
                foreach (var item in items)
                {
                    if (item is ManifestAssetItem)
                    {
                        _totalSize += ((ManifestAssetItem)item).CompressedSize;
                    }
                    else if (item is ManifestFileItem)
                    {
                        _totalSize += ((ManifestFileItem)item).Size;
                    }
                }


                foreach (var item in items)
                {
                    if (item is ManifestAssetItem)
                    {
                        downloadTasks.Add(DownloadAsset((ManifestAssetItem)item));
                    }
                    else if (item is ManifestFileItem)
                    {
                        downloadTasks.Add(DownloadFile((ManifestFileItem)item));
                    }
                }

                await Task.WhenAll(downloadTasks);
            }
        }
        #endregion
    }
}
