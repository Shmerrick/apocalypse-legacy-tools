﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace WarPatcherUI
{
    public static class HttpUtil
    {
        public delegate void StreamProgress(long current, long total, int chunkSize);

        #region methods

        private static WebRequest Create(string address, string endpoint, byte[] body)
        {
            var path = $@"http://{address}/{endpoint}";

            var request = HttpWebRequest.Create(path);

            request.Proxy = null;


            request.ContentType = "application/json";
            request.Method = "POST";

            if (body != null)
            {
                request.ContentLength = (long)body.Length;
                var bodyStream = request.GetRequestStream();
                bodyStream.Write(body, 0, body.Length);
            }
            else
            {
                       var bodyStream = request.GetRequestStream();
            }

            return request;
        }

        public static async Task<TResult> RequestAsync<TResult, TSend>(string address, TSend sendData)
        {
            var request = Create(address, typeof(TSend).Name, GetDataArray(sendData));
            var response = await request.GetResponseAsync();

            using (var stream = response.GetResponseStream())
            {
                var reader = new StreamReader(stream);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<TResult>(reader.ReadToEnd());
            }
        }

        public static TResult Request<TResult, TSend>(string address, TSend sendData)
        {
            var request = Create(address, typeof(TSend).Name, GetDataArray(sendData));
            var response = request.GetResponse();

            using (var stream = response.GetResponseStream())
            {
                var reader = new StreamReader(stream);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<TResult>(reader.ReadToEnd());
            }
        }

        public static async Task<TResult> RequestAsync<TResult>(string address)
        {
            var request = Create(address,typeof(TResult).Name,null);
            var response = await request.GetResponseAsync();

            using (var stream = response.GetResponseStream())
            {
                var reader = new StreamReader(stream);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<TResult>(reader.ReadToEnd());
            }
        }

        public static TResult Request<TResult>(string address)
        {
            var request = Create(address, typeof(TResult).Name, null);
            var response = request.GetResponse();

            using (var stream = response.GetResponseStream())
            {
                var reader = new StreamReader(stream);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<TResult>(reader.ReadToEnd());
            }
        }


        public static async Task RequestStreamAsync<TSend>(string address, TSend sendData, Stream output, StreamProgress progress, int blockSize = 0x8FFF)
        {
            var request = Create(address, typeof(TSend).Name, GetDataArray(sendData));
            var response = await request.GetResponseAsync();

            using (var stream = response.GetResponseStream())
            {
                long current = 0;
                long total = response.ContentLength;
                byte[] block = new byte[blockSize];
                while (current < total)
                {
                    int read = stream.Read(block, 0, blockSize);
                    if (read > 0)
                    {
                        output.Write(block, 0, read);
                        current += read;
                        progress(current, total, read);
                    }
                    else
                    {
                        progress(current, total, read);
                        break;
                    }
                }
            }
        }

       public static void RequestStream<TSend>(string address, TSend sendData, Stream output, StreamProgress progress, int blockSize = 0x8FFF)
        {
            var request = Create(address, typeof(TSend).Name, GetDataArray(sendData));
            var response = request.GetResponse();

            using (var stream = response.GetResponseStream())
            {
                long current = 0;
                long total = response.ContentLength;
                byte[] block = new byte[blockSize];
                while (current < total)
                {
                    int read = stream.Read(block, 0, blockSize);
                    if (read > 0)
                    {
                        output.Write(block, 0, read);
                        current += read;
                        progress(current, total, read);
                    }
                    else
                    {
                        progress(current, total, read);
                        break;
                    }
                }
            }
        }

        public static byte[] GetDataArray(object data)
        {
            return System.Text.Encoding.UTF8.GetBytes(new JavaScriptSerializer().Serialize(data));
        }

        #endregion
    }
}
