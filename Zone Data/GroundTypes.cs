  public enum GroundType
    {
        SOLID = 0,
        WATER = 1,
        WATER_DEEP = 2,
        LAVA = 3,
        INSTANT_DEATH = 4,
    }

    public enum ActionType
    {
        NONE = 0,
        SWIMING = 1,
        JUMPING = 2,
        DEAD = 5,
    }
    public enum StrafeType : int
    {
        NONE = 0,
        FORWARD = 1,
        LEFT = 2,
        FORWARD_LEFT = 3,
        RIGHT = 4,
        FORWARD_RIGHT = 5
    }