﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NIFLibrary;

namespace UnexpectedBytes.War
{
    public static class Program
    {
        public static int Main(string[] args)
        {
            System.Console.WriteLine("[!] Warhammer Online ZoneXXX to Wavefront Obj conversion tool v0.0.1");
            System.Console.WriteLine("[?] Written by Debuggin <andi.ireland@gmail.com> and Zaru <dan.andraschko@gmail.com>");
            System.Console.WriteLine("[.]");

            //args = new string[3];
            //args[0] = "-ZONE2OBJ";
            //args[1] = @"D:\Games\Warhammer Online -Age of Reckoning\myps\world.myp\zones";
            //args[2] = @"D:\Games";
            //args[3] = @"205";

            //args = new string[3];
            //args[0] = "-nif";
            //args[1] = @"D:\Develop\war-apocalypse\myps\art.myp\art\nifs\effects";
            //args[2] = @"D:\Develop\war-apocalypse\nif\";

            if (args.Length <= 0 || args.Length > 4)
            {
                System.Console.WriteLine("[!] Error: Invalid number of parameters.");
                System.Console.WriteLine("[.]");

                return 1;
            }

            if (args.Length <= 0 || args.Length > 4)
            {
                if (args[0] == "/?" || args[0] == "/h" || args[0] == "/help" || args[0] == "-h" || args[0] == "-help" || args[0] == "--h" || args[0] == "--help" || args[0] == "help")
                {
                    System.Console.WriteLine("[!] -nif: Converts Warhammer Online nif files to .xyz");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] -nif [source] [destination]");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] source           Specifies the folder of nif files to convert.");
                    System.Console.WriteLine("[!] destination      Specifies the name of the ouput file.");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] -ZONE2OBJ: Converts Warhammer Online ZoneXXX Folders to Wavefront .obj");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] -ZONE2OBJ source [destination] [index]");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] source           Specifies the zones folder.");
                    System.Console.WriteLine("[!] destination      Specifies the name of the ouput file.");
                    System.Console.WriteLine("[!] index            Specifies the number of the zone to convert.");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[?] Written by Debuggin <andi.ireland@gmail.com> and Zaru <dan.andraschko@gmail.com>");
                    System.Console.WriteLine("[.]");
                }
                return 0;
            }

            bool exported = false;
            string ctrlFlag = args[0].Replace("-", "").Replace("--", "").Replace("/", "").Trim().ToLower();

            if (string.Equals(ctrlFlag, "nif"))
            {
                exported = NifConvert(args);
            }
            else if (string.Equals(ctrlFlag, "zone2obj"))
            {
                exported = Zone2Obj(args);
            }
            
            if (!exported)
            {
                System.Console.WriteLine("[!] Error: Nothing to export.");
                System.Console.WriteLine("[.]");
                return 1;
            }

            System.Console.WriteLine($"[!] Export complete.");
            System.Console.WriteLine($"[.]");

            return 0;
        }

        private static bool Zone2Obj(string[] args)
        {
            string destination = args[1];

            int index = 0;
            int start = 0;
            int end = 1000;
            int potential = 0;

            if (args.Length == 3 && !(int.TryParse(args[2], out start)))
                destination = args[2];

            if (args.Length == 4 && !(int.TryParse(args[3], out start)))
                destination = args[3];

            if (destination.Length < 5)
                destination = args[1];

            if (start > 0)
                end = start;


            if (end >= start)
            {
                System.Console.WriteLine("[!] Error: Invalid zone number.");
                System.Console.WriteLine("[.]");
                return false;
            }

            bool done = false;
            int attempt = 0;
            while (!Directory.Exists(Path.GetPathRoot(destination)) && !done)
            {
                Directory.CreateDirectory(Path.GetPathRoot(destination));
                if (attempt++ > 8)
                    done = true;
            }

            if (!Directory.Exists(Path.GetPathRoot(destination)))
            {
                System.Console.WriteLine("[*] Error: could not create the output folder.");
                System.Console.WriteLine("[.]");
                return false;
            }

            string source = args[1] + $"\\zone{start:D3}";
            potential = end - start;
            for (index = start; index < end; ++index)
            {
                source = Path.GetFullPath(args[1] + $"\\zone{index:D3}");
                if (Directory.Exists(source))
                {
                    source = args[1] + $"\\zone{start:D3}";
                    System.Console.WriteLine($"[!] Scanning folder {source}\\*.*");
                    Zone zone = new Zone();
                    zone.LoadZone(args[1], index);

                    StreamWriter sw = new StreamWriter(new FileStream(Path.Combine(destination, $"zone{index:D3}.obj"), FileMode.Create, FileAccess.Write));

                    System.Console.WriteLine($"[!] Saving to {destination}\\zone{index:D3}.obj");

                    sw.WriteLine($"o Patch {index}");
                    sw.WriteLine($"");

                    sw.WriteLine($"g {index}");
                    foreach (Terrain.Vertex vertex in zone.Vertices)
                    {
                        sw.WriteLine($"v {vertex.P.X} {vertex.P.Z} {vertex.P.Y}");
                    }
                    sw.WriteLine($"# {zone.VertexCount} positions");
                    sw.WriteLine($"");

                    sw.WriteLine($"g {index}");
                    foreach (Terrain.Vertex vertex in zone.Vertices)
                    {
                        sw.WriteLine($"n {vertex.P.X} {vertex.P.Y} {vertex.P.Z}");
                    }
                    sw.WriteLine($"# {zone.VertexCount} normals");
                    sw.WriteLine($"");

                    sw.WriteLine($"g {index}");
                    foreach (Terrain.Triangle triangle in zone.Triangles)
                    {
                        sw.WriteLine($"f {triangle.A}/{triangle.A} {triangle.B}/{triangle.B} {triangle.C}/{triangle.C}");
                    }
                    sw.WriteLine($"# {zone.TriangleCount} triangles");
                    sw.WriteLine($"");

                    sw.Flush();
                    sw.Close();

                    if (File.Exists(Path.Combine(destination, $"zone{index:D3}.obj")))
                        return true;
                }
            }

            return false;
        }
        
        private static bool NifConvert(string[] args)
        {
            string destination = args[2];
            string source = args[1];
            List<string> files = new List<string>();

            foreach (string file in Directory.GetFiles(source))
            {
                if (!file.ToLower().Contains(".nif"))
                    continue;
                files.Add(file);
            }

            if (files.Count <= 0)
                return false;

            #region test area

            using (FileStream fs = new FileStream(files[0], FileMode.Open))
            {
                using (BinaryReader binReader = new BinaryReader(fs))
                {
                    NiFile nifFile = new NiFile(binReader);

                }
            }

            #endregion test area

            return false;
        }
    }
}

